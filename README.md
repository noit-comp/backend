## Table of Contents
- [About](#about)
- [Getting Started](#gettingStarted)
- [Deployment](#deployment)
- [Built Using](#built_using)
- [Contributing](../CONTRIBUTING.md)





## About <a name = "about"></a>
This project is part from NOIT Competition System. This repo contains
all code and tests related to backend module.





## Getting Started <a name = "gettingStarted"></a>
These instructions will get you a copy of the project up and running on your 
local machine for development and testing purposes. See 
[deployment](#deployment) for notes on how to deploy the project on a live 
system.


### Environment variables <a name = "environmentVariables"></a>
One can find the full list of environment variables 
[here](ENVIRONMENT_VARIABLES.md)


### Requirements
Before running in development mode you should have running instance of LDAP
and PostgreSQL servers. Both should be accessed via network protocols.


### Installing
Here is a tutorial how to get a development env running.
First you should download this repo (via git or download a release).
Before running project you should set at least required variables described
[here](ENVIRONMENT_VARIABLES.md). Then you can start this software with
proper gradle executable
```
./gradlew clean bootRun
```
After a while project should be working on http://localhost:8080

For developing is recommended to add `dev` to spring active profiles.





## Running the tests <a name = "tests"></a>
All test and checks can be run with 
```
./gradlew clean checkstyleMain test integrationTest spotbugsMain
```

### Unit testing
```
./gradlew clean test
```
This automatically activates spring `test` profile.

### Integration testing
We use [Testcontainers](https://www.testcontainers.org/) for
integration tests with DB. To be able to run our tests you
must have [docker](https://www.docker.com/) installed and
running. Also user should be able to create and runs containers.
[Here](https://docs.docker.com/engine/install/linux-postinstall/)
are some steps to enable non-root user to run docker command.

After you have successfully configured docker one can run tests
with 
```
./gradlew clean integrationTest
```
This automatically activates spring `integration-test` profile.

### Coding style tests
Test provided code style. Code should be written as shown in 
[CODING_GUIDELINES.md](CODING_GUIDELINES.md)
```
./gradlew clean checkstyleMain test integrationTest
```

### Bugs finder
We use spotbugs to find bugs in our code.
```
./gradlew clean spotbugsMain
```





## Deployment <a name = "deployment"></a>
As suggested in
[this article](https://spring.io/blog/2020/01/27/creating-docker-images-with-spring-boot-2-3-0-m1)
from [official Spring Blog](https://spring.io/blog/) everyone should generate
docker images not fat jars. Since SpringBoot v2.3.0 docker images could be 
build using gradle and maven. We use gradle so they are generated with
```
./gradlew bootBuildImage
```
We have created sample [docker-compose.yml](docker-compose.yml) file with most 
important properties but you can find all environment variables 
[here](#environmentVariables).

We use additional [script](https://gitlab.com/pecata/gitlab-cd-finalizer) for
deploying out app. This script uses fat jar in docker volume, so no such
big deal.

For **PRODUCTION** use with git (CI/CD). We suggest using additional 
***docker-compose.prod.yml*** file. It can be used to extend standart 
***docker-compose.yml*** configuration. Also it is excluded from tracking 
via ***.gitignore***. Then you have to start container with command: 
```
docker-compose -f docker-compose.yml -f docker-compose.prod.yml up -d
```




## Built Using <a name = "built_using"></a>
- [PostgreSQL](https://www.postgresql.org/) - Database
- [Liquibase](https://www.liquibase.org/) - DB Migrations Framework
- [SpringBoot](https://spring.io/projects/spring-boot) - Server Framework
- [ProjectReactor](https://projectreactor.io/) - Reactive Spring library
