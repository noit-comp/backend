# Coding Guidelines

- We use [Google Java Codestyle](https://google.github.io/styleguide/javaguide.html)
  for all java files
- You can validate that your code complies with these guidelines by running \
  `./gradlew clean checkstyle`
- When writing yaml or yml files list should be indented with 2 spaces and
  sequence values should not be indented additionally
```yaml
list:
- item1
- item2
ident:
  v2:
  - itemA
  - itemB
```

## Helpfull info

#### `.switchIfEmpty(<parameter>)` Method
Be **VERY** careful when using this. Especially when using it with methods as
parameters.
[Here](https://stackoverflow.com/questions/54373920/mono-switchifempty-is-always-called)
is some StackOwerflow question to help you understand how it works!

#### Reactor tips
You must read this articles to have better understanding of reactor
principes and techniques
- https://medium.com/@nikeshshetty/5-common-mistakes-of-webflux-novices-f8eda0cd6291
- https://medium.com/@kalpads/error-handling-with-reactive-streams-77b6ec7231ff
- https://nirajsonawane.github.io/2020/01/04/Project-Reactor-Part-3-Error-Handling/


#### Default integration test timeout
All integration tests (extending GenericIntegrationTest) have timeout set to
23 seconds. If you need more than 23 seconds, then u need to override this
with custom. One can read how to do this with
[this link](https://junit.org/junit5/docs/current/user-guide/#writing-tests-declarative-timeouts).

#### Integration test tips
For integration tests issues might arise due to synchronization. Use the `.block()` 
keyword to avoid that.

#### Fields that should not be null
Use `javax.validation.constraints.NotNull` when applying those restrictions.

#### Integration test container initialization exception
If your integration test don't work and it looks like there is no logical reason for that try to issue
docker network prune. From what I read this is an issue with testcontainers. Some times it fails to delete
the docker network which can lead to hitting a hard cap of the number of networks you can have which stops
the container from starting properly.

#### When validating input data
To add validations for an Object you need to place the respective tags inside the Object and then use the 
@Valid @Validated annotations. @Validated is for group validations. @Valid is for single object validations
