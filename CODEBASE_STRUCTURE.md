These are the directories that we are actively using in the development:

| Directory | Description |
| --- | --- |
| [controller](src/main/java/bg/unisofia/fmi/edusoft/backend/controller) | The controllers exposes the server API |
| [service](src/main/java/bg/unisofia/fmi/edusoft/backend/service) | The business logic is implemented here. |
| [repository](-/tree/master/src/main/java/bg/unisofia/fmi/edusoft/backend/repository) | Comunnication between store and the application. |
| [domain](src/main/java/bg/unisofia/fmi/edusoft/backend/domain) | Contains all data models. |
| [test](src/test) | Contains all unit tests |
| [integrationTest](src/integrationTest) | Contains all integration tests |
| [db/cangelog](src/main/resources/db/cangelog) | Contains everything related to db migrations |
