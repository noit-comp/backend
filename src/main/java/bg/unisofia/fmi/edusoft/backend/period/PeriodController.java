package bg.unisofia.fmi.edusoft.backend.period;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirements;
import java.util.List;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PeriodController {

  private final PeriodService periodService;

  public PeriodController(PeriodService periodService) {
    this.periodService = periodService;
  }

  /**
   * Get active periods.
   */
  @Operation(summary = "Active periods endpoint", description = "Returns all active periods")
  @GetMapping("/public/periods/active")
  //Set security to null for swagger(Removes the authentication requirement/lock).
  @SecurityRequirements()
  public List<Period> activePeriods() {
    return periodService.getActivePeriods();
  }
}
