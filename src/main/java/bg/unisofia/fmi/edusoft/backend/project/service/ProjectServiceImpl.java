package bg.unisofia.fmi.edusoft.backend.project.service;

import bg.unisofia.fmi.edusoft.backend.project.domain.Project;
import bg.unisofia.fmi.edusoft.backend.project.repository.ProjectRepository;
import java.util.Optional;
import org.springframework.stereotype.Service;

@Service
public class ProjectServiceImpl
    implements ProjectService {

  private final ProjectRepository projectRepository;

  public ProjectServiceImpl(
      ProjectRepository projectRepository) {
    this.projectRepository = projectRepository;
  }

  @Override
  public Project getProjectById(Long projectId) {
    return projectRepository.findById(projectId);
  }
}
