package bg.unisofia.fmi.edusoft.backend.user.mail;

import bg.unisofia.fmi.edusoft.backend.domain.db.UpdateMail;
import bg.unisofia.fmi.edusoft.backend.jdbc.NamedParameterSupport;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.OffsetDateTime;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

@Repository
public class UpdateMailRepository
    extends NamedParameterSupport<UpdateMail> {

  public UpdateMailRepository(DataSource dataSource) {
    super(dataSource, new UpdateMailRowMapper());
  }

  public Long insert(final String newMail, final Long userId) {
    final String sql = "INSERT INTO update_mail ( "
        + "new_mail, "
        + "user_id, "
        + "confirmed "
        + ") VALUES ( "
        + ":newMail , "
        + ":userId , "
        + ":confirmed "
        + ")";

    final Map<String, Object> params = Map.of(
        "newMail", newMail,
        "userId", userId,
        "confirmed", false);

    KeyHolder holder = new GeneratedKeyHolder();
    getNamedTemplate().update(sql, new MapSqlParameterSource(params), holder);

    Map<String, Object> updateMailId = holder.getKeys();
    if (updateMailId != null) {
      return Long.parseLong(updateMailId.get("id").toString());
    }

    throw new NullPointerException();
  }

  public void setConfirmed(final Long id, Boolean confirmed) {
    final String sql = "UPDATE update_mail "
        + "SET "
        + "confirmed = :confirmed "
        + "WHERE id = :id";

    final Map<String, Object> params = Map.of(
        "id", id,
        "confirmed", confirmed);

    getNamedTemplate().update(sql, params);
  }

  public Integer findUpdateMailCountByUserIdForTime(final Long userId, OffsetDateTime time) {
    final String sql = ""
        + "SELECT COUNT(*) FROM update_mail "
        + "WHERE generated_on >= :start AND user_id = :userId;";

    final Map<String, Object> params = Map.of(
        "start", time,
        "userId", userId
    );

    return getNamedTemplate().queryForObject(sql, params, Integer.class);
  }

  public UpdateMail findById(final Long id) {
    final String sql = "SELECT * FROM update_mail WHERE id IN (:id)";
    final Map<String, Object> params = Map.of("id", id);

    return findObject(sql, params);
  }

  public UpdateMail findUpdateMailByNewMail(final String newMail) {
    final String sql = "SELECT * FROM update_mail WHERE new_mail=:newMail";
    final Map<String, Object> params = Map.of("newMail", newMail);

    return findObject(sql, params);
  }

  public UpdateMail findUpdateMailByNewMailAndUserId(final String newMail, final Long userId) {
    final String sql = "SELECT * FROM update_mail WHERE new_mail=:newMail AND user_id=:userId";
    final Map<String, Object> params = Map.of(
        "newMail", newMail,
        "userId", userId
    );

    return findObject(sql, params);
  }

  public static final class UpdateMailRowMapper
      implements RowMapper<UpdateMail> {

    /**
     * Implementations must implement this method to map each row of data in the ResultSet. This
     * method should not call {@code next()} on the ResultSet; it is only supposed to map values of
     * the current row.
     *
     * @param rs     the ResultSet to map (pre-initialized for the current row)
     * @param rowNum the number of the current row
     * @return the result object for the current row (may be {@code null})
     * @throws SQLException if an SQLException is encountered getting column values (that is,
     *                      there's no need to catch SQLException)
     */
    @Override
    public UpdateMail mapRow(ResultSet rs, int rowNum)
        throws SQLException {

      final Long id = rs.getLong("id");
      final String new_mail = rs.getString("new_mail");
      final Long userId = rs.getLong("user_id");
      final Boolean confirmed = rs.getBoolean("confirmed");
      final OffsetDateTime dateTime = rs
          .getObject("generated_on", OffsetDateTime.class);

      return new UpdateMail(id, new_mail, userId, confirmed, dateTime);
    }
  }
}
