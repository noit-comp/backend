package bg.unisofia.fmi.edusoft.backend.activity;

import bg.unisofia.fmi.edusoft.backend.enums.EnumAbstractType;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode(callSuper = true)
public class ActivityEntity
    extends EnumAbstractType {

  public ActivityEntity(Long id, String value) {
    super(id, value);
  }
}
