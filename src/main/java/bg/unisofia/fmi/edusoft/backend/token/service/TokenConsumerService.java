package bg.unisofia.fmi.edusoft.backend.token.service;

import bg.unisofia.fmi.edusoft.backend.exception.InvalidAuthenticationException;
import bg.unisofia.fmi.edusoft.backend.token.exception.InvalidTokenCredentialsException;
import java.util.UUID;
import org.springframework.security.core.Authentication;

public interface TokenConsumerService {

  /**
   * Utilize token.
   *
   * @param tokenId  which token to utilize.
   * @param password password of the token.
   */
  void utilize(Authentication authentication, UUID tokenId, String password)
      throws InvalidTokenCredentialsException, InvalidAuthenticationException;
}

