package bg.unisofia.fmi.edusoft.backend.exception;

public class MailAlreadyExistsException extends MailException {

  public MailAlreadyExistsException() {
  }

  public MailAlreadyExistsException(String message) {
    super(message);
  }

  public MailAlreadyExistsException(String message, Throwable cause) {
    super(message, cause);
  }

  public MailAlreadyExistsException(Throwable cause) {
    super(cause);
  }

  public MailAlreadyExistsException(String message, Throwable cause, boolean enableSuppression,
      boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
