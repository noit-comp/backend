package bg.unisofia.fmi.edusoft.backend.exception;

public class MissingElementException extends RuntimeException {

  public MissingElementException() {
  }

  public MissingElementException(String message) {
    super(message);
  }

  public MissingElementException(String message, Throwable cause) {
    super(message, cause);
  }

  public MissingElementException(Throwable cause) {
    super(cause);
  }

  public MissingElementException(String message, Throwable cause, boolean enableSuppression,
      boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
