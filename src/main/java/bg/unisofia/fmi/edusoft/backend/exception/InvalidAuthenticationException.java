package bg.unisofia.fmi.edusoft.backend.exception;

public class InvalidAuthenticationException extends UnauthorizedException {

  public InvalidAuthenticationException() {
  }

  public InvalidAuthenticationException(String message) {
    super(message);
  }

  public InvalidAuthenticationException(String message, Throwable cause) {
    super(message, cause);
  }

  public InvalidAuthenticationException(Throwable cause) {
    super(cause);
  }

  public InvalidAuthenticationException(String message, Throwable cause,
      boolean enableSuppression,
      boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
