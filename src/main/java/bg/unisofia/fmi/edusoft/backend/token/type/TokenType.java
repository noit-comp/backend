package bg.unisofia.fmi.edusoft.backend.token.type;

public enum TokenType {
  GRANT_COMMITTEE,
  GRANT_SCHOOL,
  GRANT_REGION,
  ACCEPT_INSTRUCTING_PROJECT,
  @Deprecated
  ACCEPT_PRACTICING_IN_PROJECT,
  CONFIRM_EMAIL,
  ACCEPT_PARTICIPATING_IN_PROJECT
}
