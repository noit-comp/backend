package bg.unisofia.fmi.edusoft.backend.token.exception;

import org.springframework.http.HttpStatus;

public class InvalidTokenCredentialsException
    extends TokenException {

  public InvalidTokenCredentialsException() {
    super(HttpStatus.BAD_REQUEST, "Invalid token", null, null);
  }

}
