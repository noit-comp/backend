package bg.unisofia.fmi.edusoft.backend.group.repository;

import bg.unisofia.fmi.edusoft.backend.group.domain.LdapGroup;
import javax.lang.model.element.Name;
import org.springframework.data.ldap.repository.LdapRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LdapGroupRepository extends LdapRepository<LdapGroup> {
  LdapGroup findByName(String groupName);

  LdapGroup findById(Name groupId);
}
