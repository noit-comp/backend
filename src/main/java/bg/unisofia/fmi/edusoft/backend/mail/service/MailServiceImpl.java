package bg.unisofia.fmi.edusoft.backend.mail.service;

import bg.unisofia.fmi.edusoft.backend.mail.domain.MailTemplateEntity;
import bg.unisofia.fmi.edusoft.backend.mail.domain.MailTemplateType;
import java.util.Map;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class MailServiceImpl implements MailService {

  private final JavaMailSender mailSender;
  private final MailTemplateService mailTemplateService;
  private final String senderMail;

  /**
   * This is a constructor.
   **/
  public MailServiceImpl(JavaMailSender mailSender,
      MailTemplateService mailTemplateService,
      @Value("${spring.mail.sender-email}") String senderMail) {
    this.mailSender = mailSender;
    this.mailTemplateService = mailTemplateService;
    this.senderMail = senderMail;
  }


  /**
   * Send confirmation mail to user with confirmation token.
   *
   * @param templateType the type of the message
   * @param to           the person the mail is sent to
   * @param params       the parameters to substitute in the message
   * @return successfully sent mail
   */
  public Boolean sendMail(MailTemplateType templateType, String to, Map<String, String> params) {
    SimpleMailMessage message = new SimpleMailMessage();
    MailTemplateEntity template = mailTemplateService.getEntity(templateType);

    if (template == null) {
      return false;
    }

    final String mailBody = replaceParamsInBody(template.getMailBody(), params);

    message.setFrom(senderMail);
    message.setTo(to);
    message.setSubject(template.getSubject());
    message.setText(mailBody);
    mailSender.send(message);
    return true;
  }

  protected static String replaceParamsInBody(String body, final Map<String, String> params) {
    for (final Map.Entry<String, String> entry : params.entrySet()) {
      String replaceRegex = "\\{" + entry.getKey() + "}";
      body = body.replaceAll(replaceRegex, entry.getValue());
    }

    return body;
  }
}
