package bg.unisofia.fmi.edusoft.backend.security.jjwt.secrets;

import bg.unisofia.fmi.edusoft.backend.jdbc.NamedParameterSupport;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.OffsetDateTime;
import java.util.Map;
import java.util.UUID;
import javax.sql.DataSource;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class JjwtSecretRepository
    extends NamedParameterSupport<JjwtSecret> {

  public JjwtSecretRepository(DataSource dataSource) {
    super(dataSource, new JjwtSecretRowMapper());
  }

  public JjwtSecret findLast() {
    final String sql = "SELECT * FROM jjwt_secrets ORDER BY id DESC LIMIT 1";
    return getNamedTemplate().queryForObject(sql, Map.of(), rowMapper);
  }

  public void insert(final UUID secret) {
    final String sql = "INSERT INTO jjwt_secrets(secret) VALUES(:secret)";
    final var params = Map.of("secret", secret);
    getNamedTemplate().update(sql, params);
  }


  private static final class JjwtSecretRowMapper
      implements RowMapper<JjwtSecret> {

    /**
     * Implementations must implement this method to map each row of data in the ResultSet. This
     * method should not call {@code next()} on the ResultSet; it is only supposed to map values of
     * the current row.
     *
     * @param rs     the ResultSet to map (pre-initialized for the current row)
     * @param rowNum the number of the current row
     * @return the result object for the current row (may be {@code null})
     * @throws SQLException if an SQLException is encountered getting column values (that is,
     *                      there's no need to catch SQLException)
     */
    @Override
    public JjwtSecret mapRow(ResultSet rs, int rowNum)
        throws SQLException {
      final Long id = rs.getLong("id");
      final UUID secret = rs.getObject("secret", UUID.class);
      final OffsetDateTime generatedOn = rs.getObject("generated_on", OffsetDateTime.class);

      return new JjwtSecret(id, secret, generatedOn);
    }
  }

}
