package bg.unisofia.fmi.edusoft.backend.exception;

public class InvalidLdapCredentialsException extends UnauthorizedException {

  public InvalidLdapCredentialsException() {
  }

  public InvalidLdapCredentialsException(String message) {
    super(message);
  }

  public InvalidLdapCredentialsException(String message, Throwable cause) {
    super(message, cause);
  }

  public InvalidLdapCredentialsException(Throwable cause) {
    super(cause);
  }

  public InvalidLdapCredentialsException(String message, Throwable cause, boolean enableSuppression,
      boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
