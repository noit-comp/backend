package bg.unisofia.fmi.edusoft.backend.user.domain.requests;

import bg.unisofia.fmi.edusoft.backend.user.domain.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserTokenDetails {

  private Long id;
  private String username;
  private String firstName;
  private String middleName;
  private String lastName;
  private String mail;
  private Set<String> roles;

  /**
   * Generate UserTokenDetails from User.
   *
   * @param user where to get data from
   */
  public UserTokenDetails(User user) {
    this.id = user.getId();
    this.username = user.getUsername();
    this.firstName = user.getFirstName();
    this.middleName = user.getMiddleName();
    this.lastName = user.getLastName();
    this.mail = user.getMail();
    this.roles = user.getGroupNamesUserIsMemberOf();
  }

  /**
   * Get all granted authorities for user.
   *
   * @return granted authorities
   */
  @JsonIgnore
  public Collection<? extends GrantedAuthority> getAuthorities() {
    return roles.stream()
        .map(SimpleGrantedAuthority::new)
        .collect(Collectors.toList());
  }
}
