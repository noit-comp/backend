package bg.unisofia.fmi.edusoft.backend.role;

import bg.unisofia.fmi.edusoft.backend.exception.MissingElementException;
import bg.unisofia.fmi.edusoft.backend.group.service.GroupService;
import bg.unisofia.fmi.edusoft.backend.user.domain.User;
import bg.unisofia.fmi.edusoft.backend.user.service.UserService;
import java.util.function.Function;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImpl
    implements RoleService {

  private final GroupService groupService;
  private final UserService userService;

  public RoleServiceImpl(GroupService groupService, UserService userService) {
    this.groupService = groupService;
    this.userService = userService;
  }

  private Boolean addOrRemoveUserRole(String username,
      Function<User, Boolean> addOrRemove) {
    try {
      User user = userService.findByUsernameOrMail(username);
      return addOrRemove.apply(user);
    } catch (MissingElementException e) {
      return false;
    }
  }

  @Override
  public Boolean addRoleToUser(String groupName, String username) {
    return addOrRemoveUserRole(username,
        (user) -> groupService.addUserToGroup(user, groupName));
  }

  @Override
  public Boolean removeRoleFromUser(String groupName, String username) {
    return addOrRemoveUserRole(username,
        (user) -> groupService.removeUserFromGroup(user, groupName));
  }
}
