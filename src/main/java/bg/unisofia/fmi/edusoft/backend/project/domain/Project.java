package bg.unisofia.fmi.edusoft.backend.project.domain;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@AllArgsConstructor
@EqualsAndHashCode
@Getter
public class Project {

  private final Long id;
  private final String name;
  private final String shortDescription;
  private final String fullDescription;
  private final Long category;
  private final Long competition;

}
