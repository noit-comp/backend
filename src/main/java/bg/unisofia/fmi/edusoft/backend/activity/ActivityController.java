package bg.unisofia.fmi.edusoft.backend.activity;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirements;
import java.util.List;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ActivityController {

  private final ActivityService activityService;

  public ActivityController(ActivityService activityService) {
    this.activityService = activityService;
  }

  /**
   * Get current activities.
   */
  @Operation(
      summary = "Active (current) activities endpoint",
      description = "Returns active (current) activities"
  )
  @GetMapping("/public/activities/current")
  //Set security to null for swagger(Removes the authentication requirement/lock).
  @SecurityRequirements()
  public List<Activity> currentActivities() {
    return activityService.getCurrentActivities();
  }
}
