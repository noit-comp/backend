package bg.unisofia.fmi.edusoft.backend.project.domain;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@AllArgsConstructor
@EqualsAndHashCode
@Getter
public class ProjectRoundPoints {

  private final Long id;
  private final Long project;
  private final Long round;
  private final Long points;
  private final String advance;

}
