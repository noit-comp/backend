package bg.unisofia.fmi.edusoft.backend.user.repository.ldap;

import bg.unisofia.fmi.edusoft.backend.user.domain.ldap.LdapUser;
import org.springframework.data.ldap.repository.LdapRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LdapUserRepository extends LdapRepository<LdapUser> {

  LdapUser findByUsername(String username);

  LdapUser findByUsernameOrMail(String username, String mail);

  default Boolean existsByUsername(String username) {
    return findByUsername(username) != null;
  }

  default Boolean existsByMail(String mail) {
    return findByMail(mail) != null;
  }

  LdapUser findByMail(String mail);
}
