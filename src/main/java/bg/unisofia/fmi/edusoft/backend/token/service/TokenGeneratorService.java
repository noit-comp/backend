package bg.unisofia.fmi.edusoft.backend.token.service;

import bg.unisofia.fmi.edusoft.backend.token.type.TokenType;
import java.util.UUID;

public interface TokenGeneratorService {
  /**
   * Create token and sent it to recipient via mail.
   *
   * @return Is operation successful.
   */
  UUID createToken(
      Long userId,
      TokenType tokenType,
      String recipient,
      Long additionalData
  );
}
