package bg.unisofia.fmi.edusoft.backend.activity;

public enum Activity {
  REGISTER_PROJECTS,
  EDIT_PROJECTS,
  GRADE_PROJECTS_FOR_ROUND,
  ACCEPT_INSTRUCTING_PROJECT,
  ACCEPT_PARTICIPATING_IN_PROJECT
}
