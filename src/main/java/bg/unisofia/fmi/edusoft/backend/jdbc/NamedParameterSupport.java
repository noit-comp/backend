package bg.unisofia.fmi.edusoft.backend.jdbc;

import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

public abstract class NamedParameterSupport<T>
    extends NamedParameterJdbcDaoSupport {

  protected final RowMapper<T> rowMapper;

  public NamedParameterSupport(final DataSource daoSupport,
      RowMapper<T> rowMapper) {
    this.rowMapper = rowMapper;
    setDataSource(daoSupport);
  }

  protected NamedParameterJdbcTemplate getNamedTemplate() {
    final NamedParameterJdbcTemplate namedParameterJdbcTemplate = getNamedParameterJdbcTemplate();
    if (namedParameterJdbcTemplate == null) {
      throw new RuntimeException("Missing JDBC TEMPLATE");
    }
    return namedParameterJdbcTemplate;
  }

  protected T findObject(final String sql, final Map<String, Object> params) {
    try {
      return getNamedTemplate().queryForObject(sql, params, rowMapper);
    } catch (EmptyResultDataAccessException ignored) {
      return null;
    }
  }

  protected List<T> findAll(final String sql, final Map<String, Object> params) {
    try {
      return getNamedTemplate().query(sql, params, rowMapper);
    } catch (EmptyResultDataAccessException ignored) {
      return List.of();
    }
  }
}
