package bg.unisofia.fmi.edusoft.backend.project.service;

import bg.unisofia.fmi.edusoft.backend.project.domain.Project;

public interface ProjectService {

  Project getProjectById(Long projectId);

}
