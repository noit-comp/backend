package bg.unisofia.fmi.edusoft.backend.period;

import bg.unisofia.fmi.edusoft.backend.jdbc.NamedParameterSupport;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class PeriodRepository extends NamedParameterSupport<Period> {

  public PeriodRepository(final DataSource dataSource) {
    super(dataSource, new PeriodRowMapper());
  }

  public List<Period> findActivePeriods(final LocalDateTime time) {
    final String sql = "SELECT * FROM period WHERE :time BETWEEN start_time AND end_time";
    final Map<String, Object> params = Map.of("time", time);
    return findAll(sql, params);
  }

  private static final class PeriodRowMapper
      implements RowMapper<Period> {

    /**
     * Implementations must implement this method to map each row of data in the ResultSet. This
     * method should not call {@code next()} on the ResultSet; it is only supposed to map values of
     * the current row.
     *
     * @param rs     the ResultSet to map (pre-initialized for the current row)
     * @param rowNum the number of the current row
     * @return the result object for the current row (may be {@code null})
     * @throws SQLException if an SQLException is encountered getting column values (that is,
     *                      there's no need to catch SQLException)
     */
    @Override
    public Period mapRow(final ResultSet rs, final int rowNum)
        throws SQLException {
      final Long id = rs.getLong("id");
      final Long competitionId = rs.getLong("competition_id");
      final LocalDateTime start = rs.getObject("start_time", LocalDateTime.class);
      final LocalDateTime end = rs.getObject("end_time", LocalDateTime.class);
      return new Period(id, competitionId, start, end);
    }
  }
}
