package bg.unisofia.fmi.edusoft.backend.periodactivity;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

@Service
public class PeriodActivityService {

  private final PeriodActivityRepository periodActivityRepository;

  public PeriodActivityService(
      PeriodActivityRepository periodActivityRepository) {
    this.periodActivityRepository = periodActivityRepository;
  }

  public Set<Long> getAllActivitiesIdsByPeriodIds(final Collection<Long> periodIds) {
    if (periodIds == null || periodIds.isEmpty()) {
      return Set.of();
    }

    final List<PeriodActivity> allPeriodActivities
        = periodActivityRepository.findAllByPeriodIdIn(periodIds);

    return allPeriodActivities.stream()
        .map(PeriodActivity::getActivityId)
        .collect(Collectors.toSet());
  }
}
