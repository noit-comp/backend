package bg.unisofia.fmi.edusoft.backend.group;

public enum CompetitionRole {
  SCHOOL,
  REGION,
  MANAGER,
  EXPERT
}
