package bg.unisofia.fmi.edusoft.backend.version;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Getter
@EqualsAndHashCode(callSuper = true)
@Component
public class VersionDto extends VersionSimpleDto {

  @Value("${spring.profiles.active:Unknown}")
  private String activeProfiles;

  @Value("${spring.profiles.include:Unknown}")
  private String includedProfiles;

  @Value("${app.cors:MissingCors}")
  private String appCors;

  @Value("${app.path:MissingPath}")
  private String appPathData;

  @Value("${logging.file.name:MissingLoggingFilename}")
  private String appPathLogs;

  @Value("${java.runtime.version:MissingJavaVersion}")
  private String javaVersion;

  @Value("${java.vm.name:MissingJavaProvider}")
  private String javaProvider;

  @Value("${os.version:MissingOsVersion}")
  private String osVersion;

}
