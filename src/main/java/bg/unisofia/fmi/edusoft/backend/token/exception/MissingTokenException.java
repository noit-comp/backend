package bg.unisofia.fmi.edusoft.backend.token.exception;

import org.springframework.http.HttpStatus;

public class MissingTokenException
    extends TokenException {

  public MissingTokenException() {
    super(HttpStatus.NOT_FOUND, "Missing token", null, null);
  }

}
