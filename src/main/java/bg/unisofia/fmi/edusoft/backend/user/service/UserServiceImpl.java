package bg.unisofia.fmi.edusoft.backend.user.service;

import bg.unisofia.fmi.edusoft.backend.domain.db.UpdateMail;
import bg.unisofia.fmi.edusoft.backend.exception.InvalidLdapCredentialsException;
import bg.unisofia.fmi.edusoft.backend.exception.MailAlreadyExistsException;
import bg.unisofia.fmi.edusoft.backend.exception.MailRateLimitExceededException;
import bg.unisofia.fmi.edusoft.backend.exception.MissingElementException;
import bg.unisofia.fmi.edusoft.backend.exception.UsernameAlreadyExistsException;
import bg.unisofia.fmi.edusoft.backend.group.service.GroupService;
import bg.unisofia.fmi.edusoft.backend.mail.domain.MailTemplateType;
import bg.unisofia.fmi.edusoft.backend.mail.service.MailService;
import bg.unisofia.fmi.edusoft.backend.token.service.TokenGeneratorService;
import bg.unisofia.fmi.edusoft.backend.token.type.TokenType;
import bg.unisofia.fmi.edusoft.backend.user.domain.User;
import bg.unisofia.fmi.edusoft.backend.user.domain.db.DbUser;
import bg.unisofia.fmi.edusoft.backend.user.domain.ldap.LdapUser;
import bg.unisofia.fmi.edusoft.backend.user.domain.requests.AuthenticationLoginRequest;
import bg.unisofia.fmi.edusoft.backend.user.domain.requests.RegisterUserRequest;
import bg.unisofia.fmi.edusoft.backend.user.domain.requests.UpdateMailRequest;
import bg.unisofia.fmi.edusoft.backend.user.domain.requests.UpdatePasswordRequest;
import bg.unisofia.fmi.edusoft.backend.user.domain.requests.UserTokenDetails;
import bg.unisofia.fmi.edusoft.backend.user.login.DbUserLoginRepository;
import bg.unisofia.fmi.edusoft.backend.user.mail.UpdateMailRepository;
import bg.unisofia.fmi.edusoft.backend.user.repository.db.DbUserRepository;
import bg.unisofia.fmi.edusoft.backend.user.repository.ldap.CustomLdapUserRepository;
import bg.unisofia.fmi.edusoft.backend.user.repository.ldap.LdapUserRepository;
import bg.unisofia.fmi.edusoft.backend.utility.LdapHelper;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import javax.naming.Name;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl
    implements UserService {

  private final DbUserRepository dbUserRepository;
  private final DbUserLoginRepository dbUserLoginRepository;
  private final LdapUserRepository ldapUserRepository;
  private final CustomLdapUserRepository customLdapUserRepository;
  private final UpdateMailRepository updateMailRepository;
  private final Integer mailRateLimit;
  private final TokenGeneratorService tokenGeneratorService;
  private final String frontendUrl;
  private final MailService mailService;
  private final GroupService groupService;

  public UserServiceImpl(
      DbUserRepository dbUserRepository,
      DbUserLoginRepository dbUserLoginRepository,
      LdapUserRepository ldapUserRepository,
      CustomLdapUserRepository customLdapUserRepository,
      UpdateMailRepository updateMailRepository,
      TokenGeneratorService tokenGeneratorService,
      MailService mailService,
      @Value("${frontend-url}") String frontendUrl,
      @Value("${constant.mailRateLimit}") Integer mailRateLimit,
      GroupService groupService
  ) {
    this.dbUserRepository = dbUserRepository;
    this.dbUserLoginRepository = dbUserLoginRepository;
    this.ldapUserRepository = ldapUserRepository;
    this.customLdapUserRepository = customLdapUserRepository;
    this.updateMailRepository = updateMailRepository;
    this.mailRateLimit = mailRateLimit;
    this.groupService = groupService;
    this.tokenGeneratorService = tokenGeneratorService;
    this.frontendUrl = frontendUrl;
    this.mailService = mailService;
  }

  private void addLoginRecord(final Long userId) {
    dbUserLoginRepository.insert(userId, LocalDateTime.now());
  }

  private LdapUser findLdapUserByUsernameOrMail(final String searchString)
      throws MissingElementException {
    final LdapUser ldapUser =
        ldapUserRepository.findByUsernameOrMail(searchString, searchString);

    if (ldapUser == null) {
      throw new MissingElementException(searchString);
    }

    return ldapUser;
  }

  /**
   * Gets DbUser from db and if there is none inserts and then return.
   *
   * @param username username which is being searched
   * @return DbUser from DB
   */
  private DbUser findOrCreateDbUserByUsername(final String username) {
    final DbUser dbUser = dbUserRepository.findByUsername(username);
    if (dbUser == null) {
      final Long dbUserId = dbUserRepository.insertWithUsername(username);
      return dbUserRepository.findById(dbUserId);
    }

    return dbUser;
  }

  /**
   * Authenticates user from ldap.
   *
   * <p>Authenticates user from ldap source and creates proxy user inside db if
   * not exists. Inserts userLogin data.
   *
   * @param authenticationLoginRequest request data
   * @return Combined user data from ldap and db
   */
  @Override
  public User authenticate(AuthenticationLoginRequest authenticationLoginRequest)
      throws MissingElementException, InvalidLdapCredentialsException {
    final LdapUser ldapUser =
        findLdapUserByUsernameOrMail(authenticationLoginRequest.getUsername());

    Boolean loggedIn = customLdapUserRepository
        .authenticate(ldapUser.getUsername(),
            authenticationLoginRequest.getPassword());

    if (!loggedIn) {
      throw new InvalidLdapCredentialsException();
    }

    final DbUser dbUser = findOrCreateDbUserByUsername(ldapUser.getUsername());
    addLoginRecord(dbUser.getId());

    return new User(ldapUser, dbUser);
  }

  /**
   * Find user via mail or username.
   *
   * @param searchString username or email
   * @return combined user
   */
  @Override
  public User findByUsernameOrMail(String searchString) throws MissingElementException {
    final LdapUser ldapUser = findLdapUserByUsernameOrMail(searchString);
    final DbUser dbUser = findOrCreateDbUserByUsername(ldapUser.getUsername());

    return new User(ldapUser, dbUser);
  }

  /**
   * Register new user in ldap and db.
   *
   * @param registerUserRequest registrant credentials
   * @return combined user data
   */
  @Override
  public User register(RegisterUserRequest registerUserRequest)
      throws UsernameAlreadyExistsException, MailAlreadyExistsException {
    if (ldapUserRepository.existsByUsername(registerUserRequest.getUsername())) {
      throw new UsernameAlreadyExistsException();
    }

    if (ldapUserRepository.existsByMail(registerUserRequest.getEmail())) {
      throw new MailAlreadyExistsException();
    }

    final LdapUser ldapUser = ldapUserRepository.save(new LdapUser(
        registerUserRequest.getUsername(),
        registerUserRequest.getFirstName(),
        registerUserRequest.getMiddleName(),
        registerUserRequest.getLastName(),
        registerUserRequest.getEmail(),
        registerUserRequest.getPassword()
    ));

    final DbUser dbUser = findOrCreateDbUserByUsername(ldapUser.getUsername());
    saveUpdateMailAndSendMail(dbUser.getId(), ldapUser.getMail(), ldapUser.getMail());
    return new User(ldapUser, dbUser);
  }

  private LdapUser updateUserField(String username, Consumer<LdapUser> updateField)
      throws MissingElementException {
    final LdapUser ldapUser = findLdapUserByUsernameOrMail(username);
    updateField.accept(ldapUser);
    return ldapUserRepository.save(ldapUser);
  }

  @Override
  public LdapUser updatePassword(String username,
      UpdatePasswordRequest updatePasswordRequest) throws MissingElementException {
    return updateUserField(username,
        (ldapUser) -> ldapUser.setPassword(updatePasswordRequest.getNewPassword()));
  }

  private Long saveUpdateMail(Long userId, String newMail, UpdateMail updateMail) {
    if (updateMail != null) {
      updateMailRepository.setConfirmed(updateMail.getId(), false);
      return updateMail.getId();
    }

    return updateMailRepository.insert(newMail, userId);
  }

  private void saveUpdateMailAndSendMail(Long userId, String newMail, String oldMail) {
    UpdateMail updateMail = updateMailRepository
        .findUpdateMailByNewMailAndUserId(newMail, userId);

    final Long updateMailId = saveUpdateMail(userId, newMail, updateMail);

    UUID tokenId = tokenGeneratorService.createToken(userId, TokenType.CONFIRM_EMAIL,
        oldMail, updateMailId);

    Map<String, String> mailParameters = new HashMap<>();
    mailParameters.put("token", frontendUrl + "/token/" + tokenId.toString());
    mailService.sendMail(MailTemplateType.CONFIRM_MAIL, oldMail, mailParameters);
  }

  @Override
  public void requestMailUpdate(UserTokenDetails userTokenDetails,
      UpdateMailRequest updateMailRequest) throws MailAlreadyExistsException {

    if (ldapUserRepository.existsByMail(updateMailRequest.getNewMail())) {
      throw new MailAlreadyExistsException();
    }

    OffsetDateTime monthAgo = OffsetDateTime.now().minusMonths(1L);
    Integer changeRequestCount =
        updateMailRepository.findUpdateMailCountByUserIdForTime(userTokenDetails.getId(), monthAgo);

    if (changeRequestCount >= mailRateLimit) {
      throw new MailRateLimitExceededException();
    }

    saveUpdateMailAndSendMail(userTokenDetails.getId(), updateMailRequest.getNewMail(),
        userTokenDetails.getMail());
  }

  @Override
  public void confirmedMail(Long id) throws MissingElementException, MailAlreadyExistsException {
    UpdateMail updateMail = updateMailRepository.findById(id);

    if (updateMail == null) {
      throw new MissingElementException();
    }

    if (ldapUserRepository.existsByMail(updateMail.getNewMail())) {
      //TODO: sending a mail here to inform the user might be a good idea
      //seems like it is for a separate issue though
      throw new MailAlreadyExistsException("Mail confirmed already");
    }

    updateMailRepository.setConfirmed(id, true);

    final DbUser dbUser = dbUserRepository.findById(updateMail.getUserId());

    if (dbUser == null) {
      throw new MissingElementException();
    }

    if (!dbUser.getConfirmedMail()) {
      dbUserRepository.confirmMailByUserId(dbUser.getId());
    }

    updateUserField(dbUser.getUsername(),
        (ldapUser) -> ldapUser.setMail(updateMail.getNewMail()));
  }

  private List<LdapUser> getLdapUsersByUsernames(final Collection<String> usernames) {

    Set<Name> ldapIds = LdapHelper.getLdapIdsFromUsernames(usernames);
    Iterable<LdapUser> ldapUsers = ldapUserRepository.findAllById(ldapIds);

    List<LdapUser> ldapUsersList = StreamSupport
        .stream(ldapUsers.spliterator(), false)
        .collect(Collectors.toList());

    return ldapUsersList;
  }

  private List<User> generateUsersByLdapAndDbUsers(
      final Collection<LdapUser> ldapUsers,
      final Collection<DbUser> dbUsers
  ) throws MissingElementException {

    final Map<String, DbUser> dbUsersByUsername = dbUsers.stream()
        .collect(Collectors.toMap(DbUser::getUsername, Function.identity()));

    return ldapUsers.stream().map(lu -> {
      final DbUser dbUser = dbUsersByUsername.get(lu.getUsername());
      return new User(lu, dbUser);
    }).collect(Collectors.toList());
  }

  /**
   * Search and take (db and ldap) users by usernames.
   */
  @Override
  public List<User> findUsersByUsernames(final Collection<String> usernames)
      throws MissingElementException {
    List<LdapUser> ldapUsers = this.getLdapUsersByUsernames(usernames);

    List<DbUser> dbUsers = dbUserRepository.findAllByUsernameIn(usernames);

    return this.generateUsersByLdapAndDbUsers(ldapUsers, dbUsers);
  }

  /**
   * Search and take (db and ldap) users by (db) ids.
   */
  @Override
  public List<User> findUsersByIds(final Collection<Long> ids)
      throws MissingElementException {

    List<DbUser> dbUsers = dbUserRepository.findAllById(ids);

    if (dbUsers.isEmpty() && !ids.isEmpty()) {
      throw new MissingElementException();
    }

    List<String> usernamesFromDbUsers = dbUsers.stream()
        .map(DbUser::getUsername)
        .collect(Collectors.toList());

    List<LdapUser> ldapUsers = this.getLdapUsersByUsernames(usernamesFromDbUsers);

    return this.generateUsersByLdapAndDbUsers(ldapUsers, dbUsers);
  }

  @Override
  public User findUserById(Long id) throws MissingElementException {
    DbUser dbUser = dbUserRepository.findById(id);
    if (dbUser == null) {
      throw new MissingElementException();
    }

    LdapUser ldapUser = ldapUserRepository.findByUsername(dbUser.getUsername());

    return new User(ldapUser, dbUser);
  }

  @Override
  public List<User> findUsersByUsernamesAndGroups(
      final Collection<String> usernames,
      final Collection<String> groups
  ) {
    Set<String> usernamesFromUsersAndGroups = new HashSet<>();
    usernamesFromUsersAndGroups.addAll(groupService.getAllUsersUsernamesByGroups(groups));
    usernamesFromUsersAndGroups.addAll(usernames);

    return findUsersByUsernames(usernamesFromUsersAndGroups);
  }
}
