package bg.unisofia.fmi.edusoft.backend.user.controller;

import bg.unisofia.fmi.edusoft.backend.exception.InvalidLdapCredentialsException;
import bg.unisofia.fmi.edusoft.backend.exception.MailAlreadyExistsException;
import bg.unisofia.fmi.edusoft.backend.exception.MailRateLimitExceededException;
import bg.unisofia.fmi.edusoft.backend.exception.MissingElementException;
import bg.unisofia.fmi.edusoft.backend.user.domain.requests.AuthenticationLoginRequest;
import bg.unisofia.fmi.edusoft.backend.user.domain.requests.UpdateMailRequest;
import bg.unisofia.fmi.edusoft.backend.user.domain.requests.UpdatePasswordRequest;
import bg.unisofia.fmi.edusoft.backend.user.domain.requests.UserDetailsResponse;
import bg.unisofia.fmi.edusoft.backend.user.domain.requests.UserTokenDetails;
import bg.unisofia.fmi.edusoft.backend.user.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import javax.validation.Valid;
import org.apache.commons.lang3.NotImplementedException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@ApiResponses(value = {
    @ApiResponse(responseCode = "200", description = "Successfully updated"),
    @ApiResponse(responseCode = "404", description = "User not found")
})
@RestController
@RequestMapping("/user")
public class UserController {

  private final UserService userService;

  public UserController(UserService userService) {
    this.userService = userService;
  }

  /**
   * User data endpoint.
   */
  @GetMapping("/{username}")
  @Operation(summary = "NOT IMPLEMENTED: Get full data about user")
  public Mono<UserDetailsResponse> getUserData(@PathVariable String username) {
    return Mono.error(new NotImplementedException());
  }

  /**
   * Update user endpoint.
   */
  @ApiResponses(value = {
      @ApiResponse(responseCode = "409",
          description = "Email or username is taken. Return email or username",
          content = @Content(mediaType = MediaType.TEXT_PLAIN_VALUE)
      ),
      @ApiResponse(responseCode = "201",
          description = "Update mail request created successfully",
          content = @Content(mediaType = MediaType.TEXT_PLAIN_VALUE))})
  @Operation(summary = "Update user data",
      description = "Update user email or password")
  @PutMapping("/mail")
  public ResponseEntity<Object> updateMail(Authentication authentication,
      @Valid @RequestBody UpdateMailRequest updateMailRequest) {
    UserTokenDetails userTokenDetails = (UserTokenDetails) authentication.getDetails();

    try {
      userService.requestMailUpdate(userTokenDetails, updateMailRequest);
      return ResponseEntity.status(HttpStatus.CREATED).build();
    } catch (MailAlreadyExistsException e) {
      return ResponseEntity.status(HttpStatus.CONFLICT).build();
    } catch (MailRateLimitExceededException e) {
      return ResponseEntity.status(HttpStatus.TOO_MANY_REQUESTS).build();
    }
  }

  /**
   * Update user password.
   */
  @Operation(summary = "Update user password data",
      description = "Update user password")
  @PutMapping("/password")
  public ResponseEntity<Object> updatePassword(Authentication authentication,
      @Valid @RequestBody UpdatePasswordRequest updatePasswordRequest)
      throws MissingElementException {

    UserTokenDetails userTokenDetails = (UserTokenDetails) authentication.getDetails();
    try {
      userService.authenticate(new AuthenticationLoginRequest(userTokenDetails.getUsername(),
          updatePasswordRequest.getOldPassword()));
    } catch (InvalidLdapCredentialsException e) {
      return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }

    userService
        .updatePassword(userTokenDetails.getUsername(), updatePasswordRequest);

    return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
  }
}
