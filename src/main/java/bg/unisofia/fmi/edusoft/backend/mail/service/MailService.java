package bg.unisofia.fmi.edusoft.backend.mail.service;

import bg.unisofia.fmi.edusoft.backend.mail.domain.MailTemplateType;
import java.util.Map;

public interface MailService {

  /**
   * Send confirmation mail to user with confirmation token.
   *
   * @param templateType the type of the message
   * @param to    the person the mail is sent to
   * @param params the parameters to substitute in the message
   * @return successfully sent mail
   */
  Boolean sendMail(MailTemplateType templateType, String to, Map<String, String> params);
}
