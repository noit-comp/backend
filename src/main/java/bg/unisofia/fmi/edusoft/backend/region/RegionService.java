package bg.unisofia.fmi.edusoft.backend.region;

import java.util.Collection;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class RegionService {

  private final RegionRepository regionRepository;

  public RegionService(RegionRepository regionRepository) {
    this.regionRepository = regionRepository;
  }

  public List<Region> getAllRegionByCityIdIn(final Collection<Long> cityIds) {
    return regionRepository.getAllRegionByCityIdIn(cityIds);
  }
}
