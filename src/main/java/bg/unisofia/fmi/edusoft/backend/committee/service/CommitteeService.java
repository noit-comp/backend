package bg.unisofia.fmi.edusoft.backend.committee.service;

import java.util.Set;

public interface CommitteeService {

  Set<Long> getAllCommitteeIdsByProjectId(Long projectId);
}
