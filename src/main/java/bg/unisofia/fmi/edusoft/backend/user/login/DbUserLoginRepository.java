package bg.unisofia.fmi.edusoft.backend.user.login;

import bg.unisofia.fmi.edusoft.backend.jdbc.NamedParameterSupport;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class DbUserLoginRepository extends NamedParameterSupport<DbUserLogin> {

  public DbUserLoginRepository(DataSource dataSource) {
    super(dataSource, new DbUserLoginRowMapper());
  }

  public List<DbUserLogin> findAllByLoginTimeBetween(LocalDateTime start, LocalDateTime end) {
    final String sql = ""
        + "SELECT * FROM user_login "
        + "WHERE login_time >= :start AND login_time <= :end";
    final var params = Map.of("start", start, "end", end);
    return getNamedTemplate().query(sql, params, rowMapper);
  }

  public void insert(final Long userId, final LocalDateTime loginTime) {
    final String sql = ""
        + "INSERT INTO user_login ("
        + "  user_id, "
        + "  login_time"
        + ") VALUES ("
        + "  :user_id, "
        + "  :login_time"
        + ")";

    final var params = Map.of("user_id", userId, "login_time", loginTime);

    getNamedTemplate().update(sql, params);
  }

  private static final class DbUserLoginRowMapper
      implements RowMapper<DbUserLogin> {

    /**
     * Implementations must implement this method to map each row of data in the ResultSet. This
     * method should not call {@code next()} on the ResultSet; it is only supposed to map values of
     * the current row.
     *
     * @param rs     the ResultSet to map (pre-initialized for the current row)
     * @param rowNum the number of the current row
     * @return the result object for the current row (may be {@code null})
     * @throws SQLException if an SQLException is encountered getting column values (that is,
     *                      there's no need to catch SQLException)
     */
    @Override
    public DbUserLogin mapRow(ResultSet rs, int rowNum)
        throws SQLException {
      final Long id = rs.getLong("id");
      final Long userId = rs.getLong("user_id");
      final LocalDateTime loginTime = rs.getObject("login_time", LocalDateTime.class);

      return new DbUserLogin(id, userId, loginTime);
    }
  }

}
