package bg.unisofia.fmi.edusoft.backend.security;

import bg.unisofia.fmi.edusoft.backend.security.jjwt.JjwtUtil;
import bg.unisofia.fmi.edusoft.backend.user.domain.requests.UserTokenDetails;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
public class AuthenticationManager implements ReactiveAuthenticationManager {

  private final JjwtUtil jjwtUtil;

  public AuthenticationManager(JjwtUtil jjwtUtil) {
    this.jjwtUtil = jjwtUtil;
  }

  @Override
  public Mono<Authentication> authenticate(Authentication authentication) {
    final String authToken = authentication.getCredentials().toString();

    String username;
    try {
      username = jjwtUtil.getUsernameFromToken(authToken);
    } catch (Exception e) {
      // In this case maybe there is no token.
      username = null;
    }

    if (username != null && jjwtUtil.validateToken(authToken)) {
      UserTokenDetails userTokenDetails = jjwtUtil.getUserDetails(authToken);

      UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(
          userTokenDetails.getUsername(),
          userTokenDetails,
          userTokenDetails.getAuthorities()
      );
      auth.setDetails(userTokenDetails);
      return Mono.just(auth);
    }

    return Mono.empty();
  }
}
