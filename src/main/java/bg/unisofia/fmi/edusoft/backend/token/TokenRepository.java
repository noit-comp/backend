package bg.unisofia.fmi.edusoft.backend.token;

import bg.unisofia.fmi.edusoft.backend.jdbc.NamedParameterSupport;
import bg.unisofia.fmi.edusoft.backend.token.model.Token;
import bg.unisofia.fmi.edusoft.backend.token.type.TokenType;
import bg.unisofia.fmi.edusoft.backend.token.type.TokenTypeService;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;
import javax.sql.DataSource;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

@Repository
public class TokenRepository
    extends NamedParameterSupport<Token> {

  private final TokenTypeService tokenTypeService;

  public TokenRepository(DataSource dataSource,
      TokenTypeService tokenTypeService) {
    super(dataSource, new TokenRowMapper());
    this.tokenTypeService = tokenTypeService;
  }

  public Token findByValue(final String value) {
    final String sql = "SELECT * FROM token WHERE `value`=:value";
    final var params = Map.of("value", value);

    return getNamedTemplate().queryForObject(sql, params, rowMapper);
  }

  public void insert(final Token token) {
    final String sql = ""
        + "INSERT INTO token("
        + "  value, "
        + "  requested_by, "
        + "  created_on, "
        + "  type, "
        + "  expires_at, "
        + "  additional_data, "
        + "  used, "
        + "  send_to"
        + ") VALUES ("
        + "  :value, "
        + "  :requested_by, "
        + "  :created_on, "
        + "  :type, "
        + "  :expires_at, "
        + "  :additional_data, "
        + "  :used, "
        + "  :send_to"
        + ")";

    Long tokenTypeId = tokenTypeService.get(token.getType());
    final var params = Map.of(
        "value", token.getValue(),
        "requested_by", token.getRequestedBy(),
        "created_on", token.getCreatedOn(),
        "type", tokenTypeId,
        "expires_at", token.getExpiresAt(),
        "additional_data", token.getAdditionalData(),
        "used", token.getUsed(),
        "send_to", token.getSendTo());

    getNamedTemplate().update(sql, params);
  }

  public void use(final Long tokenId) {
    final String sql = "UPDATE token SET `used`=TRUE WHERE `id`=:tokenId";
    final var params = Map.of("tokenId", tokenId);

    getNamedTemplate().update(sql, params);
  }

  private static final class TokenRowMapper
      implements RowMapper<Token> {

    /**
     * Implementations must implement this method to map each row of data in the ResultSet. This
     * method should not call {@code next()} on the ResultSet; it is only supposed to map values of
     * the current row.
     *
     * @param rs     the ResultSet to map (pre-initialized for the current row)
     * @param rowNum the number of the current row
     * @return the result object for the current row (may be {@code null})
     * @throws SQLException if an SQLException is encountered getting column values (that is,
     *                      there's no need to catch SQLException)
     */
    @Override
    public Token mapRow(ResultSet rs, int rowNum)
        throws SQLException {
      final Long id = rs.getLong("id");
      final UUID value = rs.getObject("value", UUID.class);
      final Long requestedBy = rs.getLong("requested_by");
      final LocalDateTime createdOn = rs.getObject("created_on", LocalDateTime.class);
      final TokenType type = rs.getObject("type", TokenType.class);
      final LocalDateTime expiresAt = rs.getObject("expires_at", LocalDateTime.class);
      final Long additionalData = rs.getLong("additional_data");
      final Boolean used = rs.getBoolean("used");
      final String password = rs.getString("password");
      final String sendTo = rs.getString("send_to");

      return new Token(id, value, requestedBy, createdOn, type, expiresAt, additionalData, used,
          password, sendTo);
    }
  }

}
