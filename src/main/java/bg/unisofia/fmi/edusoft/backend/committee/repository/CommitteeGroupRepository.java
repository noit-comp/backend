package bg.unisofia.fmi.edusoft.backend.committee.repository;

import bg.unisofia.fmi.edusoft.backend.committee.domain.CommitteeGroup;
import bg.unisofia.fmi.edusoft.backend.jdbc.NamedParameterSupport;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class CommitteeGroupRepository
    extends NamedParameterSupport<CommitteeGroup> {

  public CommitteeGroupRepository(final DataSource dataSource) {
    super(dataSource, new CommitteeGroupRowMapper());
  }

  private static final class CommitteeGroupRowMapper
      implements RowMapper<CommitteeGroup> {

    /**
     * Implementations must implement this method to map each row of data in the ResultSet. This
     * method should not call {@code next()} on the ResultSet; it is only supposed to map values of
     * the current row.
     *
     * @param rs     the ResultSet to map (pre-initialized for the current row)
     * @param rowNum the number of the current row
     * @return the result object for the current row (may be {@code null})
     * @throws SQLException if an SQLException is encountered getting column values (that is,
     *                      there's no need to catch SQLException)
     */
    @Override
    public CommitteeGroup mapRow(ResultSet rs, int rowNum)
        throws SQLException {
      final Long id = rs.getLong("id");
      final String name = rs.getString("name");
      final Long president = rs.getLong("president");
      final Long round = rs.getLong("round");

      return new CommitteeGroup(id, name, president, round);
    }
  }

}
