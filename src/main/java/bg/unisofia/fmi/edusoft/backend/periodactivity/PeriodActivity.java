package bg.unisofia.fmi.edusoft.backend.periodactivity;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@AllArgsConstructor
@Getter
@EqualsAndHashCode
public class PeriodActivity {

  private final Long periodId;
  private final Long activityId;
}
