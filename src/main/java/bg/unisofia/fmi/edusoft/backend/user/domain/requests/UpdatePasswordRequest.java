package bg.unisofia.fmi.edusoft.backend.user.domain.requests;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@EqualsAndHashCode
@Getter
@NoArgsConstructor
public class UpdatePasswordRequest {

  @Size(min = 8, max = 128)
  @NotBlank
  private String newPassword;
  @Size(min = 8, max = 128)
  @NotBlank
  private String oldPassword;
}
