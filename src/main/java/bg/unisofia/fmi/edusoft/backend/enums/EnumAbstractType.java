package bg.unisofia.fmi.edusoft.backend.enums;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@AllArgsConstructor
@EqualsAndHashCode
@Getter
public abstract class EnumAbstractType {

  private final Long id;
  private final String value;

}
