package bg.unisofia.fmi.edusoft.backend.token.exception;

import bg.unisofia.fmi.edusoft.backend.exception.CustomException;
import java.util.Map;
import org.springframework.http.HttpStatus;

public abstract class TokenException
    extends CustomException {

  protected TokenException(
      HttpStatus httpStatus,
      String message,
      Map<String, Object> additionalInfo,
      Throwable cause
  ) {
    super(httpStatus, message, additionalInfo, cause);
  }
}
