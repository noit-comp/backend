package bg.unisofia.fmi.edusoft.backend.domain.db;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@AllArgsConstructor
@EqualsAndHashCode
@Getter
public class Technology {

  private Long id;
  private String name;

}
