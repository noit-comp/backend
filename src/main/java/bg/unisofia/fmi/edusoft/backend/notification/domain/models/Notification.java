package bg.unisofia.fmi.edusoft.backend.notification.domain.models;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@AllArgsConstructor
@Getter
@EqualsAndHashCode
public class Notification implements Serializable {
  @NotNull
  private Long id;
  @NotNull
  @Size(max = 128)
  private String title;
  @NotNull
  private String body;
  @NotNull
  private LocalDateTime creationTime;
  @NotNull
  private Long creatorId;
}
