package bg.unisofia.fmi.edusoft.backend.group.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import javax.naming.Name;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.ldap.odm.annotations.Attribute;
import org.springframework.ldap.odm.annotations.DnAttribute;
import org.springframework.ldap.odm.annotations.Entry;
import org.springframework.ldap.odm.annotations.Id;
import org.springframework.ldap.support.LdapUtils;

@Entry(
    base = "ou=Groups",
    objectClasses = {"groupOfNames"}
)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LdapGroup {

  @Id
  @JsonIgnore
  @NotBlank
  @NotNull
  private Name id;

  @Attribute(name = "cn")
  @DnAttribute(value = "cn", index = 1)
  private String name;

  @Attribute
  private String description;

  @Attribute(name = "member")
  private Set<String> members;

  /**
   * Generate ldap group with minimum info.
   *
   * @param groupName for the new user
   */
  public LdapGroup(String groupName) {
    this.id = LdapUtils.newLdapName("cn=" + groupName);
    this.name = groupName;
    this.description = "none";
    this.members = null;
  }

  /**
   * Add user to group.
   *
   * @param userDn the ldap user id
   * @return true if the user was successfully added to group(group added to memberOf). true if
   *     already a part of the group
   */
  public Boolean addUserToGroup(String userDn) {
    if (this.members == null) {
      this.members = new HashSet<>();
    }

    if (this.members.contains(userDn)) {
      return true;
    }

    return this.members.add(userDn);
  }

  /**
   * Remove user from group.
   *
   * @param userDn the ldap user id
   * @return true if user was not in group or has successfully left the group
   */
  public Boolean removeUserFromGroup(String userDn) {
    if (this.members == null || !this.members.contains(userDn)) {
      return true;
    }

    return this.members.remove(userDn);
  }

  /**
   * A getter for member.
   *
   * @return member the members of a group
   */
  public Set<String> getMembers() {
    if (this.members == null) {
      return new HashSet<>();
    }

    //Maps a member to only the uid value.
    //For example if we have uid=Person,ou=People....
    //It maps only to Person
    return this.members.stream()
        .map(t -> t.substring(4, t.indexOf(",")))
        .collect(Collectors.toSet());
  }
}
