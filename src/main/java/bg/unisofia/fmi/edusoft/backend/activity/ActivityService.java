package bg.unisofia.fmi.edusoft.backend.activity;

import bg.unisofia.fmi.edusoft.backend.enums.EnumAbstractService;
import bg.unisofia.fmi.edusoft.backend.period.Period;
import bg.unisofia.fmi.edusoft.backend.period.PeriodService;
import bg.unisofia.fmi.edusoft.backend.periodactivity.PeriodActivityService;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class ActivityService
    extends EnumAbstractService<ActivityEntity, Activity> {

  private final PeriodService periodService;
  private final PeriodActivityService periodActivityService;

  private List<Activity> currentActivitiesCached;

  public ActivityService(ActivityRepository activityRepository, PeriodService periodService,
      PeriodActivityService periodActivityService) {
    super(activityRepository, Activity.class);
    this.periodService = periodService;
    this.periodActivityService = periodActivityService;
  }

  @Override
  protected void customReload() {
    reloadStates();
  }

  @Scheduled(fixedRate = 60 * 1000L)
  private void reloadStates() {
    final List<Period> activePeriods = periodService.getActivePeriods();
    final Set<Long> activePeriodIds = activePeriods.stream()
        .map(Period::getId)
        .collect(Collectors.toSet());

    final Set<Long> activityIds
        = periodActivityService.getAllActivitiesIdsByPeriodIds(activePeriodIds);

    this.currentActivitiesCached = activityIds.stream()
        .map(this::getEnumValue)
        .collect(Collectors.toList());
  }

  public List<Activity> getCurrentActivities() {
    return Collections.unmodifiableList(currentActivitiesCached);
  }

}
