package bg.unisofia.fmi.edusoft.backend.security.jjwt.secrets;

import java.time.OffsetDateTime;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@AllArgsConstructor
@EqualsAndHashCode
@Getter
public class JjwtSecret {

  private final Long id;
  private final UUID secret;
  private final OffsetDateTime generatedOn;

}
