package bg.unisofia.fmi.edusoft.backend.exception;

public class MailRateLimitExceededException extends MailException {

  public MailRateLimitExceededException() {
    super();
  }

  public MailRateLimitExceededException(String message) {
    super(message);
  }

  public MailRateLimitExceededException(String message, Throwable cause) {
    super(message, cause);
  }

  public MailRateLimitExceededException(Throwable cause) {
    super(cause);
  }

  protected MailRateLimitExceededException(String message, Throwable cause,
      boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
