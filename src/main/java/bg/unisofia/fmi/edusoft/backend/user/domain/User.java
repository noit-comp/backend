package bg.unisofia.fmi.edusoft.backend.user.domain;

import bg.unisofia.fmi.edusoft.backend.user.domain.db.DbUser;
import bg.unisofia.fmi.edusoft.backend.user.domain.ldap.LdapUser;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;
import javax.naming.Name;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

@AllArgsConstructor
@NoArgsConstructor
@Setter
public class User implements UserDetails {

  private LdapUser ldapUser;
  private DbUser dbUser;

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    return ldapUser.getMemberOf().stream()
        .map(SimpleGrantedAuthority::new)
        .collect(Collectors.toList());
  }

  @JsonIgnore
  @Override
  public String getPassword() {
    return ldapUser.getPassword();
  }

  @Override
  public String getUsername() {
    return ldapUser.getUsername();
  }

  public String getFirstName() {
    return ldapUser.getFirstName();
  }

  public String getMiddleName() {
    return ldapUser.getMiddleName();
  }

  public String getLastName() {
    return ldapUser.getLastName();
  }

  public String getFullName() {
    return ldapUser.getFullName();
  }

  public Long getId() {
    return dbUser.getId();
  }

  public String getMail() {
    return ldapUser.getMail();
  }

  public Set<String> getGroupNamesUserIsMemberOf() {
    return ldapUser.getMemberOf();
  }

  public Name getUserLdapName() {
    return ldapUser.getId();
  }

  @Override
  public boolean isAccountNonExpired() {
    return true;
  }

  @Override
  public boolean isAccountNonLocked() {
    return true;
  }

  @Override
  public boolean isCredentialsNonExpired() {
    return true;
  }

  @Override
  public boolean isEnabled() {
    return true;
  }
}
