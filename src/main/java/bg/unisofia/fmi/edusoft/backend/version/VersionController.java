package bg.unisofia.fmi.edusoft.backend.version;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirements;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/version")
public class VersionController {

  private final VersionDto version;
  private final VersionSimpleDto versionSimple;

  public VersionController(VersionDto version,
      @Qualifier("versionSimpleDto") VersionSimpleDto versionSimple) {
    this.version = version;
    this.versionSimple = versionSimple;
  }

  /**
   * Prints all public version data.
   */
  @GetMapping
  @Operation(summary = "Basic version", description = "Get basic backend version", responses = {
      @ApiResponse(responseCode = "200", description = "Return version in json format")
  })
  @SecurityRequirements
  public Mono<VersionSimpleDto> publicVersionData() {
    return Mono.just(versionSimple);
  }

  /**
   * Print full version data.
   */
  @GetMapping("/full")
  @Operation(summary = "Full version",
      description = "Return json with all application data. Requires ROLE_SYSTEM_ADMIN",
      responses = {
          @ApiResponse(responseCode = "200", description = "Return version in json format")
      })
  @PreAuthorize("hasAuthority('ROLE_SYSTEM_ADMIN')")
  public Mono<VersionDto> fullEnvData() {
    //    // THIS CODE INSERT ALL ENV VARIABLES (INCLUDING PASSWORDS) INTO Properties
    //    Properties props = new Properties();
    //    MutablePropertySources propSrcs = ((AbstractEnvironment) env).getPropertySources();
    //    StreamSupport.stream(propSrcs.spliterator(), false)
    //        .filter(ps -> ps instanceof EnumerablePropertySource)
    //        .map(ps -> ((EnumerablePropertySource) ps).getPropertyNames())
    //        .flatMap(Arrays::<String>stream)
    //        .forEach(propName -> props.setProperty(propName, env.getProperty(propName)));
    //    return new ResponseEntity(props, HttpStatus.OK);
    //    // END OF SPECIAL CODE

    return Mono.just(version);
  }
}
