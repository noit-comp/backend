package bg.unisofia.fmi.edusoft.backend.region;

import bg.unisofia.fmi.edusoft.backend.jdbc.NamedParameterSupport;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class RegionRepository
    extends NamedParameterSupport<Region> {

  public RegionRepository(DataSource dataSource) {
    super(dataSource, new RegionRowMapper());
  }

  public List<Region> getAllRegionByCityIdIn(final Collection<Long> cityIds) {
    final String sql = "SELECT * FROM region WHERE id IN ("
        + "SELECT DISTINCT(c.region) FROM city c WHERE c.id IN (:cityIds)"
        + ")";
    final var params = Map.of("cityIds", cityIds);
    return getNamedTemplate().query(sql, params, rowMapper);
  }

  private static final class RegionRowMapper
      implements RowMapper<Region> {

    /**
     * Implementations must implement this method to map each row of data in the ResultSet. This
     * method should not call {@code next()} on the ResultSet; it is only supposed to map values of
     * the current row.
     *
     * @param rs     the ResultSet to map (pre-initialized for the current row)
     * @param rowNum the number of the current row
     * @return the result object for the current row (may be {@code null})
     * @throws SQLException if an SQLException is encountered getting column values (that is,
     *                      there's no need to catch SQLException)
     */
    @Override
    public Region mapRow(ResultSet rs, int rowNum)
        throws SQLException {
      final Long id = rs.getLong("id");
      final String name = rs.getString("name");

      return new Region(id, name);
    }
  }
}
