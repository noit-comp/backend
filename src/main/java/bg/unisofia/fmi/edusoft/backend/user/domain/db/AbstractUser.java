package bg.unisofia.fmi.edusoft.backend.user.domain.db;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@AllArgsConstructor
@EqualsAndHashCode
@Getter
public class AbstractUser {

  private final Long id;
  private final Long user;
  private final Long competition;
  private final String abstractKeyType;
  private final Long abstractKeyValue;

}
