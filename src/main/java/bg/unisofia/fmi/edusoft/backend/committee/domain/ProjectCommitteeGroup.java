package bg.unisofia.fmi.edusoft.backend.committee.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@AllArgsConstructor
@EqualsAndHashCode
@Getter
public class ProjectCommitteeGroup {

  private final Long committeeGroup;
  private final Long project;

}
