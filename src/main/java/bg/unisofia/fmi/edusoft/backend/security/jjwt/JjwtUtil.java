package bg.unisofia.fmi.edusoft.backend.security.jjwt;

import bg.unisofia.fmi.edusoft.backend.user.domain.User;
import bg.unisofia.fmi.edusoft.backend.user.domain.requests.UserTokenDetails;
import java.util.Date;

public interface JjwtUtil {

  String getUsernameFromToken(String token);

  Date getExpirationDateFromToken(String token);

  UserTokenDetails getUserDetails(final String token);

  String generateToken(final User user);

  Boolean validateToken(String token);
}
