package bg.unisofia.fmi.edusoft.backend.token.exception;

import org.springframework.http.HttpStatus;

public class ExpiredTokenException
    extends TokenException {

  public ExpiredTokenException() {
    super(HttpStatus.GONE, "Token expired", null, null);
  }

}
