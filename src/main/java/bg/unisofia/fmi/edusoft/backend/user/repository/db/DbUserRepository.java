package bg.unisofia.fmi.edusoft.backend.user.repository.db;

import bg.unisofia.fmi.edusoft.backend.jdbc.NamedParameterSupport;
import bg.unisofia.fmi.edusoft.backend.user.domain.db.DbUser;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

@Repository
public class DbUserRepository extends NamedParameterSupport<DbUser> {

  public DbUserRepository(final DataSource dataSource) {
    super(dataSource, new DbUserRowMapper());
  }

  public List<DbUser> findAllById(final Collection<Long> userIds) {
    if (userIds.isEmpty()) {
      return List.of();
    }

    final String sql = "SELECT * FROM users WHERE id IN (:userIds)";
    final var params = Map.of("userIds", userIds);

    return getNamedTemplate().query(sql, params, rowMapper);
  }

  public DbUser findById(final Long userId) {
    final String sql = "SELECT * FROM users WHERE id = :userId";
    final Map<String, Object> params = Map.of("userId", userId);

    return findObject(sql, params);
  }

  public List<DbUser> findAllByUsernameIn(final Collection<String> usernames) {
    if (usernames.isEmpty()) {
      return List.of();
    }

    final String sql = "SELECT * FROM users WHERE username IN (:usernames)";
    final var params = Map.of("usernames", usernames);

    return getNamedTemplate().query(sql, params, rowMapper);
  }

  public DbUser findByUsername(final String username) {
    final String sql = "SELECT * FROM users WHERE username = :username";
    final Map<String, Object> params = Map.of("username", username);

    return findObject(sql, params);
  }

  public Long insertWithUsername(final String username) {
    final String sql = ""
        + "INSERT INTO users ("
        + "  username, "
        + "  confirmed_mail, "
        + "  created_at"
        + ") VALUES ("
        + "  :username, "
        + "  FALSE, "
        + "  NOW()"
        + ")";
    final var params = Map.of("username", username);

    KeyHolder holder = new GeneratedKeyHolder();
    getNamedTemplate().update(sql, new MapSqlParameterSource(params), holder);

    Map<String, Object> keys = holder.getKeys();
    if (keys == null) {
      throw new DataRetrievalFailureException(
          "The holder couldn't retrieve the keys of the table.");
    }

    final Long userId = Long.parseLong(keys.get("id").toString());
    return userId;
  }

  public void confirmMailByUserId(final Long userId) {
    final String sql = ""
        + " UPDATE users "
        + " SET "
        + "  confirmed_mail = TRUE"
        + " WHERE "
        + "  id = :id ";

    final Map<String, Object> params = Map.of("id", userId);
    getNamedTemplate().update(sql, params);
  }

  private static final class DbUserRowMapper
      implements RowMapper<DbUser> {

    /**
     * Implementations must implement this method to map each row of data in the ResultSet. This
     * method should not call {@code next()} on the ResultSet; it is only supposed to map values of
     * the current row.
     *
     * @param rs     the ResultSet to map (pre-initialized for the current row)
     * @param rowNum the number of the current row
     * @return the result object for the current row (may be {@code null})
     * @throws SQLException if an SQLException is encountered getting column values (that is,
     *                      there's no need to catch SQLException)
     */
    @Override
    public DbUser mapRow(ResultSet rs, int rowNum)
        throws SQLException {
      final Long id = rs.getLong("id");
      final String username = rs.getString("username");
      final Boolean confirmedMail = rs.getBoolean("confirmed_mail");
      final LocalDateTime createdAt = rs.getObject("created_at", LocalDateTime.class);
      return new DbUser(id, username, confirmedMail, createdAt);
    }
  }
}
