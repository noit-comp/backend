package bg.unisofia.fmi.edusoft.backend.domain.db;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@AllArgsConstructor
@Getter
@EqualsAndHashCode
public class Criteria {

  private Long id;
  private String name;
  private Long maxPt;
  private Long category;

}
