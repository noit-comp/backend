package bg.unisofia.fmi.edusoft.backend.notification.repository;

import bg.unisofia.fmi.edusoft.backend.jdbc.NamedParameterSupport;
import bg.unisofia.fmi.edusoft.backend.notification.domain.models.Notification;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

@Repository
public class NotificationRepository extends NamedParameterSupport<Notification> {

  public NotificationRepository(final DataSource dataSource) {
    super(dataSource, new NotificationRowMapper());
  }

  public List<Notification> findAllByIds(Collection<Long> notificationIds) {
    if (notificationIds.isEmpty()) {
      return new ArrayList<>();
    }

    final String sql = "SELECT * FROM notification WHERE id IN (:notificationIds)";
    final var params = Map.of("notificationIds", notificationIds);

    return getNamedTemplate().query(sql, params, rowMapper);
  }

  public Notification findById(final Long notificationId) {
    final String sql = "SELECT * FROM notification WHERE id = :notificationId";
    final Map<String, Object> params = Map.of("notificationId", notificationId);

    return findObject(sql, params);
  }

  public Long insert(final String title, final String body, final Long creatorId) {
    final String sql = ""
        + "INSERT INTO notification ("
        + "  title, "
        + "  body, "
        + "  creation_time,"
        + "  creator_id"
        + ") VALUES ("
        + "  :title, "
        + "  :body, "
        + "  NOW(),"
        + "  :creatorId"
        + ")";
    final var params = Map.of(
        "title", title,
        "body", body,
        "creatorId", creatorId);

    KeyHolder holder = new GeneratedKeyHolder();
    getNamedTemplate().update(sql, new MapSqlParameterSource(params), holder, new String[] {"id"});

    Number notificationId = holder.getKey();
    if (notificationId == null) {
      throw new DataRetrievalFailureException(
          "The holder couldn't retrieve the keys of the table.");
    }
    return notificationId.longValue();
  }

  private static final class NotificationRowMapper
      implements RowMapper<Notification> {

    /**
     * Implementations must implement this method to map each row of data in the ResultSet. This
     * method should not call {@code next()} on the ResultSet; it is only supposed to map values of
     * the current row.
     *
     * @param rs     the ResultSet to map (pre-initialized for the current row)
     * @param rowNum the number of the current row
     * @return the result object for the current row (may be {@code null})
     * @throws SQLException if an SQLException is encountered getting column values (that is,
     *                      there's no need to catch SQLException)
     */
    @Override
    public Notification mapRow(ResultSet rs, int rowNum)
        throws SQLException {
      final Long id = rs.getLong("id");
      final String title = rs.getString("title");
      final String body = rs.getString("body");
      final LocalDateTime creationTime = rs.getObject("creation_time", LocalDateTime.class);
      final Long creatorId = rs.getLong("creator_id");

      return new Notification(id, title, body, creationTime, creatorId);
    }
  }
}
