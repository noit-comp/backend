package bg.unisofia.fmi.edusoft.backend.user.domain.ldap;

import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import javax.naming.Name;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.ldap.odm.annotations.Attribute;
import org.springframework.ldap.odm.annotations.Attribute.Type;
import org.springframework.ldap.odm.annotations.DnAttribute;
import org.springframework.ldap.odm.annotations.Entry;
import org.springframework.ldap.odm.annotations.Id;
import org.springframework.ldap.support.LdapUtils;

@Entry(
    base = "ou=People",
    objectClasses = {"inetOrgPerson", "organizationalPerson", "person", "top"}
)
@Getter
@NoArgsConstructor
@Setter
public class LdapUser implements Serializable {

  @Id
  private Name id;

  @Attribute(name = "uid")
  @DnAttribute(value = "uid", index = 1)
  @NotBlank
  @NotNull
  private String username;

  @Attribute(name = "cn")
  @NotBlank
  @NotNull
  private String fullName;

  @Attribute(name = "givenName")
  @NotBlank
  @NotNull
  private String firstName;

  @Attribute(name = "displayName")
  @NotBlank
  @NotNull
  private String middleName;

  @Attribute(name = "sn")
  @NotBlank
  @NotNull
  private String lastName;

  private String mail;

  @Attribute(name = "userPassword", type = Type.BINARY)
  @NotBlank
  @NotNull
  private byte[] password;

  @Attribute(readonly = true)
  private Set<String> memberOf;

  /**
   * A getter for memberOf(user rights).
   *
   * @return memberOf current user rights
   */
  public Set<String> getMemberOf() {
    if (this.memberOf == null) {
      return new HashSet<>();
    }

    //Maps group to only the cn value.
    //For example if we have cn=Group,ou=Groups....
    //It maps only to Group
    return this.memberOf.stream()
        .map(t -> t.substring(3, t.indexOf(",")))
        .collect(Collectors.toSet());
  }

  /**
   * Generate ldap user with minimum info + groups.
   *
   * @param username   for the new user
   * @param firstName  first name
   * @param middleName middle name
   * @param lastName   last name
   * @param mail       mail of the new user
   * @param password   encrypted password of the new user
   */
  public LdapUser(String username, String firstName, String middleName, String lastName,
      String mail, String password) {
    this.id = LdapUtils.newLdapName("uid=" + username);
    this.username = username;
    this.firstName = firstName;
    this.middleName = middleName;
    this.lastName = lastName;
    this.mail = mail;
    this.fullName = firstName + " " + middleName + " " + lastName;
    setPassword(password);
  }

  public String getPassword() {
    return new String(this.password, StandardCharsets.UTF_8);
  }

  /**
   * Set password to user.
   *
   * @param password encrypted user password
   */
  public void setPassword(String password) {
    this.password = password.getBytes(StandardCharsets.UTF_8);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    LdapUser ldapUser = (LdapUser) o;
    return id.equals(ldapUser.id)
        && username.equals(ldapUser.username)
        && fullName.equals(ldapUser.fullName)
        && firstName.equals(ldapUser.firstName)
        && middleName.equals(ldapUser.middleName)
        && lastName.equals(ldapUser.lastName)
        && Objects.equals(mail, ldapUser.mail)
        && Arrays.equals(password, ldapUser.password)
        && Objects.equals(memberOf, ldapUser.memberOf);
  }

  @Override
  public int hashCode() {
    int result = Objects.hash(id, username, fullName, firstName, middleName, lastName, mail, 
        memberOf);
    result = 31 * result + Arrays.hashCode(password);
    return result;
  }
}
