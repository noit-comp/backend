package bg.unisofia.fmi.edusoft.backend.utility;

import static org.springframework.core.annotation.AnnotationUtils.getAnnotation;

import bg.unisofia.fmi.edusoft.backend.group.domain.LdapGroup;
import bg.unisofia.fmi.edusoft.backend.user.domain.ldap.LdapUser;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;
import javax.naming.Name;
import org.springframework.ldap.odm.annotations.Entry;
import org.springframework.ldap.support.LdapNameBuilder;

public class LdapHelper {

  public static String getBaseFromLdapEntry(Class clazz) {
    Entry annotation = getAnnotation(clazz, Entry.class);
    String base = "";

    if (annotation != null) {
      base = annotation.base();
    }

    return base;
  }

  private static Name generateLdapIdFromIdentifier(
      Class ldapStructure,
      String formatting,
      String identifier
  ) {
    return LdapNameBuilder.newInstance()
        .add(LdapHelper.getBaseFromLdapEntry(ldapStructure))
        .add(String.format(formatting, identifier))
        .build();
  }

  private static Set<Name> generateLdapIdsFromIdentifiers(
      Class ldapStructure,
      String formatting,
      Collection<String> identifiers
  ) {
    Set<Name> ldapIds =
        identifiers.stream()
            .map(identifier -> generateLdapIdFromIdentifier(ldapStructure, formatting, identifier))
            .collect(Collectors.toSet());

    return ldapIds;
  }

  /**
   * Helper function for finding entry base from LdapUser. Uses reflection.
   */
  public static Set<Name> getLdapIdsFromUsernames(Collection<String> usernames) {
    return generateLdapIdsFromIdentifiers(LdapUser.class, "uid=%s", usernames);
  }

  /**
   * Helper function for finding entry base from LdapGroup. Uses reflection.
   */
  public static Set<Name> getLdapIdsFromGroupNames(Collection<String> groupNames) {
    return generateLdapIdsFromIdentifiers(LdapGroup.class, "cn=%s", groupNames);
  }
}
