package bg.unisofia.fmi.edusoft.backend.enums;

import bg.unisofia.fmi.edusoft.backend.jdbc.NamedParameterSupport;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.jdbc.core.RowMapper;

public abstract class EnumAbstractRepository<T extends EnumAbstractType>
    extends NamedParameterSupport<T> {

  private final String tableName;

  protected EnumAbstractRepository(
      DataSource dataSource,
      String tableName,
      RowMapper<T> rowMapper
  ) {
    super(dataSource, rowMapper);
    this.tableName = tableName;
  }

  protected static Long getId(final ResultSet rs) throws SQLException {
    return rs.getLong("id");
  }

  protected static String getValue(final ResultSet rs) throws SQLException {
    return rs.getString("value");
  }

  public List<T> findAll() {
    final String sql = "SELECT * FROM " + tableName + " ORDER BY id";
    return getNamedTemplate().query(sql, rowMapper);
  }
}
