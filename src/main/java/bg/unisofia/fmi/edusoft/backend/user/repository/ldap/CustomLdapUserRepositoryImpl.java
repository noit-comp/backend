package bg.unisofia.fmi.edusoft.backend.user.repository.ldap;

import bg.unisofia.fmi.edusoft.backend.user.domain.ldap.LdapUser;
import bg.unisofia.fmi.edusoft.backend.utility.LdapHelper;
import javax.naming.Name;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.support.LdapNameBuilder;
import org.springframework.stereotype.Repository;


@Repository
public class CustomLdapUserRepositoryImpl implements CustomLdapUserRepository {

  private final LdapTemplate ldapTemplate;

  public CustomLdapUserRepositoryImpl(LdapTemplate ldapTemplate) {
    this.ldapTemplate = ldapTemplate;
  }

  @Override
  public Boolean authenticate(String username, String password) {

    String base = LdapHelper.getBaseFromLdapEntry(LdapUser.class);
    Name dn = LdapNameBuilder.newInstance().add(base).build();
    String filter = "(uid=" + username + ")";

    return ldapTemplate.authenticate(dn, filter, password);
  }
}
