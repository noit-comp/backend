package bg.unisofia.fmi.edusoft.backend.group;

public interface GroupNameGenerationService {

  String generateGroupForCompetition(CompetitionRole role, Long competitionId);

  String generateGroupForCompetition(CompetitionRole role, Long competitionId, Long additional);

  String generateGroupForCommittee(Long committeeId);

  String generateGroupForProject(ProjectRoles role, Long projectId);
}
