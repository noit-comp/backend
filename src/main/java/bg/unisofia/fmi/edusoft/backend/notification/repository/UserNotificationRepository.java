package bg.unisofia.fmi.edusoft.backend.notification.repository;

import bg.unisofia.fmi.edusoft.backend.jdbc.NamedParameterSupport;
import bg.unisofia.fmi.edusoft.backend.notification.domain.models.UserNotification;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class UserNotificationRepository extends NamedParameterSupport<UserNotification> {

  public UserNotificationRepository(final DataSource dataSource) {
    super(dataSource, new UserNotificationRowMapper());
  }

  public UserNotification findByNotificationIdAndUserId(
      final Long notificationId,
      final Long userId
  ) {
    final String sql = ""
        + "SELECT * "
        + "FROM user_notification "
        + "WHERE user_id = :userId "
        + "AND notification_id = :notificationId ";

    final Map<String, Object> params = Map.of(
        "notificationId", notificationId,
        "userId", userId);

    return findObject(sql, params);
  }

  public List<UserNotification> findAllByUserIdAndLimitAndOffset(
      final Long userId,
      final int limit,
      final Long offset
  ) {
    final String sql = ""
        + "SELECT un.* "
        + "FROM user_notification AS un "
        + "WHERE un.user_id = :userId "
        + "ORDER BY start_sending_email DESC "
        + "LIMIT :limit OFFSET :offset";

    final var params = Map.of(
        "userId", userId,
        "limit", limit,
        "offset", offset);

    return getNamedTemplate().query(sql, params, rowMapper);
  }

  public Long getCountByUserId(final Long userId) {
    final String sql = ""
        + "SELECT COUNT(un.*) "
        + "FROM user_notification AS un "
        + "WHERE un.user_id = :userId ";

    final var params = Map.of("userId", userId);

    return getNamedTemplate().queryForObject(sql, params, Long.class);
  }

  public void updateStatusByNotificationIdAndUserId(
      final Long notificationId,
      final Long userId,
      final String status
  ) {
    final String sql = ""
        + "UPDATE user_notification "
        + "SET status = :status::notification_read_type "
        + "WHERE notification_id = :notificationId "
        + "AND user_id = :userId";

    final var params = Map.of(
        "status", status,
        "notificationId", notificationId,
        "userId", userId);

    getNamedTemplate().update(sql, params);
  }

  public void updateEmailSentByNotificationIdAndUserId(
      final Long notificationId,
      final Long userId,
      final LocalDateTime emailSent
  ) {
    final String sql = ""
        + "UPDATE user_notification "
        + "SET email_sent = :emailSent "
        + "WHERE notification_id = :notificationId "
        + "AND user_id = :userId";

    final var params = Map.of(
        "emailSent", emailSent,
        "notificationId", notificationId,
        "userId", userId);

    getNamedTemplate().update(sql, params);
  }

  public void insert(
      final Long notificationId,
      final Long userId,
      final String status,
      final LocalDateTime startSendingEmail
  ) {
    final String sql = ""
        + "INSERT INTO user_notification ("
        + "  notification_id, "
        + "  user_id, "
        + "  status, "
        + "  start_sending_email"
        + ") VALUES ("
        + "  :notificationId, "
        + "  :userId, "
        + "  :status::notification_read_type, "
        + "  :startSendingEmail"
        + ")";
    final var params = Map.of(
        "notificationId", notificationId,
        "userId", userId,
        "status", status,
        "startSendingEmail", startSendingEmail);

    getNamedTemplate().update(sql, params);
  }

  private static final class UserNotificationRowMapper
      implements RowMapper<UserNotification> {

    /**
     * Implementations must implement this method to map each row of data in the ResultSet. This
     * method should not call {@code next()} on the ResultSet; it is only supposed to map values of
     * the current row.
     *
     * @param rs     the ResultSet to map (pre-initialized for the current row)
     * @param rowNum the number of the current row
     * @return the result object for the current row (may be {@code null})
     * @throws SQLException if an SQLException is encountered getting column values (that is,
     *                      there's no need to catch SQLException)
     */
    @Override
    public UserNotification mapRow(ResultSet rs, int rowNum)
        throws SQLException {
      final Long notificationId = rs.getLong("notification_id");
      final Long userId = rs.getLong("user_id");
      final String status = rs.getString("status");
      final LocalDateTime startSendingEmail =
          rs.getObject("start_sending_email", LocalDateTime.class);
      final LocalDateTime emailSent = rs.getObject("email_sent", LocalDateTime.class);

      return new UserNotification(notificationId, userId, status, startSendingEmail, emailSent);
    }
  }
}
