package bg.unisofia.fmi.edusoft.backend.config;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeIn;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.responses.ApiResponse;
import io.swagger.v3.oas.models.responses.ApiResponses;
import io.swagger.v3.oas.models.servers.Server;
import org.springdoc.core.customizers.OpenApiCustomiser;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

@Configuration
@PropertySource(value = "classpath:application.yml")
@OpenAPIDefinition(
    info = @Info(
        title = "NOIT Competition system",
        license = @License(name = "GNU GPL v3.0"),
        contact = @Contact(name = "Petar Toshev", email = "pecata.toshev@gmail.com"),
        version = "${app.version}"
    ),
    security = {@SecurityRequirement(name = RestApiDocs.DEFAULT_SECURITY_NAME)}
)
@SecurityScheme(
    type = SecuritySchemeType.HTTP,
    scheme = "bearer",
    name = RestApiDocs.DEFAULT_SECURITY_NAME,
    in = SecuritySchemeIn.HEADER,
    bearerFormat = "Bearer JWT_API_TOKEN",
    description = "requires login via /auth/login endpoint"
)
public class RestApiDocs {

  private final Environment environment;

  public static final String DEFAULT_SECURITY_NAME = "BearerAuth";

  private static final ApiResponse FORBIDDEN = new ApiResponse()
      .description("Authorized user does NOT have enough rights to complete requested operation");
  private static final ApiResponse UNAUTHORIZED = new ApiResponse()
      .description("Missing authorization. This resource needs authorization.");
  private static final ApiResponse UNPROCESSABLE_ENTITY = new ApiResponse()
      .description("Missing parameter in a login or register user request");

  public RestApiDocs(Environment environment) {
    this.environment = environment;
  }

  /**
  * Default api responses.
  */
  @Bean
  public OpenApiCustomiser customerGlobalHeaderOpenApiCustomizer() {
    return openApi -> {
      openApi.getPaths().values()
          .forEach(pathItem -> pathItem.readOperations().forEach(operation -> {
            ApiResponses apiResponses = operation.getResponses();
            apiResponses.addApiResponse("401", UNAUTHORIZED);
            apiResponses.addApiResponse("403", FORBIDDEN);
            apiResponses.addApiResponse("422", UNPROCESSABLE_ENTITY);
          }));
    };
  }

  /**
  * Adding server url, that will be used from springdoc api-docs for sending example requests.
  */
  @Bean
  public OpenAPI serverRequestsListCustomizer() {
    String docsServerUrl = environment.getProperty("springdoc.server");

    return new OpenAPI().addServersItem(new Server().url(docsServerUrl));
  }
}
