package bg.unisofia.fmi.edusoft.backend.project.domain;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@AllArgsConstructor
@EqualsAndHashCode
@Getter
public class StudentProject {

  private final Long id;
  private final Long grade;
  private final Long user;
  private final Long school;
  private final Long project;

}
