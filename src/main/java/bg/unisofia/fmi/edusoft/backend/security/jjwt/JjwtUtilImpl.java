package bg.unisofia.fmi.edusoft.backend.security.jjwt;

import bg.unisofia.fmi.edusoft.backend.security.jjwt.secrets.JjwtSecretService;
import bg.unisofia.fmi.edusoft.backend.user.domain.User;
import bg.unisofia.fmi.edusoft.backend.user.domain.requests.UserTokenDetails;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class JjwtUtilImpl implements JjwtUtil {

  private static final String USER_DETAILS = "user";

  private final JjwtSecretService jjwtSecretService;
  private final ObjectMapper objectMapper;

  @Value("${app.jwt.validityInMs}")
  private long jwtValidityInMs;

  /**
   * Constructor.
   */
  public JjwtUtilImpl(JjwtSecretService jjwtSecretService, ObjectMapper objectMapper) {
    this.jjwtSecretService = jjwtSecretService;
    this.objectMapper = objectMapper;
  }

  private Claims getAllClaimsFromToken(String token) {
    return Jwts.parser()
        .setSigningKey(jjwtSecretService.getEncodedSecret())
        .parseClaimsJws(token)
        .getBody();
  }

  @Override
  public String getUsernameFromToken(String token) {
    return getAllClaimsFromToken(token).getSubject();
  }

  @Override
  public Date getExpirationDateFromToken(String token) {
    return getAllClaimsFromToken(token).getExpiration();
  }

  @Override
  public UserTokenDetails getUserDetails(String token) {

    Claims claims = getAllClaimsFromToken(token);

    try {
      String details = claims.get(USER_DETAILS, String.class);
      return objectMapper.readValue(details, UserTokenDetails.class);
    } catch (JsonProcessingException ignored) {
      // ignored parsing exception
    }

    UserTokenDetails userTokenDetails = new UserTokenDetails();
    userTokenDetails.setUsername(claims.getIssuer());
    userTokenDetails.setRoles(new HashSet<>());
    return userTokenDetails;
  }

  private String doGenerateToken(Map<String, Object> claims, String username) {
    final Date createdDate = new Date();
    final Date expirationDate = new Date(createdDate.getTime() + jwtValidityInMs);
    return Jwts.builder()
        .setClaims(claims)
        .setSubject(username)
        .setIssuedAt(createdDate)
        .setExpiration(expirationDate)
        .signWith(SignatureAlgorithm.HS512, jjwtSecretService.getEncodedSecret())
        .compact();
  }

  @SneakyThrows
  @Override
  public String generateToken(final User user) {
    UserTokenDetails userTokenDetails = new UserTokenDetails(user);
    String userTokenDetailsString = objectMapper.writeValueAsString(userTokenDetails);

    Map<String, Object> claims = new HashMap<>();
    claims.put(USER_DETAILS, userTokenDetailsString);
    return doGenerateToken(claims, user.getUsername());
  }

  @Override
  public Boolean validateToken(String token) {
    return !isTokenExpired(token);
  }

  private Boolean isTokenExpired(String token) {
    final Date expiration = getExpirationDateFromToken(token);
    return expiration.before(new Date());
  }
}
