package bg.unisofia.fmi.edusoft.backend.token.model;

import bg.unisofia.fmi.edusoft.backend.token.type.TokenType;
import java.time.LocalDateTime;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@AllArgsConstructor
@EqualsAndHashCode
@Getter
public class Token {

  private final Long id;
  private final UUID value;
  private final Long requestedBy;
  private final LocalDateTime createdOn;
  private final TokenType type;
  private final LocalDateTime expiresAt;
  private final Long additionalData;
  private final Boolean used;
  private final String password;
  private final String sendTo;

  /**
   * Easier way to create a token.
   */
  public Token(
      TokenType tokenType,
      String recipient,
      Long requestedBy,
      Long additionalData,
      Long validityInMinutes,
      String encodedPassword
  ) {
    this.id = null;
    this.value = UUID.randomUUID();
    this.requestedBy = requestedBy;
    this.createdOn = LocalDateTime.now();
    this.type = tokenType;
    this.expiresAt = this.createdOn.plusMinutes(validityInMinutes);
    this.additionalData = additionalData;
    this.used = false;
    this.password = encodedPassword;
    this.sendTo = recipient;
  }

}
