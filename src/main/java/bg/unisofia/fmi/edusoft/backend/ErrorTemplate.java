package bg.unisofia.fmi.edusoft.backend;

import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
@AllArgsConstructor
public class ErrorTemplate {

  HttpStatus httpStatus;
  String message;
  Map<String, Object> additionalInfo;
}

