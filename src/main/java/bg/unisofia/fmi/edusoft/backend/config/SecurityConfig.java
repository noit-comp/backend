package bg.unisofia.fmi.edusoft.backend.config;

import bg.unisofia.fmi.edusoft.backend.security.AuthenticationManager;
import bg.unisofia.fmi.edusoft.backend.security.SecurityContextRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.method.configuration.EnableReactiveMethodSecurity;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.web.server.SecurityWebFilterChain;
import reactor.core.publisher.Mono;

@Configuration
@EnableWebFluxSecurity
@EnableReactiveMethodSecurity
public class SecurityConfig {

  private final AuthenticationManager authenticationManager;
  private final SecurityContextRepository securityContextRepository;

  public SecurityConfig(AuthenticationManager authenticationManager,
      SecurityContextRepository securityContextRepository) {
    this.authenticationManager = authenticationManager;
    this.securityContextRepository = securityContextRepository;
  }

  /**
   * Spring security webflux configuration.
   */
  @Bean
  public SecurityWebFilterChain securityWebFilterChain(ServerHttpSecurity http) {
    return http
        .exceptionHandling()
        .authenticationEntryPoint((swe, e) -> {
          return Mono.fromRunnable(() -> {
            swe.getResponse().setStatusCode(HttpStatus.UNAUTHORIZED);
          });
        }).accessDeniedHandler((swe, e) -> {
          return Mono.fromRunnable(() -> {
            swe.getResponse().setStatusCode(HttpStatus.FORBIDDEN);
          });
        }).and()
        .cors().and()
        .csrf().disable()
        .formLogin().disable()
        .httpBasic().disable()
        .authenticationManager(authenticationManager)
        .securityContextRepository(securityContextRepository)
        .authorizeExchange()
        .pathMatchers(HttpMethod.OPTIONS).permitAll()
        .pathMatchers(HttpMethod.POST, "/auth/login", "/auth/register").permitAll()
        .pathMatchers(HttpMethod.GET, "/public/**").permitAll()
        .pathMatchers(HttpMethod.GET, "/version").permitAll()
        .pathMatchers(HttpMethod.GET,
            "/api-docs/**", "/swagger-ui", "/webjars/swagger-ui/**").permitAll()
        .pathMatchers(HttpMethod.POST, "/token/utilize/not-logged-in").permitAll()
        .anyExchange().authenticated()
        .and().build();
  }
}