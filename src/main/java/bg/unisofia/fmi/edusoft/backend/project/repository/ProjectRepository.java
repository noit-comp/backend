package bg.unisofia.fmi.edusoft.backend.project.repository;


import bg.unisofia.fmi.edusoft.backend.jdbc.NamedParameterSupport;
import bg.unisofia.fmi.edusoft.backend.project.domain.Project;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class ProjectRepository
    extends NamedParameterSupport<Project> {

  public ProjectRepository(DataSource dataSource) {
    super(dataSource, new ProjectRowMapper());
  }

  public Project findById(final Long projectId) {
    final String sql = "SELECT * FROM project WHERE id IN (:projectId)";
    final Map<String, Object> params = Map.of("projectId", projectId);
    return getNamedTemplate().queryForObject(sql, params, rowMapper);
  }

  private static final class ProjectRowMapper
      implements RowMapper<Project> {

    /**
     * Implementations must implement this method to map each row of data in the ResultSet. This
     * method should not call {@code next()} on the ResultSet; it is only supposed to map values of
     * the current row.
     *
     * @param rs     the ResultSet to map (pre-initialized for the current row)
     * @param rowNum the number of the current row
     * @return the result object for the current row (may be {@code null})
     * @throws SQLException if an SQLException is encountered getting column values (that is,
     *                      there's no need to catch SQLException)
     */
    @Override
    public Project mapRow(ResultSet rs, int rowNum)
        throws SQLException {

      final Long id = rs.getLong("id");
      final String name = rs.getString("name");
      final Long category = rs.getLong("project");
      final Long competition = rs.getLong("competition");
      final String shortDescription = rs.getString("short_description");
      final String fullDescription = rs.getString("full_description");

      return new Project(id, name, shortDescription, fullDescription, category, competition);
    }
  }
}
