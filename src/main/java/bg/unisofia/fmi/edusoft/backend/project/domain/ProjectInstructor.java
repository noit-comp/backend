package bg.unisofia.fmi.edusoft.backend.project.domain;

import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@AllArgsConstructor
@EqualsAndHashCode
@Getter
public class ProjectInstructor {

  private final Long project;
  private final Long instructor;
  private final Boolean primary;
  private final Boolean accepted;
  private final LocalDateTime rejectValidity;

}
