package bg.unisofia.fmi.edusoft.backend.mail.repository;

import bg.unisofia.fmi.edusoft.backend.enums.EnumAbstractRepository;
import bg.unisofia.fmi.edusoft.backend.mail.domain.MailTemplateEntity;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class MailTemplateRepository
    extends EnumAbstractRepository<MailTemplateEntity> {

  protected MailTemplateRepository(DataSource dataSource) {
    super(dataSource, "n_mail_template", new MailTemplateRowMapper());
  }

  private static final class MailTemplateRowMapper
      implements RowMapper<MailTemplateEntity> {

    /**
     * Implementations must implement this method to map each row of data in the ResultSet. This
     * method should not call {@code next()} on the ResultSet; it is only supposed to map values of
     * the current row.
     *
     * @param rs     the ResultSet to map (pre-initialized for the current row)
     * @param rowNum the number of the current row
     * @return the result object for the current row (may be {@code null})
     * @throws SQLException if an SQLException is encountered getting column values (that is,
     *                      there's no need to catch SQLException)
     */
    @Override
    public MailTemplateEntity mapRow(ResultSet rs, int rowNum)
        throws SQLException {

      final Long id = getId(rs);
      final String value = getValue(rs);
      final String subject = rs.getString("subject");
      final String mailBody = rs.getString("mail_body");

      return new MailTemplateEntity(id, value, subject, mailBody);
    }
  }
}
