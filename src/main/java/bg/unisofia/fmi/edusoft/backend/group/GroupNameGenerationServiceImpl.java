package bg.unisofia.fmi.edusoft.backend.group;

import org.springframework.stereotype.Service;

@Service
public class GroupNameGenerationServiceImpl implements GroupNameGenerationService {

  @Override
  public String generateGroupForCompetition(CompetitionRole role, Long competitionId) {
    return String.format("competition_%d__%s", competitionId, role.name().toLowerCase());
  }

  @Override
  public String generateGroupForCompetition(CompetitionRole role, Long competitionId,
      Long additional) {
    return generateGroupForCompetition(role, competitionId).concat("_" + additional);
  }

  @Override
  public String generateGroupForCommittee(Long committeeId) {
    return String.format("judge_%d", committeeId);
  }

  @Override
  public String generateGroupForProject(ProjectRoles role, Long projectId) {
    return String.format("project_%d__%s", projectId, role.name().toLowerCase());
  }
}
