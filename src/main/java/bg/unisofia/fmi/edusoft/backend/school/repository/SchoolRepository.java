package bg.unisofia.fmi.edusoft.backend.school.repository;

import bg.unisofia.fmi.edusoft.backend.jdbc.NamedParameterSupport;
import bg.unisofia.fmi.edusoft.backend.school.domain.School;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class SchoolRepository
    extends NamedParameterSupport<School> {

  public SchoolRepository(final DataSource dataSource) {
    super(dataSource, new SchoolRowMapper());
  }

  public List<School> findAllSchoolsForProject(final Long projectId) {
    final String sql = "SELECT * FROM school WHERE id IN ("
        + "SELECT DISTINCT(sp.school) FROM student_project WHERE sp.project = :projectId"
        + ")";
    final var params = Map.of("projectId", projectId);
    return getNamedTemplate().query(sql, params, rowMapper);
  }

  private static final class SchoolRowMapper
      implements RowMapper<School> {

    /**
     * Implementations must implement this method to map each row of data in the ResultSet. This
     * method should not call {@code next()} on the ResultSet; it is only supposed to map values of
     * the current row.
     *
     * @param rs     the ResultSet to map (pre-initialized for the current row)
     * @param rowNum the number of the current row
     * @return the result object for the current row (may be {@code null})
     * @throws SQLException if an SQLException is encountered getting column values (that is,
     *                      there's no need to catch SQLException)
     */
    @Override
    public School mapRow(ResultSet rs, int rowNum)
        throws SQLException {
      final Long id = rs.getLong("id");
      final String name = rs.getString("name");
      final Long city = rs.getLong("city");

      return new School(id, name, city);
    }
  }
}
