package bg.unisofia.fmi.edusoft.backend.security;

import org.springframework.security.core.Authentication;
import reactor.core.publisher.Mono;

public interface PermissionService {
  Mono<Boolean> accessProject(Authentication authentication, Long projectId);
}
