package bg.unisofia.fmi.edusoft.backend.user.domain.requests;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@EqualsAndHashCode
@Getter
@NoArgsConstructor
public class UpdateMailRequest {

  @Pattern(regexp = "^.+@.+\\..+$")
  @NotBlank
  private String newMail;
}
