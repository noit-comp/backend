package bg.unisofia.fmi.edusoft.backend.exception;

import java.util.Map;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class CustomException extends RuntimeException {

  HttpStatus httpStatus;
  Map<String, Object> additionalInfo;

  public CustomException() {

  }

  public CustomException(
      HttpStatus httpStatus,
      String message,
      Map<String, Object> additionalInfo,
      Throwable cause
  ) {
    super(message, cause);
    this.httpStatus = httpStatus;
    this.additionalInfo = additionalInfo;
  }

}
