package bg.unisofia.fmi.edusoft.backend.committee.repository;

import bg.unisofia.fmi.edusoft.backend.committee.domain.ProjectCommitteeGroup;
import bg.unisofia.fmi.edusoft.backend.jdbc.NamedParameterSupport;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class ProjectCommitteeGroupRepository
    extends NamedParameterSupport<ProjectCommitteeGroup> {

  public ProjectCommitteeGroupRepository(final DataSource dataSource) {
    super(dataSource, new ProjectCommitteeGroupRowMapper());
  }

  public List<ProjectCommitteeGroup> findAllByProjectIdIn(final Collection<Long> projectIds) {
    final String sql = "SELECT * FROM committee_group WHERE project IN (:projectIds)";
    final Map<String, Object> params = Map.of("projectIds", projectIds);
    return getNamedTemplate().query(sql, params, rowMapper);
  }

  private static final class ProjectCommitteeGroupRowMapper
      implements RowMapper<ProjectCommitteeGroup> {

    /**
     * Implementations must implement this method to map each row of data in the ResultSet. This
     * method should not call {@code next()} on the ResultSet; it is only supposed to map values of
     * the current row.
     *
     * @param rs     the ResultSet to map (pre-initialized for the current row)
     * @param rowNum the number of the current row
     * @return the result object for the current row (may be {@code null})
     * @throws SQLException if an SQLException is encountered getting column values (that is,
     *                      there's no need to catch SQLException)
     */
    @Override
    public ProjectCommitteeGroup mapRow(ResultSet rs, int rowNum)
        throws SQLException {

      final Long committeeGroup = rs.getLong("committee_group");
      final Long project = rs.getLong("project");

      return new ProjectCommitteeGroup(committeeGroup, project);
    }
  }
}
