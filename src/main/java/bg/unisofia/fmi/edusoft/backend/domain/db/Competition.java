package bg.unisofia.fmi.edusoft.backend.domain.db;

import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;


@AllArgsConstructor
@EqualsAndHashCode
@Getter
public class Competition {

  private Long id;
  @Size(max = 128)
  private String name;
  private Long period;

}
