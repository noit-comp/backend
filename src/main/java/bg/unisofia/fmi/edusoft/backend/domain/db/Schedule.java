package bg.unisofia.fmi.edusoft.backend.domain.db;

import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@AllArgsConstructor
@EqualsAndHashCode
@Getter
public class Schedule {

  private Long id;
  private Long project;
  private Long committeeGroup;
  private Long number;
  private LocalDateTime startTime;
  private LocalDateTime endTime;

}
