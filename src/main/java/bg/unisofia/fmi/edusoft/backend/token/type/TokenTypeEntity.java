package bg.unisofia.fmi.edusoft.backend.token.type;

import bg.unisofia.fmi.edusoft.backend.enums.EnumAbstractType;
import lombok.EqualsAndHashCode;
import lombok.Getter;


@Getter
@EqualsAndHashCode(callSuper = true)
public class TokenTypeEntity
    extends EnumAbstractType {

  private final boolean requireLogin;
  private final long validityInMinutes;
  private final boolean requirePassword;

  /**
   * Constructor with super.
   */
  public TokenTypeEntity(Long id, String value, boolean requireLogin, long validityInMinutes,
      boolean requirePassword) {
    super(id, value);
    this.requireLogin = requireLogin;
    this.validityInMinutes = validityInMinutes;
    this.requirePassword = requirePassword;
  }

}
