package bg.unisofia.fmi.edusoft.backend.user.controller;

import bg.unisofia.fmi.edusoft.backend.exception.MailAlreadyExistsException;
import bg.unisofia.fmi.edusoft.backend.exception.MissingElementException;
import bg.unisofia.fmi.edusoft.backend.exception.UsernameAlreadyExistsException;
import bg.unisofia.fmi.edusoft.backend.security.jjwt.JjwtUtil;
import bg.unisofia.fmi.edusoft.backend.user.domain.User;
import bg.unisofia.fmi.edusoft.backend.user.domain.requests.AuthenticationLoginRequest;
import bg.unisofia.fmi.edusoft.backend.user.domain.requests.RegisterUserRequest;
import bg.unisofia.fmi.edusoft.backend.user.domain.requests.UserDetailsResponse;
import bg.unisofia.fmi.edusoft.backend.user.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirements;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

@RestController
@RequestMapping("/auth")
public class AuthController {

  private final JjwtUtil jjwtUtil;
  private final UserService userService;

  public AuthController(JjwtUtil jjwtUtil, UserService userService) {
    this.jjwtUtil = jjwtUtil;
    this.userService = userService;
  }

  /**
   * Login endpoint.
   */
  @Operation(summary = "Login endpoint", description = "Returns jwt token for authentication")
  @PostMapping("/login")
  //Set security to null for swagger(Removes the authentication requirement/lock).
  @SecurityRequirements()
  public ResponseEntity<String> login(
      @Valid @RequestBody AuthenticationLoginRequest authenticationLoginRequest) {

    try {
      User userDetails = userService.authenticate(authenticationLoginRequest);
      return ResponseEntity.ok(jjwtUtil.generateToken(userDetails));
    } catch (Exception e) {
      return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }
  }

  /**
   * registration endpoint.
   */
  @ApiResponse(responseCode = "409",
      description = "Email or username is taken. Return mail or username",
      content = @Content(mediaType = MediaType.TEXT_PLAIN_VALUE)
  )
  @Operation(summary = "Register new user", description = "Creates new user")
  @PostMapping("/register")
  //Set security to null for swagger(Removes the authentication requirement/lock).
  @SecurityRequirements()
  public ResponseEntity<Object> register(
      @Valid @RequestBody RegisterUserRequest registerUserRequest,
      UriComponentsBuilder b) {

    UriComponents uriComponents = b.path("/user/{username}")
        .buildAndExpand(registerUserRequest.getUsername());

    try {
      userService.register(registerUserRequest);
      return ResponseEntity.created(uriComponents.toUri()).build();
    } catch (MailAlreadyExistsException e) {
      return ResponseEntity.status(HttpStatus.CONFLICT).body("Mail already exists");
    } catch (UsernameAlreadyExistsException e) {
      return ResponseEntity.status(HttpStatus.CONFLICT).body("Username already exists");
    } catch (Exception e) {
      return ResponseEntity.status(HttpStatus.CONFLICT).body(e.getMessage());
    }
  }

  /**
   * Authenticated user data endpoint.
   */
  @GetMapping("/user")
  @Operation(summary = "User data", description = "Gets logged user data")
  public ResponseEntity<Object> me(Authentication authentication) {
    try {
      final String username = String.valueOf(authentication.getPrincipal());
      final User user = userService
          .findByUsernameOrMail(username);
      final UserDetailsResponse userDetailsResponse =
          new UserDetailsResponse(user);

      return ResponseEntity.ok(userDetailsResponse);
    } catch (MissingElementException e) {
      return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
    }
  }
}
