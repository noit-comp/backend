package bg.unisofia.fmi.edusoft.backend.mail.service;

import bg.unisofia.fmi.edusoft.backend.enums.EnumAbstractService;
import bg.unisofia.fmi.edusoft.backend.mail.domain.MailTemplateEntity;
import bg.unisofia.fmi.edusoft.backend.mail.domain.MailTemplateType;
import bg.unisofia.fmi.edusoft.backend.mail.repository.MailTemplateRepository;
import org.springframework.stereotype.Service;

@Service
public class MailTemplateService extends EnumAbstractService<MailTemplateEntity, MailTemplateType> {

  public MailTemplateService(
      MailTemplateRepository mailTemplateRepository) {
    super(mailTemplateRepository, MailTemplateType.class);
  }
}
