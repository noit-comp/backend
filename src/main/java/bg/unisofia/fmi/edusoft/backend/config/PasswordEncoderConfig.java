package bg.unisofia.fmi.edusoft.backend.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.Pbkdf2PasswordEncoder;
import org.springframework.security.crypto.password.Pbkdf2PasswordEncoder.SecretKeyFactoryAlgorithm;

@Configuration
public class PasswordEncoderConfig {

  /**
   * Configures pbkdf2 password encoder to work with sha512.
   *
   * @return configured password encoder
   */
  @Bean
  public PasswordEncoder passwordEncoder() {
    Pbkdf2PasswordEncoder encoder = new Pbkdf2PasswordEncoder();
    encoder.setAlgorithm(SecretKeyFactoryAlgorithm.PBKDF2WithHmacSHA512);

    return encoder;
  }
}