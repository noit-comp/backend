package bg.unisofia.fmi.edusoft.backend.config;

import bg.unisofia.fmi.edusoft.backend.ErrorTemplate;
import bg.unisofia.fmi.edusoft.backend.exception.CustomException;
import bg.unisofia.fmi.edusoft.backend.exception.UnauthorizedException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Map;
import org.springframework.boot.web.reactive.error.ErrorWebExceptionHandler;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DefaultDataBufferFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

@Configuration
@Order(-2)
public class GlobalErrorHandlerConfig implements ErrorWebExceptionHandler {

  private final ObjectMapper objectMapper;

  GlobalErrorHandlerConfig(ObjectMapper objectMapper) {
    this.objectMapper = objectMapper;
  }

  private Mono<Void> createErrorTemplate(ServerWebExchange serverWebExchange, HttpStatus httpStatus,
      String message, Map<String, Object> additionalProperties) {
    ServerHttpResponse serverHttpResponse = serverWebExchange.getResponse();
    serverHttpResponse.setStatusCode(httpStatus);
    serverHttpResponse.getHeaders().setContentType(MediaType.APPLICATION_JSON);
    try {
      DataBuffer db = new DefaultDataBufferFactory()
          .wrap(this.objectMapper
              .writeValueAsBytes(new ErrorTemplate(httpStatus, message, additionalProperties)));
      return serverHttpResponse.writeWith(Mono.just(db));
    } catch (JsonProcessingException e) {
      //no need to log this
    }
    return Mono.empty();
  }

  @Override
  public Mono<Void> handle(ServerWebExchange serverWebExchange, Throwable throwable) {

    if (throwable instanceof CustomException) {
      CustomException customException = (CustomException) throwable;
      return createErrorTemplate(serverWebExchange, customException.getHttpStatus(),
          customException.getMessage(), customException.getAdditionalInfo());
    }

    if (throwable instanceof UnauthorizedException) {
      return createErrorTemplate(serverWebExchange, HttpStatus.UNAUTHORIZED, "Unauthorized",
          Map.of("Error", throwable.getClass()));
    }

    return Mono.error(throwable);
  }
}
