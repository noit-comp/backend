package bg.unisofia.fmi.edusoft.backend.domain.db;

import java.time.OffsetDateTime;
import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class UpdateMail {

  private Long id;
  private String newMail;
  private Long userId;
  private Boolean confirmed;
  private OffsetDateTime generatedOn;

}
