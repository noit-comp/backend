package bg.unisofia.fmi.edusoft.backend.school.domain;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@AllArgsConstructor
@EqualsAndHashCode
@Getter
public class School {

  private final Long id;
  private final String name;
  private final Long city;

}
