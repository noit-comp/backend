package bg.unisofia.fmi.edusoft.backend.user.domain.requests;

import bg.unisofia.fmi.edusoft.backend.user.domain.User;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@AllArgsConstructor
@EqualsAndHashCode
@Getter
public class UserDetailsResponse {
  private final String username;
  private final String firstName;
  private final String lastName;
  private final String mail;

  public UserDetailsResponse(User user) {
    this.username = user.getUsername();
    this.firstName = user.getFirstName();
    this.lastName = user.getLastName();
    this.mail = user.getMail();
  }
}
