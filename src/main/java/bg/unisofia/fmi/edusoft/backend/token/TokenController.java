package bg.unisofia.fmi.edusoft.backend.token;


import bg.unisofia.fmi.edusoft.backend.ErrorTemplate;
import bg.unisofia.fmi.edusoft.backend.exception.InvalidAuthenticationException;
import bg.unisofia.fmi.edusoft.backend.token.exception.InvalidTokenCredentialsException;
import bg.unisofia.fmi.edusoft.backend.token.exception.MissingTokenException;
import bg.unisofia.fmi.edusoft.backend.token.exception.UsedTokenException;
import bg.unisofia.fmi.edusoft.backend.token.service.TokenConsumerService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import java.util.HashMap;
import java.util.UUID;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/tokens")
public class TokenController {

  private final TokenConsumerService tokenConsumerService;

  public TokenController(TokenConsumerService tokenConsumerService) {
    this.tokenConsumerService = tokenConsumerService;
  }

  private ResponseEntity<Object> exceptionHandler(
      Throwable e,
      HttpStatus httpStatus,
      String message
  ) {
    HashMap<String, Object> additionalProperties = new HashMap<>();
    additionalProperties.put("Error", e.getClass());
    return ResponseEntity.status(httpStatus)
        .body(new ErrorTemplate(httpStatus, message,
            additionalProperties));
  }

  /**
   * Utilize endpoint when logging in is required.
   */
  @ApiResponses({
      @ApiResponse(responseCode = "400",
          description = "Token password is incorrect."
      ),
      @ApiResponse(responseCode = "404",
          description = "Token not found."
      ),
      @ApiResponse(responseCode = "410",
          description = "Token expired or was already used."
      ),
      @ApiResponse(responseCode = "401",
          description = "Must be logged in to use token."
      )
  })
  @Operation(
      summary = "Utilize token",
      description = "Utilize given token"
  )
  @PostMapping("/{tokenUuid}/utilize")
  public ResponseEntity<Object> utilizeToken(
      @PathVariable UUID tokenUuid,
      @RequestBody TokenRequest body,
      Authentication authentication
  ) {
    if (authentication != null && !authentication.isAuthenticated()) {
      return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
          .body("Log in to be able to use this token");
    }
    try {
      tokenConsumerService.utilize(authentication, tokenUuid, body.getPassword());
    } catch (MissingTokenException e) {
      return exceptionHandler(e, HttpStatus.NOT_FOUND, "Token not found");
    } catch (InvalidTokenCredentialsException e) {
      return exceptionHandler(e, HttpStatus.BAD_REQUEST, "Token password is incorrect");
    } catch (UsedTokenException e) {
      return exceptionHandler(e, HttpStatus.GONE, "Token expired or was already used");
    } catch (InvalidAuthenticationException e) {
      return exceptionHandler(e, HttpStatus.UNAUTHORIZED, "Missing Authentication");
    }
    return ResponseEntity.accepted().build();
  }

  @ApiResponses({
      @ApiResponse(responseCode = "400",
          description = "Token password is incorrect."
      ),
      @ApiResponse(responseCode = "404",
          description = "No such token."
      ),
      @ApiResponse(responseCode = "410",
          description = "Token expired or were already used."
      )
  })
  @Operation(
      summary = "Utilize token",
      description = "Utilize given token"
  )
  @PostMapping("/{tokenUuid}/utilize/not-logged-in")
  public ResponseEntity<Object> utilizeTokenWithoutAuthorization(
      @PathVariable UUID tokenUuid,
      @RequestBody TokenRequest  body
  ) {
    return this.utilizeToken(tokenUuid, body, null);
  }
}
