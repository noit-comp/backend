package bg.unisofia.fmi.edusoft.backend.role;

import bg.unisofia.fmi.edusoft.backend.exception.MissingElementException;

public interface RoleService {
  /**
   * Gives a user a role.
   *
   * @param role name of the role that will be assigned
   * @param username  username of the user to get the role
   * @return Was the user's role successfully added
   */
  Boolean addRoleToUser(String role, String username) throws MissingElementException;

  /**
   * Remove user role.
   *
   * @param role name of the role that will be removed
   * @param username  username of the user that will lose the role
   * @return Was the user's role successfully removed
   */
  Boolean removeRoleFromUser(String role, String username) throws MissingElementException;
}
