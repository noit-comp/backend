package bg.unisofia.fmi.edusoft.backend.version;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@EqualsAndHashCode
@Getter
public class VersionSimpleDto {

  @Value("${app.version:MissingAppVersion}")
  private String appVersion;

}
