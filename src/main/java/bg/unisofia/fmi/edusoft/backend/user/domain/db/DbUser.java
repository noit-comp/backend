package bg.unisofia.fmi.edusoft.backend.user.domain.db;

import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@AllArgsConstructor
@Getter
@EqualsAndHashCode
public class DbUser implements Serializable {

  private Long id;
  private String username;
  private Boolean confirmedMail;
  private LocalDateTime createdAt;

}
