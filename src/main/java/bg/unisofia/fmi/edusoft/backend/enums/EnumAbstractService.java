package bg.unisofia.fmi.edusoft.backend.enums;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.scheduling.annotation.Scheduled;

public abstract class EnumAbstractService<
    EntityT extends EnumAbstractType,
    EnumTypeT extends Enum<EnumTypeT>>
    extends GenericEnumAbstractService<EntityT, EnumTypeT>
    implements ApplicationListener<ContextRefreshedEvent> {

  private final EnumAbstractRepository<EntityT> enumAbstractRepository;

  /**
   * Abstract enum tables service.
   */
  public EnumAbstractService(
      final EnumAbstractRepository<EntityT> enumAbstractRepository,
      final Class<EnumTypeT> enumClass) {

    super(enumClass);
    this.enumAbstractRepository = enumAbstractRepository;
  }

  @Override
  public void onApplicationEvent(final ContextRefreshedEvent event) {
    reload();
  }

  @Scheduled(cron = "10 3 * * * *")
  public void reload() {
    load(enumAbstractRepository.findAll());
    customReload();
  }

  protected void customReload() {}
}
