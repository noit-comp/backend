package bg.unisofia.fmi.edusoft.backend.token.service;

import bg.unisofia.fmi.edusoft.backend.exception.InvalidAuthenticationException;
import bg.unisofia.fmi.edusoft.backend.token.TokenRepository;
import bg.unisofia.fmi.edusoft.backend.token.exception.ExpiredTokenException;
import bg.unisofia.fmi.edusoft.backend.token.exception.InvalidTokenCredentialsException;
import bg.unisofia.fmi.edusoft.backend.token.exception.MissingTokenException;
import bg.unisofia.fmi.edusoft.backend.token.exception.UsedTokenException;
import bg.unisofia.fmi.edusoft.backend.token.model.Token;
import bg.unisofia.fmi.edusoft.backend.token.type.TokenType;
import bg.unisofia.fmi.edusoft.backend.token.type.TokenTypeEntity;
import bg.unisofia.fmi.edusoft.backend.token.type.TokenTypeService;
import bg.unisofia.fmi.edusoft.backend.user.service.UserService;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.UUID;
import java.util.function.Consumer;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class TokenConsumerServiceImpl implements TokenConsumerService {
  private final PasswordEncoder passwordEncoder;
  private final TokenRepository tokenRepository;
  private final UserService userService;
  private final TokenTypeService tokenTypeService;
  private final Map<TokenType, Consumer<Long>> tokenTypeConsumerMap;

  /**
   * Implementation of TokenConsumerService.
   */
  public TokenConsumerServiceImpl(
      PasswordEncoder passwordEncoder,
      TokenRepository tokenRepository,
      UserService userService,
      TokenTypeService tokenTypeService) {
    this.passwordEncoder = passwordEncoder;
    this.tokenRepository = tokenRepository;
    this.userService = userService;
    this.tokenTypeService = tokenTypeService;
    this.tokenTypeConsumerMap = Map.of(TokenType.CONFIRM_EMAIL, userService::confirmedMail);
  }

  private Token getToken(UUID tokenId) {
    final Token token = tokenRepository.findByValue(tokenId.toString());
    if (token == null) {
      throw new MissingTokenException();
    }
    if (token.getUsed()) {
      throw new UsedTokenException();
    }
    if (token.getExpiresAt().isBefore(LocalDateTime.now())) {
      throw new ExpiredTokenException();
    }
    return token;
  }

  private void checkPassword(Token token, String password)
      throws InvalidTokenCredentialsException {
    if (!passwordEncoder.matches(password, token.getPassword())) {
      throw new InvalidTokenCredentialsException();
    }
  }

  private void checkAuthentication(Authentication authentication)
      throws InvalidAuthenticationException {
    if (authentication == null) {
      throw new InvalidAuthenticationException();
    }
  }

  @Override
  public void utilize(Authentication authentication, UUID tokenId, String password)
      throws InvalidTokenCredentialsException, InvalidAuthenticationException {

    final Token token = getToken(tokenId);
    final TokenTypeEntity tokenTypeEntity = tokenTypeService.getEntity(token.getType());

    if (tokenTypeEntity.isRequireLogin()) {
      checkAuthentication(authentication);
    }

    if (tokenTypeEntity.isRequirePassword()) {
      checkPassword(token, password);
    }

    Consumer<Long> consumer = tokenTypeConsumerMap.get(token.getType());
    if (consumer == null) {
      throw new RuntimeException("Missing token type method");
    }

    consumer.accept(token.getAdditionalData());
    tokenRepository.use(token.getId());
  }
}
