package bg.unisofia.fmi.edusoft.backend.region;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@AllArgsConstructor
@EqualsAndHashCode
@Getter
public class Region {

  private final Long id;
  private final String name;

}
