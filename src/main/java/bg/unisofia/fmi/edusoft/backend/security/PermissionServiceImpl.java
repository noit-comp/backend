package bg.unisofia.fmi.edusoft.backend.security;

import bg.unisofia.fmi.edusoft.backend.committee.service.CommitteeService;
import bg.unisofia.fmi.edusoft.backend.group.CompetitionRole;
import bg.unisofia.fmi.edusoft.backend.group.GroupNameGenerationService;
import bg.unisofia.fmi.edusoft.backend.group.ProjectRoles;
import bg.unisofia.fmi.edusoft.backend.project.domain.Project;
import bg.unisofia.fmi.edusoft.backend.project.service.ProjectService;
import bg.unisofia.fmi.edusoft.backend.region.Region;
import bg.unisofia.fmi.edusoft.backend.region.RegionService;
import bg.unisofia.fmi.edusoft.backend.school.domain.School;
import bg.unisofia.fmi.edusoft.backend.school.service.SchoolService;
import bg.unisofia.fmi.edusoft.backend.user.domain.requests.UserTokenDetails;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
@Slf4j
public class PermissionServiceImpl implements PermissionService {

  private static final String PROJECT_ACL_LOGGER_PREFIX = "Project has permission";
  private final GroupNameGenerationService groupService;
  private final CommitteeService committeeService;
  private final ProjectService projectService;
  private final RegionService regionService;
  private final SchoolService schoolService;

  /**
   * Class for granting permissions.
   */
  public PermissionServiceImpl(GroupNameGenerationService groupService,
      CommitteeService committeeService,
      ProjectService projectService,
      RegionService regionService,
      SchoolService schoolService) {
    this.groupService = groupService;
    this.committeeService = committeeService;
    this.projectService = projectService;
    this.regionService = regionService;
    this.schoolService = schoolService;
  }

  @Override
  public Mono<Boolean> accessProject(Authentication authentication, Long projectId) {
    UserTokenDetails userTokenDetails = (UserTokenDetails) authentication.getDetails();
    final Set<String> roles = userTokenDetails.getRoles();

    if (hasProjectGroupAccess(roles, projectId)
        || hasProjectCommitteeAccess(roles, projectId)) {

      projectAclLoggerResult(projectId, true);
      return Mono.just(true);
    }

    Project project = projectService.getProjectById(projectId);
    if (project == null) {
      projectAclLoggerResult(projectId, null);
      return Mono.just(false);
    }

    if (hasProjectExpertAccess(roles, project)
        || hasProjectSchoolOrRegionAccess(roles, project)
    ) {
      projectAclLoggerResult(projectId, true);
      return Mono.just(true);
    }

    projectAclLoggerResult(projectId, false);
    return Mono.just(false);
  }

  // region Project permissions
  private void projectAclLoggerCheck(Long projectId, ProjectAclLoggerType type) {
    log.info(PROJECT_ACL_LOGGER_PREFIX + " check {type: {}, id: {}}", type.name(), projectId);
  }

  private void projectAclLoggerResult(Long projectId, Boolean success) {
    log.info(PROJECT_ACL_LOGGER_PREFIX + " access {} for id {}",
        success == null ? "NO_SUCH_ID" : (success ? "GRANTED" : "DENIED"),
        projectId);
  }

  private boolean hasProjectGroupAccess(final Set<String> roles, Long projectId) {
    projectAclLoggerCheck(projectId, ProjectAclLoggerType.GROUPS);
    List<String> projectGroups = EnumSet.allOf(ProjectRoles.class)
        .stream()
        .map(r -> groupService.generateGroupForProject(r, projectId))
        .collect(Collectors.toList());

    return !Collections.disjoint(roles, projectGroups);
  }

  // Get all committees attached to the project and check if our user has any
  private Boolean hasProjectCommitteeAccess(
      final Set<String> roles,
      final Long projectId
  ) {
    projectAclLoggerCheck(projectId, ProjectAclLoggerType.COMMITTEE);
    return committeeService.getAllCommitteeIdsByProjectId(projectId)
        .stream()
        .map(groupService::generateGroupForCommittee)
        .anyMatch(roles::contains);
  }

  private boolean hasProjectExpertAccess(final Set<String> roles, Project project) {
    projectAclLoggerCheck(project.getId(), ProjectAclLoggerType.EXPERT);
    return roles.contains(
        groupService.generateGroupForCompetition(CompetitionRole.EXPERT, project.getCompetition()));
  }

  private Boolean hasProjectSchoolOrRegionAccess(final Set<String> roles, final Project project) {
    projectAclLoggerCheck(project.getId(), ProjectAclLoggerType.SCHOOL_AND_REGION);

    final List<School> schools = schoolService.findAllSchoolsByProject(project.getId());
    if (hasSchoolAccess(schools, roles, project)) {
      return true;
    }

    final List<Long> cityIds = schools.stream().map(School::getCity).collect(Collectors.toList());
    final List<Region> regions = regionService.getAllRegionByCityIdIn(cityIds);
    return hasRegionAccess(regions, roles, project);
  }

  private boolean hasSchoolAccess(
      final Collection<School> schools,
      final Set<String> roles,
      final Project project
  ) {

    return schools.stream()
        .map(School::getId)
        .map(schoolId -> groupService.generateGroupForCompetition(
            CompetitionRole.SCHOOL,
            project.getCompetition(),
            schoolId
        ))
        .anyMatch(roles::contains);
  }

  private boolean hasRegionAccess(
      final List<Region> regions,
      final Set<String> roles,
      final Project project
  ) {

    return regions.stream()
        .map(Region::getId)
        .map(regionId -> groupService.generateGroupForCompetition(
            CompetitionRole.REGION,
            project.getCompetition(),
            regionId
        ))
        .anyMatch(roles::contains);
  }
  // endregion

  private enum ProjectAclLoggerType {
    GROUPS,
    COMMITTEE,
    SCHOOL_AND_REGION,
    EXPERT
  }
}
