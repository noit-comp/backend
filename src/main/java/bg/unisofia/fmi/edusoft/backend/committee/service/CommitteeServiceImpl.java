package bg.unisofia.fmi.edusoft.backend.committee.service;

import bg.unisofia.fmi.edusoft.backend.committee.domain.ProjectCommitteeGroup;
import bg.unisofia.fmi.edusoft.backend.committee.repository.ProjectCommitteeGroupRepository;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

@Service
public class CommitteeServiceImpl implements CommitteeService {

  // TODO: When adding new committeeGroup every member (also president)
  //  should get committeeGroupInto his groups

  private final ProjectCommitteeGroupRepository projectCommitteeGroupRepository;

  public CommitteeServiceImpl(ProjectCommitteeGroupRepository projectCommitteeGroupRepository) {
    this.projectCommitteeGroupRepository = projectCommitteeGroupRepository;
  }

  @Override
  public Set<Long> getAllCommitteeIdsByProjectId(final Long projectId) {
    return projectCommitteeGroupRepository.findAllByProjectIdIn(Set.of(projectId))
        .stream()
        .map(ProjectCommitteeGroup::getCommitteeGroup)
        .collect(Collectors.toUnmodifiableSet());
  }
}
