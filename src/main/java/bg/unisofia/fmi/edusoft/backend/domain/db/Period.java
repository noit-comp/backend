package bg.unisofia.fmi.edusoft.backend.domain.db;

import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@AllArgsConstructor
@EqualsAndHashCode
@Getter
public class Period {

  private Long id;
  private LocalDateTime from;
  private LocalDateTime to;

}
