package bg.unisofia.fmi.edusoft.backend.user.service;

import bg.unisofia.fmi.edusoft.backend.exception.InvalidLdapCredentialsException;
import bg.unisofia.fmi.edusoft.backend.exception.MailAlreadyExistsException;
import bg.unisofia.fmi.edusoft.backend.exception.MissingElementException;
import bg.unisofia.fmi.edusoft.backend.exception.UsernameAlreadyExistsException;
import bg.unisofia.fmi.edusoft.backend.user.domain.User;
import bg.unisofia.fmi.edusoft.backend.user.domain.ldap.LdapUser;
import bg.unisofia.fmi.edusoft.backend.user.domain.requests.AuthenticationLoginRequest;
import bg.unisofia.fmi.edusoft.backend.user.domain.requests.RegisterUserRequest;
import bg.unisofia.fmi.edusoft.backend.user.domain.requests.UpdateMailRequest;
import bg.unisofia.fmi.edusoft.backend.user.domain.requests.UpdatePasswordRequest;
import bg.unisofia.fmi.edusoft.backend.user.domain.requests.UserTokenDetails;
import java.util.Collection;
import java.util.List;

public interface UserService {

  /**
   * Authenticate user and return combined user data (ldap + db).
   *
   * <p>Should check credentials for user in ldap and then (if ok) create proxy record (if missing)
   * in db</p>
   *
   * @param authenticationLoginRequest credentials
   * @return User or InvalidCredentialsException
   */
  User authenticate(final AuthenticationLoginRequest authenticationLoginRequest)
      throws MissingElementException, InvalidLdapCredentialsException;

  /**
   * Find and return combined user data (ldap + db).
   *
   * @param key searching parameter
   * @return User or MissingElementException
   */
  User findByUsernameOrMail(final String key) throws MissingElementException;

  /**
   * Registers user into ldap and send activation email.
   *
   * @param registerUserRequest registrant credentials
   * @return registered user or instance of ElementAlreadyExistsException
   */
  User register(RegisterUserRequest registerUserRequest)
      throws UsernameAlreadyExistsException, MailAlreadyExistsException;

  /**
   * Change the user password.
   *
   * @param username  credentials
   * @param updatePasswordRequest  new credentials
   * @return LdapUser with changed attribute when successful
   *         MissingElementException when there is no user to update
   */
  LdapUser updatePassword(String username, UpdatePasswordRequest updatePasswordRequest)
      throws MissingElementException;

  /**
   * Add the request to updateMail table.
   *
   * @param userTokenDetails credentials
   * @param updateMailRequest new credentials
   */
  void requestMailUpdate(UserTokenDetails userTokenDetails,
      UpdateMailRequest updateMailRequest) throws MailAlreadyExistsException;

  /**
   * Swap mail in updateMailTable with the current mail.
   * This method should be called when the new email is activated
   * so it swaps the old main in the ldap with the
   * new mail(in the updateMail table) by giving the id of the row
   * with this information.
   *
   * @param rowId primary key of row in UpdateMail table */
  void confirmedMail(Long rowId) throws MissingElementException, MailAlreadyExistsException;

  List<User> findUsersByUsernames(Collection<String> usernames) throws MissingElementException;

  List<User> findUsersByIds(Collection<Long> ids) throws MissingElementException;

  User findUserById(Long id) throws MissingElementException;

  List<User> findUsersByUsernamesAndGroups(Collection<String> usernames, Collection<String> groups);

}
