package bg.unisofia.fmi.edusoft.backend.period;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class PeriodService
    implements ApplicationListener<ContextRefreshedEvent> {

  private final PeriodRepository periodRepository;

  private List<Period> activePeriods;

  public PeriodService(final PeriodRepository periodRepository) {
    this.periodRepository = periodRepository;
  }

  public List<Period> getActivePeriods() {
    return Collections.unmodifiableList(activePeriods);
  }

  @Override
  public void onApplicationEvent(final ContextRefreshedEvent event) {
    reloadPeriods();
  }

  @Scheduled(fixedRate = 60 * 1000L)
  private void reloadPeriods() {
    activePeriods = periodRepository.findActivePeriods(LocalDateTime.now());
  }
}
