package bg.unisofia.fmi.edusoft.backend.fake;

public interface FakeUpdateMailTokenService {
  /**
   * Send mail to user to confirm newEmail.
   * This is a fake and should be implemented later.
   *
   * @param mail new credentials
   */
  void sendMail(String mail);
}
