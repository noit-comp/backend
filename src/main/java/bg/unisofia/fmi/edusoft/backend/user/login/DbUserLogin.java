package bg.unisofia.fmi.edusoft.backend.user.login;

import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@AllArgsConstructor
@EqualsAndHashCode
@Getter
public class DbUserLogin {

  private final Long id;
  private final Long userId;
  private final LocalDateTime loginTime;

}
