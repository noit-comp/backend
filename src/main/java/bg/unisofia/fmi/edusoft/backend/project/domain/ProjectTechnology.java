package bg.unisofia.fmi.edusoft.backend.project.domain;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@AllArgsConstructor
@EqualsAndHashCode
@Getter
public class ProjectTechnology {

  private final Long project;
  private final Long technology;

}
