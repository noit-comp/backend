package bg.unisofia.fmi.edusoft.backend.periodactivity;

import bg.unisofia.fmi.edusoft.backend.jdbc.NamedParameterSupport;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class PeriodActivityRepository
    extends NamedParameterSupport<PeriodActivity> {

  protected PeriodActivityRepository(final DataSource dataSource) {
    super(dataSource, new PeriodStateRowMapper());
  }

  public List<PeriodActivity> findAllByPeriodIdIn(final Collection<Long> periodIds) {
    final String sql = "SELECT * FROM periods_activities WHERE period IN (:periodIds)";
    final Map<String, Object> params = Map.of("periodIds", periodIds);
    return findAll(sql, params);
  }

  private static final class PeriodStateRowMapper
      implements RowMapper<PeriodActivity> {

    /**
     * Implementations must implement this method to map each row of data in the ResultSet. This
     * method should not call {@code next()} on the ResultSet; it is only supposed to map values of
     * the current row.
     *
     * @param rs     the ResultSet to map (pre-initialized for the current row)
     * @param rowNum the number of the current row
     * @return the result object for the current row (may be {@code null})
     * @throws SQLException if an SQLException is encountered getting column values (that is,
     *                      there's no need to catch SQLException)
     */
    @Override
    public PeriodActivity mapRow(final ResultSet rs, final int rowNum)
        throws SQLException {
      final Long periodId = rs.getLong("period");
      final Long activityId = rs.getLong("activity");

      return new PeriodActivity(periodId, activityId);
    }
  }
}
