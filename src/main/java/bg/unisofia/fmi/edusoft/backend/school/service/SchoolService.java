package bg.unisofia.fmi.edusoft.backend.school.service;

import bg.unisofia.fmi.edusoft.backend.school.domain.School;
import java.util.List;

public interface SchoolService {

  List<School> findAllSchoolsByProject(Long projectId);
}
