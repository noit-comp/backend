package bg.unisofia.fmi.edusoft.backend.group.service;

import bg.unisofia.fmi.edusoft.backend.user.domain.User;
import java.util.Collection;
import java.util.Set;

public interface GroupService {

  /**
   * Add user to ldap group.
   *
   * @param groupName name of the group that the user will join
   * @param user      the user that will join the group
   * @return Has the user successfully joined the group
   */
  Boolean addUserToGroup(User user, String groupName);

  /**
   * Remove user from ldap group.
   * This method deletes the group if it has no members(required by openldap).
   *
   * @param groupName name of the group that the user will leave
   * @param user      the user that will leave the group
   * @return Has the user successfully left the group
   */
  Boolean removeUserFromGroup(User user, String groupName);

  Set<String> getAllUsersUsernamesByGroups(Collection<String> groupNames);
}
