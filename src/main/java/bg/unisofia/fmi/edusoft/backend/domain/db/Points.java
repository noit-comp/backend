package bg.unisofia.fmi.edusoft.backend.domain.db;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@AllArgsConstructor
@EqualsAndHashCode
@Getter
public class Points {

  private Long id;
  private Long fromUser;
  private Long project;
  private Long value;
  private Long criteria;
  private Long round;

}
