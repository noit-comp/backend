package bg.unisofia.fmi.edusoft.backend.security.jjwt.secrets;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

public interface JjwtSecretService {

  String getSecret();

  String getEncodedSecret();

  static String encode(final String secret) {
    return Base64.getEncoder().encodeToString(secret.getBytes(StandardCharsets.UTF_8));
  }
}
