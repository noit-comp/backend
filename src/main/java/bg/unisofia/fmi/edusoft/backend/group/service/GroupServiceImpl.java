package bg.unisofia.fmi.edusoft.backend.group.service;

import bg.unisofia.fmi.edusoft.backend.group.domain.LdapGroup;
import bg.unisofia.fmi.edusoft.backend.group.repository.LdapGroupRepository;
import bg.unisofia.fmi.edusoft.backend.user.domain.User;
import bg.unisofia.fmi.edusoft.backend.utility.LdapHelper;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import javax.naming.Name;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class GroupServiceImpl implements GroupService {

  private final LdapGroupRepository ldapGroupRepository;
  private final String ldapBase;


  public GroupServiceImpl(
      LdapGroupRepository ldapGroupRepository, @Value("${spring.ldap.base}") String ldapBase) {
    this.ldapGroupRepository = ldapGroupRepository;
    this.ldapBase = ldapBase;
  }

  @Override
  public Boolean addUserToGroup(User user, String groupName) {
    LdapGroup ldapGroup = ldapGroupRepository.findByName(groupName);

    if (ldapGroup == null) {
      ldapGroup = new LdapGroup(groupName);
    }

    final String userDn = user.getUserLdapName().toString() + "," + ldapBase;
    final boolean res = ldapGroup.addUserToGroup(userDn);
    ldapGroupRepository.save(ldapGroup);
    return res;
  }

  @Override
  public Boolean removeUserFromGroup(User user, String groupName) {
    LdapGroup ldapGroup = ldapGroupRepository.findByName(groupName);

    if (ldapGroup == null) {
      return true;
    }

    final String userDn = user.getUserLdapName().toString() + "," + ldapBase;
    final boolean res = ldapGroup.removeUserFromGroup(userDn);

    if (ldapGroup.getMembers().size() == 0) {
      ldapGroupRepository.delete(ldapGroup);
      return res;
    }

    ldapGroupRepository.save(ldapGroup);
    return res;
  }


  @Override
  public Set<String> getAllUsersUsernamesByGroups(Collection<String> groupNames) {
    Set<Name> groupIds = LdapHelper.getLdapIdsFromGroupNames(groupNames);

    Iterable<LdapGroup> ldapGroups = ldapGroupRepository.findAllById(groupIds);

    return StreamSupport.stream(ldapGroups.spliterator(), false)
        .map(LdapGroup::getMembers)
        .flatMap(Set::stream)
        .collect(Collectors.toSet());
  }
}
