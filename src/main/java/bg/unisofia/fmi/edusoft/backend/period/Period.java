package bg.unisofia.fmi.edusoft.backend.period;

import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@AllArgsConstructor
@EqualsAndHashCode
@Getter
public class Period {

  private final Long id;
  private final Long competitionId;
  private final LocalDateTime start;
  private final LocalDateTime end;
}
