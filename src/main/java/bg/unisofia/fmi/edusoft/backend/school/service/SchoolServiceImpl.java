package bg.unisofia.fmi.edusoft.backend.school.service;

import bg.unisofia.fmi.edusoft.backend.school.domain.School;
import bg.unisofia.fmi.edusoft.backend.school.repository.SchoolRepository;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class SchoolServiceImpl implements SchoolService {

  private final SchoolRepository schoolRepository;

  public SchoolServiceImpl(
      SchoolRepository schoolRepository) {
    this.schoolRepository = schoolRepository;
  }

  @Override
  public List<School> findAllSchoolsByProject(Long projectId) {
    return schoolRepository.findAllSchoolsForProject(projectId);
  }
}
