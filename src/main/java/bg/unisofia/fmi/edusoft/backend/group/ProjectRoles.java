package bg.unisofia.fmi.edusoft.backend.group;

public enum ProjectRoles {
  INSTRUCTOR,
  STUDENT
}
