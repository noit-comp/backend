package bg.unisofia.fmi.edusoft.backend.token.exception;

import org.springframework.http.HttpStatus;

public class UsedTokenException
    extends TokenException {

  public UsedTokenException() {
    super(HttpStatus.GONE, "Token already used", null, null);
  }

}
