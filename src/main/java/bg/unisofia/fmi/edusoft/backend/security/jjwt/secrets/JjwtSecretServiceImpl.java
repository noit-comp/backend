package bg.unisofia.fmi.edusoft.backend.security.jjwt.secrets;

import java.time.OffsetDateTime;
import java.util.UUID;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class JjwtSecretServiceImpl implements JjwtSecretService,
    ApplicationListener<ContextRefreshedEvent> {

  private static final long MAXIMUM_TOKEN_LIFETIME_MINUTES = 24 * 60 - 3;

  private final JjwtSecretRepository jjwtSecretRepository;

  private UUID secret = null;
  private String encodedSecret = null;

  public JjwtSecretServiceImpl(
      JjwtSecretRepository jjwtSecretRepository) {
    this.jjwtSecretRepository = jjwtSecretRepository;
  }

  private static UUID generateSecret() {
    return UUID.randomUUID();
  }

  @Override
  public String getSecret() {
    return secret.toString();
  }

  @Override
  public String getEncodedSecret() {
    return encodedSecret;
  }

  @Override
  public void onApplicationEvent(ContextRefreshedEvent event) {
    reload();
  }

  @Scheduled(cron = "31 07 04 * * *")
  private void reload() {
    final JjwtSecret jjwtSecret = getDbSecret();
    secret = jjwtSecret.getSecret();
    encodedSecret = JjwtSecretService.encode(secret.toString());
  }

  private JjwtSecret getDbSecret() {
    final JjwtSecret jjwtSecret = jjwtSecretRepository.findLast();
    if (OffsetDateTime.now().minusMinutes(MAXIMUM_TOKEN_LIFETIME_MINUTES)
        .isAfter(jjwtSecret.getGeneratedOn())) {
      jjwtSecretRepository.insert(generateSecret());
      return jjwtSecretRepository.findLast();
    }
    return jjwtSecret;
  }
}

