package bg.unisofia.fmi.edusoft.backend.token;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class TokenRequest {
  private String password;
}
