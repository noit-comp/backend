package bg.unisofia.fmi.edusoft.backend.user.repository.ldap;


public interface CustomLdapUserRepository {
  /**
   * Authenticate user by asking the ldap server and return true if they are correct.
   *
   *
   * @param username credentials
   * @param password credentials
   * @return true or false if valid or invalid credentials
   */
  Boolean authenticate(String username, String password);
}
