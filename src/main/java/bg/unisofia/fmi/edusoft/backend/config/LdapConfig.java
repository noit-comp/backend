package bg.unisofia.fmi.edusoft.backend.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.ldap.repository.config.EnableLdapRepositories;

@Configuration
@EnableLdapRepositories
public class LdapConfig {

}
