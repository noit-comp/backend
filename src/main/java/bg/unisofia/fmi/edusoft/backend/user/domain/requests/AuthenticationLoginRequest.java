package bg.unisofia.fmi.edusoft.backend.user.domain.requests;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Builder(builderClassName = "Builder", toBuilder = true)
@EqualsAndHashCode
@Getter
@JsonDeserialize(builder = AuthenticationLoginRequest.Builder.class)
@NoArgsConstructor
public class AuthenticationLoginRequest {

  @NotNull
  private String username;

  @NotNull
  private String password;

  @JsonPOJOBuilder(withPrefix = "")
  public static class Builder {
  }

}
