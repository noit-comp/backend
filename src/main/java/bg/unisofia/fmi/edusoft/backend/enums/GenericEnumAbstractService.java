package bg.unisofia.fmi.edusoft.backend.enums;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public abstract class GenericEnumAbstractService<
    EntityT extends EnumAbstractType,
    EnumTypeT extends Enum<EnumTypeT>> {

  protected final Class<EnumTypeT> enumClass;

  private Map<Long, Wrapper> idToEntity = null;
  private Map<EnumTypeT, EntityT> enumToEntity = null;

  /**
   * Generic enum tables service.
   */
  protected GenericEnumAbstractService(final Class<EnumTypeT> enumClass) {
    this.enumClass = enumClass;
  }

  private Wrapper getWrapper(final Long id) {
    return idToEntity.get(id);
  }

  public final EnumTypeT getEnumValue(final Long id) {
    return getWrapper(id).enumValue;
  }

  public final EntityT getEntity(final Long id) {
    return getWrapper(id).entity;
  }

  public final EntityT getEntity(final EnumTypeT value) {
    return enumToEntity.get(value);
  }

  public final Long get(final EnumTypeT value) {
    return getEntity(value).getId();
  }

  public final EnumTypeT get(final Long id) {
    return getEnumValue(id);
  }

  protected final void load(final Collection<EntityT> entities) {
    final Map<Long, Wrapper> idToEntity = new HashMap<>();
    final Map<EnumTypeT, EntityT> enumToEntity = new HashMap<>();

    entities.forEach(entityT -> {
      final EnumTypeT e = Enum.valueOf(enumClass, entityT.getValue());
      idToEntity.put(entityT.getId(), new Wrapper(entityT, e));
      enumToEntity.put(e, entityT);
    });

    this.enumToEntity = enumToEntity;
    this.idToEntity = idToEntity;
  }

  private final class Wrapper {

    public final EntityT entity;
    public final EnumTypeT enumValue;

    public Wrapper(EntityT entity, EnumTypeT enumValue) {
      this.entity = entity;
      this.enumValue = enumValue;
    }
  }
}
