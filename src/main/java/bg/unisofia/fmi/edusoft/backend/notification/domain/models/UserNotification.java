package bg.unisofia.fmi.edusoft.backend.notification.domain.models;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@AllArgsConstructor
@Getter
@EqualsAndHashCode
public class UserNotification implements Serializable {
  @NotNull
  private Long notificationId;
  @NotNull
  private Long userId;
  @NotNull
  private String status;
  private LocalDateTime startSendingEmail;
  private LocalDateTime emailSent;
}
