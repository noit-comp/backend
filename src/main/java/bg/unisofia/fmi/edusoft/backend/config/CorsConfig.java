package bg.unisofia.fmi.edusoft.backend.config;

import java.util.Arrays;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.reactive.CorsConfigurationSource;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;

@Configuration
public class CorsConfig {

  @Value("${app.cors}")
  private String[] corsDomains;

  @Bean
  CorsConfigurationSource corsConfigurationSource() {
    final CorsConfiguration configuration = new CorsConfiguration();
    final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
    configuration.setAllowedOrigins(Arrays.asList(corsDomains));
    configuration.setAllowedMethods(Arrays.asList(
        HttpMethod.GET.name(),
        HttpMethod.POST.name(),
        HttpMethod.PUT.name(),
        HttpMethod.HEAD.name(),
        HttpMethod.OPTIONS.name()));
    configuration.setMaxAge(1800L);
    configuration.addAllowedHeader("*");
    source.registerCorsConfiguration("/**", configuration);
    return source;
  }
}
