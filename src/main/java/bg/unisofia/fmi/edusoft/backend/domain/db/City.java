package bg.unisofia.fmi.edusoft.backend.domain.db;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@EqualsAndHashCode
@Getter
public class City {

  private Long id;
  private String name;
  private Long region;

}