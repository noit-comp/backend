package bg.unisofia.fmi.edusoft.backend.token.service;

import bg.unisofia.fmi.edusoft.backend.token.TokenRepository;
import bg.unisofia.fmi.edusoft.backend.token.model.Token;
import bg.unisofia.fmi.edusoft.backend.token.type.TokenType;
import bg.unisofia.fmi.edusoft.backend.token.type.TokenTypeService;
import java.util.UUID;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class TokenGeneratorServiceImpl implements TokenGeneratorService {
  private final PasswordEncoder passwordEncoder;
  private final TokenRepository tokenRepository;
  private final TokenTypeService tokenTypeService;

  /**
   * Implementation of TokenGeneratorService.
   */
  public TokenGeneratorServiceImpl(
      TokenTypeService tokenTypeService,
      PasswordEncoder passwordEncoder,
      TokenRepository tokenRepository) {
    this.passwordEncoder = passwordEncoder;
    this.tokenRepository = tokenRepository;
    this.tokenTypeService = tokenTypeService;
  }

  @Override
  public UUID createToken(
      Long userId,
      TokenType tokenType,
      String recipient,
      Long additionalData
  ) {
    final String password = UUID.randomUUID().toString().substring(0, 10);
    final String encodedPassword = passwordEncoder.encode(password);

    Token token = new Token(
        tokenType,
        recipient,
        userId,
        additionalData,
        tokenTypeService.getValidityInMinutes(tokenType),
        encodedPassword);
    tokenRepository.insert(token);

    return token.getValue();
  }
}
