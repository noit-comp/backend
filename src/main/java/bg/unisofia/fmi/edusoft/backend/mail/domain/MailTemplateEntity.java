package bg.unisofia.fmi.edusoft.backend.mail.domain;

import bg.unisofia.fmi.edusoft.backend.enums.EnumAbstractType;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode(callSuper = true)
public class MailTemplateEntity
    extends EnumAbstractType {

  private final String subject;
  private final String mailBody;

  public MailTemplateEntity(Long id, String value, String subject, String mailBody) {
    super(id, value);
    this.subject = subject;
    this.mailBody = mailBody;
  }
}
