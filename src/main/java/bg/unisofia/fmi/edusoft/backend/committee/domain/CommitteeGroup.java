package bg.unisofia.fmi.edusoft.backend.committee.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@AllArgsConstructor
@EqualsAndHashCode
@Getter
public class CommitteeGroup {

  private final Long id;
  private final String name;
  private final Long president;
  private final Long round;

}
