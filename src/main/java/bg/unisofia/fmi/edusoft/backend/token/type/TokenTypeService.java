package bg.unisofia.fmi.edusoft.backend.token.type;

import bg.unisofia.fmi.edusoft.backend.enums.EnumAbstractService;
import org.springframework.stereotype.Service;

@Service
public class TokenTypeService
    extends EnumAbstractService<TokenTypeEntity, TokenType> {

  public TokenTypeService(TokenTypeRepository tokenTypeRepository) {
    super(tokenTypeRepository, TokenType.class);
  }

  public Boolean isRequireLogin(TokenType tokenType) {
    return this.getEntity(tokenType).isRequireLogin();
  }

  public Long getValidityInMinutes(TokenType tokenType) {
    return this.getEntity(tokenType).getValidityInMinutes();
  }

  public Boolean isRequirePassword(TokenType tokenType) {
    return this.getEntity(tokenType).isRequirePassword();
  }
}
