package bg.unisofia.fmi.edusoft.backend;

import static org.junit.jupiter.api.Assertions.assertFalse;

import bg.unisofia.fmi.edusoft.backend.config.LdapContainerConfig;
import bg.unisofia.fmi.edusoft.backend.config.PostgreSQLContainerConfig;
import bg.unisofia.fmi.edusoft.backend.user.domain.ldap.LdapUser;
import bg.unisofia.fmi.edusoft.backend.user.domain.requests.RegisterUserRequest;
import bg.unisofia.fmi.edusoft.backend.user.domain.requests.UserTokenDetails;
import com.icegreen.greenmail.configuration.GreenMailConfiguration;
import com.icegreen.greenmail.junit5.GreenMailExtension;
import com.icegreen.greenmail.util.ServerSetupTest;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;
import org.junit.Rule;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.junit.rules.Timeout;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.testcontainers.containers.DockerComposeContainer;
import org.testcontainers.containers.PostgreSQLContainer;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles({"integration-test"})
public abstract class GenericIntegrationTest {

  @RegisterExtension
  protected static GreenMailExtension greenMail = new GreenMailExtension(ServerSetupTest.SMTP)
      .withConfiguration(GreenMailConfiguration.aConfig().withUser("noit", "test"))
      .withPerMethodLifecycle(false);

  protected static final String PASSWORD = "password123";
  // TODO(pecata): Find better solution than static
  private static final PostgreSQLContainer POSTGRE_SQL_CONTAINER;
  private static final DockerComposeContainer LDAP_CONTAINER;
  private static final String STRING_DATE_TIME = LocalDateTime.now().toString()
      .replaceAll("[:\\.-]", "_");
  private static final Set<String> keyMap = new TreeSet<>();

  static {
    POSTGRE_SQL_CONTAINER = PostgreSQLContainerConfig.getInstance();
    POSTGRE_SQL_CONTAINER.start();
    LDAP_CONTAINER = LdapContainerConfig.getInstance();
    LDAP_CONTAINER.start();
  }

  @Rule
  public Timeout globalTimeout = new Timeout(23, TimeUnit.SECONDS);

  @Autowired
  private PasswordEncoder passwordEncoder;

  private LocalDateTime testStartTime;

  @BeforeEach
  private void updateTestStartTime() {
    testStartTime = LocalDateTime.now();
  }

  protected LocalDateTime getTestStartTime() {
    return testStartTime;
  }

  protected String generateUsername(final String prefix) {
    return "u__" + prefix + "__" + STRING_DATE_TIME;
  }

  protected String generateMail(final String prefix) {
    return generateUsername(prefix) + "@mail.me";
  }

  protected UserTokenDetails generateUserTokenDetails(final String prefix) {

    String username = generateUsername(prefix);
    String mail = generateMail(prefix);
    return new UserTokenDetails(
        (long) prefix.hashCode(),
        username,
        "fName" + username,
        "mName" + username,
        "lName" + username,
        mail,
        null);
  }

  protected LdapUser generateLdapUser(final String prefix) {
    String username = generateUsername(prefix);
    return new LdapUser(
        username,
        "fName" + username,
        "mName" + username,
        "lName" + username,
        generateMail(prefix),
        PASSWORD
    );
  }

  protected RegisterUserRequest generateRegisterUserRequest(final String prefix) {
    String username = generateUsername(prefix);
    return new RegisterUserRequest(
        username,
        PASSWORD,
        generateMail(prefix),
        "fName" + username,
        "mName" + username,
        "lName" + username
    );
  }

  protected void assertUniqueKey(final String key) {
    assertFalse(keyMap.contains(key), "Duplicated unique key: ".concat(key));
    keyMap.add(key);
    System.out.println("Executing test with key: ".concat(key));
  }
}
