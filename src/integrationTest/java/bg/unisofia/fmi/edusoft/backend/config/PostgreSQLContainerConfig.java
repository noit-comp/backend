package bg.unisofia.fmi.edusoft.backend.config;

import org.testcontainers.containers.PostgreSQLContainer;

public class PostgreSQLContainerConfig extends PostgreSQLContainer<PostgreSQLContainerConfig> {

  private static final String IMAGE = "postgres";
  private static final String VERSION = "11.1";
  private static PostgreSQLContainerConfig container;

  private PostgreSQLContainerConfig() {
    super(IMAGE + ":" + VERSION);
  }

  public static PostgreSQLContainerConfig getInstance() {
    if (container == null) {
      container = new PostgreSQLContainerConfig();
    }
    return container;
  }

  @Override
  public void start() {
    super.start();
    System.setProperty("JDBC_URL", container.getJdbcUrl());
    System.setProperty("DB_USERNAME", container.getUsername());
    System.setProperty("DB_PASSWORD", container.getPassword());
  }

  @Override
  public void stop() {
    //do nothing, JVM handles shut down
  }
}
