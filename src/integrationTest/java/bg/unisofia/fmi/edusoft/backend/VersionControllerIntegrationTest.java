package bg.unisofia.fmi.edusoft.backend;

import bg.unisofia.fmi.edusoft.backend.controller.basic.AbstractControllerIntegrationTest;
import bg.unisofia.fmi.edusoft.backend.version.VersionDto;
import bg.unisofia.fmi.edusoft.backend.version.VersionSimpleDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class VersionControllerIntegrationTest extends AbstractControllerIntegrationTest {

  private static final String SIMPLE_VERSION_URL = "/version";
  private static final String FULL_VERSION_URL = SIMPLE_VERSION_URL + "/full";

  @Autowired
  VersionSimpleDto versionSimpleDto;

  @Autowired
  VersionDto versionDto;

  @Test
  void publicVersionData__Should_ReturnValidVersionData_When_RequestedWithoutAuthentication() {
    final String KEY = "version_01";
    assertUniqueKey(KEY);

    webTestClient
        .get().uri(SIMPLE_VERSION_URL)
        .exchange()
        .expectStatus().isOk()
        .expectBody(VersionSimpleDto.class)
        .isEqualTo(versionSimpleDto)
        .returnResult().getResponseBody();
  }

  @Test
  void publicVersionData__Should_ReturnValidVersionData_When_RequestedWithAuthentication() {
    final String KEY = "version_02";
    assertUniqueKey(KEY);

    registerUser(KEY);

    authorize(KEY)
        .get().uri(SIMPLE_VERSION_URL)
        .exchange()
        .expectStatus().isOk()
        .expectBody(VersionSimpleDto.class)
        .isEqualTo(versionSimpleDto)
        .returnResult().getResponseBody();
  }

  @Test
  void adminVersionData__Should_ReturnUnauthorized_When_RequestedWithoutAuthentication() {
    final String KEY = "adminVersion_01";
    assertUniqueKey(KEY);

    webTestClient
        .get().uri(FULL_VERSION_URL)
        .exchange()
        .expectStatus().isUnauthorized();
  }

  @Test
  void adminVersionData__Should_ReturnUnauthorized_When_RequestedWithoutAuthorities() {
    final String KEY = "adminVersion_02";
    assertUniqueKey(KEY);

    registerUser(KEY);

    authorize(KEY)
        .get().uri(FULL_VERSION_URL)
        .exchange()
        .expectStatus().isForbidden();
  }

  @Test
  void adminVersionData__Should_ReturnFullVersionData_When_RequestedWithAuthorities() {
    final String KEY = "adminVersion_03";
    assertUniqueKey(KEY);

    registerUser(KEY, "ROLE_SYSTEM_ADMIN");

    authorize(KEY)
        .get().uri(FULL_VERSION_URL)
        .exchange()
        .expectStatus().isOk()
        .expectBody(VersionDto.class)
        .isEqualTo(versionDto)
        .returnResult().getResponseBody();
  }

}
