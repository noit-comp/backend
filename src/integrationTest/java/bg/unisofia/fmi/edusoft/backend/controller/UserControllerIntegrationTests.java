package bg.unisofia.fmi.edusoft.backend.controller;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import bg.unisofia.fmi.edusoft.backend.controller.basic.AbstractControllerIntegrationTest;
import bg.unisofia.fmi.edusoft.backend.user.domain.requests.UpdateMailRequest;
import bg.unisofia.fmi.edusoft.backend.user.domain.requests.UpdatePasswordRequest;
import bg.unisofia.fmi.edusoft.backend.user.mail.UpdateMailRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

public class UserControllerIntegrationTests extends AbstractControllerIntegrationTest {

  private final String passwordEndpoint = "/user/password";
  private final String mailEndpoint = "/user/mail";

  @Autowired
  UpdateMailRepository updateMailRepository;

  @Test
  void user_mail__Should_Return400_When_MailIsInvalid() {
    final String KEY = "update_controller_01";
    assertUniqueKey(KEY);
    registerUserWithWebTestClient(KEY);

    updateRequestShouldReturnStatus(KEY, new UpdateMailRequest(), HttpStatus.BAD_REQUEST,
        mailEndpoint);

    updateRequestShouldReturnStatus(KEY, new UpdateMailRequest(""), HttpStatus.BAD_REQUEST,
        mailEndpoint);

    updateRequestShouldReturnStatus(KEY, new UpdateMailRequest("mail"),
        HttpStatus.BAD_REQUEST, mailEndpoint);

    updateRequestShouldReturnStatus(KEY, new UpdateMailRequest("mail@domain"),
        HttpStatus.BAD_REQUEST, mailEndpoint);

    updateRequestShouldReturnStatus(KEY, new UpdateMailRequest("@domain"),
        HttpStatus.BAD_REQUEST, mailEndpoint);

    updateRequestShouldReturnStatus(KEY, new UpdateMailRequest("@domain.com"),
        HttpStatus.BAD_REQUEST, mailEndpoint);
  }

  @Test
  void user_mail__Should_Return201_When_MailIsValid() {
    final String KEY = "update_controller_02";
    assertUniqueKey(KEY);
    registerUserWithWebTestClient(KEY);

    updateRequestShouldReturnStatus(KEY, new UpdateMailRequest("new" + generateMail(KEY)),
        HttpStatus.CREATED, mailEndpoint);

    assertNotNull(updateMailRepository.findUpdateMailByNewMail(generateMail(KEY)));
  }

  @Test
  void user_password__Should_Return400_When_PasswordIsInvalid() {
    final String KEY = "update_controller_03";
    assertUniqueKey(KEY);
    registerUserWithWebTestClient(KEY);

    updateRequestShouldReturnStatus(KEY, new UpdatePasswordRequest(), HttpStatus.BAD_REQUEST,
        passwordEndpoint);

    updateRequestShouldReturnStatus(KEY, new UpdatePasswordRequest("", "validPassword"),
        HttpStatus.BAD_REQUEST, passwordEndpoint);

    updateRequestShouldReturnStatus(KEY, new UpdatePasswordRequest("", "short"),
        HttpStatus.BAD_REQUEST, passwordEndpoint);
  }

  @Test
  void user_password__Should_Return204_When_PasswordIsValid() {
    final String KEY = "update_controller_04";
    assertUniqueKey(KEY);
    registerUserWithWebTestClient(KEY);

    updateRequestShouldReturnStatus(KEY, new UpdatePasswordRequest("validPassword", PASSWORD),
        HttpStatus.NO_CONTENT, passwordEndpoint);
  }

  @Test
  void user_password__Should_Return401_When_PasswordIsInvalid() {
    final String KEY = "update_controller_05";
    assertUniqueKey(KEY);
    registerUserWithWebTestClient(KEY);

    updateRequestShouldReturnStatus(KEY,
        new UpdatePasswordRequest("validPassword", "wrongPassword"),
        HttpStatus.UNAUTHORIZED, passwordEndpoint);
  }

  private void updateRequestShouldReturnStatus(String KEY, Object mailOrPasswordRequest,
      HttpStatus httpStatus, String endpoint) {

    authorize(KEY)
        .put().uri(endpoint)
        .bodyValue(mailOrPasswordRequest)
        .exchange()
        .expectStatus().isEqualTo(httpStatus);
  }
}
