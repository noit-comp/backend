package bg.unisofia.fmi.edusoft.backend.config;

import java.io.File;
import org.testcontainers.containers.DockerComposeContainer;

public class LdapContainerConfig extends DockerComposeContainer<LdapContainerConfig> {

  private static final Integer LDAP_PORT = 389;
  private static final String LDAP_SERVICE_NAME = "openldap";

  private static final File containerFile =
      new File("src/integrationTest/resources/docker-compose.yml");

  public static LdapContainerConfig container;

  private LdapContainerConfig() {
    super(containerFile);
  }

  public static LdapContainerConfig getInstance() {
    if (null == container) {
      container = new LdapContainerConfig()
          .withExposedService(LDAP_SERVICE_NAME, LDAP_PORT);
    }
    return container;
  }

  @Override
  public void start() {
    super.start();
    System.setProperty("LDAP_URL", "ldap://"
        + container.getServiceHost(LDAP_SERVICE_NAME, LDAP_PORT)
        + ":" + container.getServicePort(LDAP_SERVICE_NAME, LDAP_PORT));
  }

  public void stop() {
    //do nothing, JVM handles shut down
  }
}
