package bg.unisofia.fmi.edusoft.backend.controller.basic;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import bg.unisofia.fmi.edusoft.backend.GenericIntegrationTest;
import bg.unisofia.fmi.edusoft.backend.exception.MailAlreadyExistsException;
import bg.unisofia.fmi.edusoft.backend.exception.UsernameAlreadyExistsException;
import bg.unisofia.fmi.edusoft.backend.role.RoleService;
import bg.unisofia.fmi.edusoft.backend.user.domain.User;
import bg.unisofia.fmi.edusoft.backend.user.domain.requests.AuthenticationLoginRequest;
import bg.unisofia.fmi.edusoft.backend.user.domain.requests.RegisterUserRequest;
import bg.unisofia.fmi.edusoft.backend.user.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.http.HttpHeaders;
import org.springframework.test.web.reactive.server.WebTestClient;

@AutoConfigureWebTestClient
public class AbstractControllerIntegrationTest extends GenericIntegrationTest {

  protected static final AuthenticationLoginRequest DEFAULT_AUTHENTICATION_LOGIN_REQUEST =
      new AuthenticationLoginRequest("ben", "parola123");

  @Autowired
  protected WebTestClient webTestClient;

  @Autowired
  protected UserService userService;

  @Autowired
  protected RoleService roleService;

  @Autowired
  protected ObjectMapper objectMapper;

  // region User registration helpers

  protected User registerUser(final RegisterUserRequest registerUserRequest)
      throws MailAlreadyExistsException, UsernameAlreadyExistsException {
    return userService.register(registerUserRequest);
  }

  protected User registerUser(final String KEY) {
    return assertDoesNotThrow(() -> registerUser(new RegisterUserRequest(
        generateUsername(KEY),
        PASSWORD,
        generateMail(KEY),
        KEY,
        KEY,
        KEY
    )));
  }

  //I use the assertDoesNotThrow here to avoid the requirement for a try catch
  //I believe here it is not required.
  protected User registerUser(final String KEY, final String... authorities) {
    final User user = registerUser(KEY);
    if (authorities != null && user != null) {
      Arrays.stream(authorities)
          .forEach(authority -> assertDoesNotThrow(
              () -> roleService.addRoleToUser(authority, user.getUsername())));
    } else {
      System.out.println("Skipping adding roles to registered user KEY=" + KEY);
    }

    return assertDoesNotThrow(() -> userService.findByUsernameOrMail(generateUsername(KEY)));
  }

  //endregion
  //region WebTestClient helpers

  protected WebTestClient authorize(AuthenticationLoginRequest authenticationLoginRequest) {
    final String token = webTestClient.post().uri("/auth/login")
        .bodyValue(authenticationLoginRequest)
        .exchange().expectBody(String.class).returnResult().getResponseBody();

    return webTestClient.mutate()
        .defaultHeader(HttpHeaders.AUTHORIZATION, "Bearer " + token).build();
  }

  protected WebTestClient authorize(final String username, final String password) {
    return authorize(new AuthenticationLoginRequest(username, password));
  }

  protected WebTestClient authorize(final String KEY) {
    return authorize(new AuthenticationLoginRequest(generateUsername(KEY), PASSWORD));
  }

  // endregion

  protected void registerUserWithWebTestClient(String prefix) {
    final String username = generateUsername(prefix);
    webTestClient.post().uri("/auth/register")
        .bodyValue(new RegisterUserRequest(
            username,
            PASSWORD,
            generateMail(prefix),
            prefix,
            prefix,
            prefix
        )).exchange();
  }
}
