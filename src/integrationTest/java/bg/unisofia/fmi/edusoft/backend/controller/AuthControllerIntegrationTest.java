package bg.unisofia.fmi.edusoft.backend.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import bg.unisofia.fmi.edusoft.backend.controller.basic.AbstractControllerIntegrationTest;
import bg.unisofia.fmi.edusoft.backend.user.domain.requests.AuthenticationLoginRequest;
import bg.unisofia.fmi.edusoft.backend.user.domain.requests.RegisterUserRequest;
import bg.unisofia.fmi.edusoft.backend.security.jjwt.JjwtUtil;
import bg.unisofia.fmi.edusoft.backend.user.domain.requests.UserDetailsResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

public class AuthControllerIntegrationTest extends AbstractControllerIntegrationTest {

  @Autowired
  private JjwtUtil jjwtUtil;
  private final String loginEndpoint = "/auth/login";
  private final String registerEndpoint = "/auth/register";
  private final String userEndpoint = "/auth/user";
  private final String mailExistsMessage = "Mail already exists";
  private final String usernameExistsMessage = "Username already exists";

  @Autowired
  ObjectMapper objectMapper;

  @Test
  void auth_login__Should_ReturnValidToken_When_UsernameAndPasswordIsCorrect() {
    final String KEY = "token_01";
    assertUniqueKey(KEY);
    registerUserWithWebTestClient(KEY);

    final String token = webTestClient.post().uri(loginEndpoint)
        .bodyValue(new AuthenticationLoginRequest(
            generateUsername(KEY),
            PASSWORD
        ))
        .exchange()
        .expectStatus().isOk()
        .expectBody(String.class).returnResult().getResponseBody();

    Assertions.assertTrue(jjwtUtil.validateToken(token));
  }

  @Test
  void auth_login__Should_ReturnUnauthorized_When_WrongCredentials() {
    final String KEY = "login_01";
    assertUniqueKey(KEY);
    registerUserWithWebTestClient(KEY);

    webTestClient.post().uri(loginEndpoint)
        .bodyValue(new AuthenticationLoginRequest(
            generateUsername(KEY),
            PASSWORD + "oops"
        ))
        .exchange().expectStatus().isUnauthorized();
  }

  @Test
  void auth_register__Should_ReturnConflict_When_UsernameExists() {
    final String KEY = "registerConflict_01";
    assertUniqueKey(KEY);
    registerUserWithWebTestClient(KEY);

    webTestClient.post().uri(registerEndpoint)
        .bodyValue(new RegisterUserRequest(
            generateUsername(KEY),
            PASSWORD,
            generateMail(KEY.concat("text")),
            KEY,
            KEY,
            KEY
        ))
        .exchange()
        .expectStatus().isEqualTo(HttpStatus.CONFLICT)
        .expectBody(String.class).isEqualTo(usernameExistsMessage);
  }

  @Test
  void auth_register__Should_ReturnConflict_When_MailExists() {
    final String KEY = "registerConflict_02";
    assertUniqueKey(KEY);
    registerUserWithWebTestClient(KEY);

    webTestClient.post().uri(registerEndpoint)
        .bodyValue(new RegisterUserRequest(
            generateUsername(KEY.concat("text")),
            PASSWORD,
            generateMail(KEY),
            KEY,
            KEY,
            KEY
        ))
        .exchange()
        .expectStatus().isEqualTo(HttpStatus.CONFLICT)
        .expectBody(String.class).isEqualTo(mailExistsMessage);
  }

  @Test
  void auth_register__Should_ReturnCreated_When_UserIsValid() {
    final String KEY = "registerSuccess_01";
    assertUniqueKey(KEY);
    webTestClient.post().uri(registerEndpoint)
        .bodyValue(new RegisterUserRequest(
            generateUsername(KEY),
            PASSWORD,
            generateMail(KEY),
            KEY,
            KEY,
            KEY
        ))
        .exchange()
        .expectStatus().isCreated();
  }

  private void nullPassingCycle(String registerEndpoint, String json)
      throws JsonProcessingException {
    Map<String, String> map = new HashMap<>();
    try {
      map = objectMapper.readValue(json, HashMap.class);
    } catch (JsonProcessingException e) {
      System.err.println("Error while mapping json to object");
      throw e;
    }
    Set<String> keys = new HashSet<>(map.keySet());
    for (String key : keys) {
      String value = map.get(key);
      map.remove(key);
      String object = "";
      try {
        object = objectMapper.writeValueAsString(map);
      } catch (JsonProcessingException e) {
        System.err.println("Error while mapping object to json");
        throw e;
      }
      webTestClient.post().uri(registerEndpoint)
          .contentType(MediaType.APPLICATION_JSON)
          .bodyValue(object)
          .exchange()
          .expectStatus().isEqualTo(HttpStatus.BAD_REQUEST);
      map.put(key, value);
    }
  }

  @Test
  void auth_login__Should_ReturnUnprocessable_Entity_When_FieldIsMissing()
      throws JsonProcessingException {
    final String KEY = "loginWithNull_01";
    assertUniqueKey(KEY);
    AuthenticationLoginRequest authenticationLoginRequest = new AuthenticationLoginRequest(
        generateUsername(KEY),
        PASSWORD
    );
    String json = objectMapper.writeValueAsString(authenticationLoginRequest);
    nullPassingCycle(loginEndpoint, json);
  }

  @Test
  void auth_register__Should_ReturnUnprocessable_Entity_When_PasswordIsMissing()
      throws JsonProcessingException {
    final String KEY = "registerWithNull_01";
    assertUniqueKey(KEY);
    RegisterUserRequest registerUserRequest = new RegisterUserRequest(
        generateUsername(KEY),
        PASSWORD,
        generateMail(KEY),
        KEY,
        KEY,
        KEY
    );
    String json = objectMapper.writeValueAsString(registerUserRequest);
    nullPassingCycle(registerEndpoint, json);
  }

  @Test
  void auth_user__Should_ReturnUser_When_UserExists() {
    final String KEY = "user";
    assertUniqueKey(KEY);
    registerUserWithWebTestClient(KEY);

    UserDetailsResponse user = authorize(KEY)
        .get().uri(userEndpoint)
        .exchange()
        .expectStatus()
        .isEqualTo(HttpStatus.OK)
        .expectBody(UserDetailsResponse.class).returnResult().getResponseBody();

    String expectedUsername = generateUsername(KEY);
    assertEquals(expectedUsername, user.getUsername());
  }
}
