package bg.unisofia.fmi.edusoft.backend.controller;

import bg.unisofia.fmi.edusoft.backend.GenericIntegrationTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.web.reactive.server.WebTestClient;

public class CorsControllerIntegrationTest extends GenericIntegrationTest {

  private final String testingCorsResource = "/version";
  @Autowired
  private WebTestClient webTestClient;
  @Value("${app.cors}")
  private String[] corsAllowedDomains;

  private WebTestClient.ResponseSpec requestWithOrigin(final String origin) {
    return webTestClient.get()
        .uri(uriBuilder -> uriBuilder
            .path(testingCorsResource)
            .build())
        .header("Origin", origin)
        .exchange();
  }

  @Test
  void cors__Should_ForbiddenAccess_When_DomainNotListedInCors() {
    requestWithOrigin("missing-origin-values-goes-here").expectStatus().isForbidden();
  }

  @Test
  void cors__Should_ReturnOkStatusForEachDomain_When_DomainListedInCors() {
    Assertions.assertNotEquals(0, corsAllowedDomains.length);
    for (final String domain : corsAllowedDomains) {
      requestWithOrigin(domain).expectStatus().isOk();
    }
  }
}
