package bg.unisofia.fmi.edusoft.backend.user.service;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import bg.unisofia.fmi.edusoft.backend.GenericIntegrationTest;
import bg.unisofia.fmi.edusoft.backend.domain.db.UpdateMail;
import bg.unisofia.fmi.edusoft.backend.exception.InvalidLdapCredentialsException;
import bg.unisofia.fmi.edusoft.backend.exception.MailAlreadyExistsException;
import bg.unisofia.fmi.edusoft.backend.exception.MailRateLimitExceededException;
import bg.unisofia.fmi.edusoft.backend.exception.MissingElementException;
import bg.unisofia.fmi.edusoft.backend.exception.UsernameAlreadyExistsException;
import bg.unisofia.fmi.edusoft.backend.user.domain.User;
import bg.unisofia.fmi.edusoft.backend.user.domain.db.DbUser;
import bg.unisofia.fmi.edusoft.backend.user.domain.ldap.LdapUser;
import bg.unisofia.fmi.edusoft.backend.user.domain.requests.AuthenticationLoginRequest;
import bg.unisofia.fmi.edusoft.backend.user.domain.requests.RegisterUserRequest;
import bg.unisofia.fmi.edusoft.backend.user.domain.requests.UpdateMailRequest;
import bg.unisofia.fmi.edusoft.backend.user.domain.requests.UserTokenDetails;
import bg.unisofia.fmi.edusoft.backend.user.login.DbUserLoginRepository;
import bg.unisofia.fmi.edusoft.backend.user.mail.UpdateMailRepository;
import bg.unisofia.fmi.edusoft.backend.user.repository.db.DbUserRepository;
import bg.unisofia.fmi.edusoft.backend.user.repository.ldap.LdapUserRepository;
import com.icegreen.greenmail.store.FolderException;
import com.icegreen.greenmail.util.GreenMailUtil;
import java.time.LocalDateTime;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

// TODO: create tests for the database once it is configured
public class UserServiceIntegrationTest extends GenericIntegrationTest {

  @Autowired
  private UserService userService;

  @Autowired
  private LdapUserRepository ldapUserRepository;

  @Autowired
  private DbUserRepository dbUserRepository;

  @Autowired
  private DbUserLoginRepository dbUserLoginRepository;

  @Autowired
  private UpdateMailRepository updateMailRepository;

  private void testInsertedLoginTime(Long userId) {
    assertEquals(1,
        dbUserLoginRepository.findAllByLoginTimeBetween(getTestStartTime(), LocalDateTime.now())
            .stream()
            .filter(dbUserLogin -> dbUserLogin.getUserId().equals(userId)).count(),
        "Test inserted login data failed");
  }

  // region authenticate
  @Test
  void authenticate__Should_ReturnUserAndInsertLoginData_When_ExistUserWithSuchCredentialsWithEntryInDb() {
    final String KEY = "auth_01";
    assertUniqueKey(KEY);

    final LdapUser ldapUser = ldapUserRepository.save(generateLdapUser(KEY));
    final Long dbUserId = dbUserRepository.insertWithUsername(ldapUser.getUsername());
    final DbUser dbUser = dbUserRepository.findById(dbUserId);

    // check for correct response
    User user = assertDoesNotThrow(() ->
        userService.authenticate(new AuthenticationLoginRequest(ldapUser.getUsername(), PASSWORD)));

    assertNotNull(dbUser);
    assertEquals(dbUser.getId(), user.getId(), "Wrong id");
    assertEquals(ldapUser.getUsername(), user.getUsername(), "Username did not match");

    testInsertedLoginTime(dbUser.getId());
  }

  @Test
  void authenticate__Should_ReturnUserAndInsertLoginData_When_ExistUserWithSuchCredentialsAndWithNoEntryInDb() {
    final String KEY = "auth_02";
    assertUniqueKey(KEY);

    final LdapUser ldapUser = ldapUserRepository.save(generateLdapUser(KEY));

    // check for correct response
    User user = assertDoesNotThrow(() -> userService
        .authenticate(new AuthenticationLoginRequest(ldapUser.getUsername(), PASSWORD)));
    assertEquals(ldapUser.getUsername(), user.getUsername(), "Username did not match");

    // check for insertion in DB
    DbUser dbUser = assertDoesNotThrow(
        () -> dbUserRepository.findByUsername(ldapUser.getUsername()));
    assertEquals(ldapUser.getUsername(), dbUser.getUsername());

    testInsertedLoginTime(dbUser.getId());
  }

  @Test
  void authenticate__Should_MissingElementException_When_NoUserWithThisEmailOrUsername() {
    final String KEY = "auth_03";
    assertUniqueKey(KEY);

    assertThrows(MissingElementException.class, () -> userService
        .authenticate(new AuthenticationLoginRequest(generateUsername(KEY), PASSWORD)));
  }

  @Test
  void authenticate__Should_InvalidLdapCredentials_When_PasswordDoesNotMatch() {
    final String KEY = "auth_04";
    assertUniqueKey(KEY);

    final LdapUser ldapUser = ldapUserRepository.save(generateLdapUser(KEY));

    assertThrows(InvalidLdapCredentialsException.class, () -> userService.authenticate(
        new AuthenticationLoginRequest(ldapUser.getUsername(), PASSWORD.concat(PASSWORD))));
  }

  // endregion
  // region findUserByUsernameOrMail

  @Test
  void findByUsernameOrMail__Should_ReturnValidUser_When_SearchingForUsernameAndUserExistsWithEntryInDb() {
    final String KEY = "findByUsernameOrMail_01u";
    assertUniqueKey(KEY);

    final LdapUser ldapUser = ldapUserRepository.save(generateLdapUser(KEY));
    final Long dbUserId = dbUserRepository.insertWithUsername(ldapUser.getUsername());
    final DbUser dbUser = dbUserRepository.findById(dbUserId);
    assertNotNull(dbUser);

    User user = assertDoesNotThrow(() -> userService.findByUsernameOrMail(ldapUser.getUsername()));

    assertEquals(ldapUser.getUsername(), user.getUsername());
    assertEquals(dbUser.getId(), user.getId());
  }

  @Test
  void findByUsernameOrMail__Should_ReturnValidUser_When_SearchingForMailAndUserExistsWithEntryInDb() {
    final String KEY = "findByUsernameOrMail_01m";
    assertUniqueKey(KEY);

    final LdapUser ldapUser = ldapUserRepository.save(generateLdapUser(KEY));
    final Long dbUserId = dbUserRepository.insertWithUsername(ldapUser.getUsername());
    final DbUser dbUser = dbUserRepository.findById(dbUserId);
    assertNotNull(dbUser);

    User user = assertDoesNotThrow(() -> userService.findByUsernameOrMail(ldapUser.getMail()));
    assertEquals(ldapUser.getUsername(), user.getUsername());
    assertEquals(dbUser.getId(), user.getId());
  }

  @Test
  void findByUsernameOrMail__Should_InsertDataIntoDb_When_SearchingForUsernameAndMissingSuchData() {
    final String KEY = "findByUsernameOrMail_02u";
    assertUniqueKey(KEY);

    final LdapUser ldapUser = ldapUserRepository.save(generateLdapUser(KEY));

    User user = assertDoesNotThrow(() -> userService.findByUsernameOrMail(ldapUser.getUsername()));
    assertEquals(ldapUser.getUsername(), user.getUsername());

    // check for insertion in DB
    Assertions.assertNotNull(dbUserRepository.findByUsername(ldapUser.getUsername()));
  }

  @Test
  void findByUsernameOrMail__Should_InsertDataIntoDb_When_SearchingForMailAndMissingSuchData() {
    final String KEY = "findByUsernameOrMail_02m";
    assertUniqueKey(KEY);

    final LdapUser ldapUser = ldapUserRepository.save(generateLdapUser(KEY));

    User user = assertDoesNotThrow(() -> userService.findByUsernameOrMail(ldapUser.getMail()));
    assertEquals(ldapUser.getUsername(), user.getUsername());

    // check for insertion in DB
    Assertions.assertNotNull(dbUserRepository.findByUsername(ldapUser.getUsername()));
  }

  @Test
  void findByUsernameOrMail__Should_ReturnMissingElementException_When_SearchingWithUsernameAndNoSuchUserInLdap() {
    final String KEY = "findByUsernameOrMail_03u";
    assertUniqueKey(KEY);

    assertThrows(MissingElementException.class,
        () -> userService.findByUsernameOrMail(generateUsername(KEY)));
  }

  @Test
  void findByUsernameOrMail__Should_ReturnMissingElementException_When_SearchingWithMailAndNoSuchUserInLdap() {
    final String KEY = "findByUsernameOrMail_03m";
    assertUniqueKey(KEY);

    assertThrows(MissingElementException.class,
        () -> userService.findByUsernameOrMail(generateUsername(KEY)));
  }

  // endregion
  // region register

  @Test
  void register__Should_ReturnCombinedUser_When_RegistrationIsSuccessful()
      throws MessagingException, FolderException {
    final String KEY = "register_01";
    assertUniqueKey(KEY);
    User user = assertDoesNotThrow(() -> userService.register(new RegisterUserRequest(
        generateUsername(KEY),
        PASSWORD,
        generateMail(KEY),
        KEY,
        KEY,
        KEY
    )));

    assertEquals(user.getUsername(), generateUsername(KEY));

    // check for insertion in DB
    Assertions.assertNotNull(dbUserRepository.findByUsername(generateUsername(KEY)));

    // check if mail confirmation is added
    Assertions
        .assertNotNull(
            updateMailRepository.findUpdateMailByNewMailAndUserId(generateMail(KEY), user.getId()));

    this.verifyMail(generateMail(KEY));
  }

  @Test
  void register__Should_ReturnUsernameAlreadyExists_When_AlreadyHasThisUsername() {
    final String KEY = "register_02";
    assertUniqueKey(KEY);

    ldapUserRepository.save(generateLdapUser(KEY));

    assertTrue(ldapUserRepository.existsByUsername(generateUsername(KEY)));

    assertThrows(UsernameAlreadyExistsException.class,
        () -> userService.register(new RegisterUserRequest(
            generateUsername(KEY),
            PASSWORD,
            generateMail(KEY).concat("of"),
            KEY,
            KEY,
            KEY
        )));
  }

  @Test
  void register__Should_ReturnEmailAlreadyExists_When_AlreadyRegisteredMail() {
    final String KEY = "register_03";
    assertUniqueKey(KEY);

    ldapUserRepository.save(generateLdapUser(KEY));

    assertThrows(MailAlreadyExistsException.class,
        () -> userService.register(new RegisterUserRequest(
            generateUsername(KEY).concat("of"),
            PASSWORD,
            generateMail(KEY),
            KEY,
            KEY,
            KEY
        )));
  }

  // endregion
  // region requestMailUpdate

  @Test
  void requestMailUpdate__Should_SaveUserRequestAndSendMail_When_MailUpdateRequestDoesNotExist()
      throws MailAlreadyExistsException, MessagingException, FolderException {
    final String KEY = "mail_01";
    final UserTokenDetails userTokenDetails = generateUserTokenDetails(KEY);
    assertUniqueKey(KEY);

    final Long dbUserId = dbUserRepository.insertWithUsername(userTokenDetails.getUsername());
    final DbUser dbUser = dbUserRepository.findById(dbUserId);
    userTokenDetails.setId(dbUser.getId());

    userService
        .requestMailUpdate(userTokenDetails, new UpdateMailRequest("new" + generateMail(KEY)));

    UpdateMail updateMail = updateMailRepository
        .findUpdateMailByNewMailAndUserId("new" + generateMail(KEY), dbUserId);

    assertFalse(updateMail.getConfirmed());
    assertEquals("new" + generateMail(KEY), updateMail.getNewMail());

    this.verifyMail(generateMail(KEY));
  }

  @Test
  void requestMailUpdate__Should_UpdateUserRequestAndSendMail_When_MailUpdateRequestExistedBefore()
      throws MailAlreadyExistsException, MessagingException, FolderException {
    final String KEY = "mail_02";
    final UserTokenDetails userTokenDetails = generateUserTokenDetails(KEY);
    assertUniqueKey(KEY);

    //Need to create the DbUser used for the foreign key
    final Long dbUserId = dbUserRepository.insertWithUsername(userTokenDetails.getUsername());
    DbUser dbUser = dbUserRepository.findById(dbUserId);

    userTokenDetails.setId(dbUser.getId());

    Long updateMailId = updateMailRepository
        .insert("new" + generateMail(KEY), userTokenDetails.getId());

    userService
        .requestMailUpdate(userTokenDetails, new UpdateMailRequest("new" + generateMail(KEY)));

    UpdateMail updateMailResult = updateMailRepository
        .findUpdateMailByNewMailAndUserId("new" + generateMail(KEY), dbUserId);
    assertEquals(updateMailResult.getId(), updateMailId);
    assertFalse(updateMailResult.getConfirmed());
    assertEquals("new" + generateMail(KEY), updateMailResult.getNewMail());
    this.verifyMail(generateMail(KEY));
  }

  @Test
  void requestMailUpdate__Should_UpdateCreateNewUserRequestWithNewUserAndSendMail_When_UserRequestForSameMailExistsButIsNotYetConfirmed()
      throws MailAlreadyExistsException, MessagingException, FolderException {
    final String KEY = "mail_03";
    final UserTokenDetails userWithNewRequestTokenDetails = generateUserTokenDetails(KEY);
    assertUniqueKey(KEY);

    Long dbUserId = dbUserRepository
        .insertWithUsername("userWithOldRequest" + userWithNewRequestTokenDetails.getUsername());
    Long oldDbUserId = dbUserId;

    //Need to create the DbUser used for the foreign key
    dbUserId = dbUserRepository.insertWithUsername(userWithNewRequestTokenDetails.getUsername());
    DbUser dbUser = dbUserRepository.findById(dbUserId);
    userWithNewRequestTokenDetails.setId(dbUser.getId());

    Long oldUpdateMailId = updateMailRepository.insert("new" + generateMail(KEY),
        oldDbUserId);

    userService.requestMailUpdate(userWithNewRequestTokenDetails,
        new UpdateMailRequest("new" + generateMail(KEY)));

    UpdateMail updateMailResponse = updateMailRepository
        .findUpdateMailByNewMailAndUserId("new" + generateMail(KEY), dbUserId);

    assertNotEquals(updateMailResponse.getId(), oldUpdateMailId);
    assertEquals(updateMailResponse.getUserId(), userWithNewRequestTokenDetails.getId());
    assertFalse(updateMailResponse.getConfirmed());
    assertEquals("new" + generateMail(KEY), updateMailResponse.getNewMail());

    this.verifyMail(generateMail(KEY));
  }

  @Test
  void requestMailUpdate__Should_ThrowMailRateLimitExceededException_When_ToManyUpdateMailRequestSent()
      throws MailAlreadyExistsException {
    final String KEY = "mail_04";
    final UserTokenDetails userTokenDetails = generateUserTokenDetails(KEY);
    assertUniqueKey(KEY);

    final Long dbUserId = dbUserRepository.insertWithUsername(userTokenDetails.getUsername());
    final DbUser dbUser = dbUserRepository.findById(dbUserId);
    userTokenDetails.setId(dbUser.getId());

    updateMailRepository
        .insert("new1" + generateMail(KEY), userTokenDetails.getId());
    updateMailRepository
        .insert("new2" + generateMail(KEY), userTokenDetails.getId());

    assertThrows(MailRateLimitExceededException.class, () ->
        userService
            .requestMailUpdate(userTokenDetails,
                new UpdateMailRequest("new" + generateMail(KEY)))
    );
  }

  //endregion
  //region confirmedMail

  @Test
  void confirmedMail__Should_UpdateUserMail_When_UpdateMailRequestExists()
      throws MissingElementException, MailAlreadyExistsException {
    final String KEY = "confirm_mail_01";
    final LdapUser expectedLdapUser = generateLdapUser(KEY);
    assertUniqueKey(KEY);

    ldapUserRepository.save(expectedLdapUser);
    final Long dbUserId = dbUserRepository.insertWithUsername(expectedLdapUser.getUsername());

    Long updateMailId = updateMailRepository
        .insert("new" + generateMail(KEY), dbUserId);

    userService.confirmedMail(updateMailId);
    LdapUser actualLdapUser = ldapUserRepository.
        findByUsername(expectedLdapUser.getUsername());
    assertEquals(expectedLdapUser.getUsername(), actualLdapUser.getUsername());
    assertEquals("new" + generateMail(KEY), actualLdapUser.getMail());

    UpdateMail foundUpdateMail = updateMailRepository.findById(updateMailId);

    Assertions.assertNotNull(foundUpdateMail);
    Assertions.assertTrue(foundUpdateMail.getConfirmed());
  }

  @Test
  void confirmedMail__Should_ReturnMissingElementException_When_UpdateMailRequestDoesNotExist() {
    final String KEY = "confirm_mail_02";
    final Long invalidRowId = 123L;
    assertUniqueKey(KEY);

    assertThrows(MissingElementException.class, () -> userService.confirmedMail(invalidRowId));
  }
  //endregion

  private void verifyMail(String expectedMail) throws MessagingException, FolderException {
    MimeMessage[] receivedMessages = greenMail.getReceivedMessages();
    assertEquals(1, receivedMessages.length);

    MimeMessage receivedMessage = receivedMessages[0];

    String test = GreenMailUtil.getBody(receivedMessage);
    assertTrue(GreenMailUtil.getBody(receivedMessage).contains(
            "You are receiving this email because your email address was used " +
            "to register for the NOIT competition."
    ));

    assertEquals(1, receivedMessage.getAllRecipients().length);
    assertEquals(expectedMail, receivedMessage.getAllRecipients()[0].toString());
    greenMail.purgeEmailFromAllMailboxes();
  }
}
