package bg.unisofia.fmi.edusoft.backend.controller.basic;

import bg.unisofia.fmi.edusoft.backend.user.domain.requests.AuthenticationLoginRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

/**
 * This class is created to make it easy to create
 * authorized requests via user ben
 */
public class AbstractAuthorizedIntegrationTests extends
    AbstractControllerIntegrationTest {

  protected static final AuthenticationLoginRequest AUTHENTICATION_LOGIN_REQUEST =
      new AuthenticationLoginRequest("ben", "parola123");

  @PostConstruct
  void addDefaultAuthorizationHeader() {
    final String token = webTestClient.post().uri("/auth/login").bodyValue(
        AUTHENTICATION_LOGIN_REQUEST)
        .exchange().expectBody(String.class).returnResult().getResponseBody();
    webTestClient.mutate()
        .defaultHeader(HttpHeaders.AUTHORIZATION, "Bearer " + token).build();
  }
}
