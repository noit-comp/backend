package bg.unisofia.fmi.edusoft.backend.role;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import bg.unisofia.fmi.edusoft.backend.GenericIntegrationTest;
import bg.unisofia.fmi.edusoft.backend.exception.MissingElementException;
import bg.unisofia.fmi.edusoft.backend.user.domain.User;
import bg.unisofia.fmi.edusoft.backend.user.service.UserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

public class RoleServiceIntegrationTest extends GenericIntegrationTest {

  @Autowired
  private RoleService roleService;

  @Autowired
  private UserService userService;

  @Test
  void addRoleToUser__Should_ReturnTrueAndGiveUserRole_When_UserDoesNotHaveRole()
      throws MissingElementException {
    String key = "role_01";
    assertUniqueKey(key);
    String role = "nonExistent" + key;

    User user = assertDoesNotThrow(() -> userService.register(generateRegisterUserRequest(key)));
    assertNotNull(user);

    user = userService.findByUsernameOrMail(user.getUsername());
    assertNotNull(user);

    assertTrue(roleService.addRoleToUser(role, user.getUsername()));

    User updatedUser = userService.findByUsernameOrMail(user.getUsername());
    assertTrue(updatedUser.getAuthorities().contains(new SimpleGrantedAuthority(role)));
  }

  @Test
  void removeRoleFromUser__Should_ReturnTrueAndRemoveUserRole_When_UserHasTheRole()
      throws MissingElementException {
    String key = "role_02";
    assertUniqueKey(key);
    String role = "roleName" + key;

    User user = assertDoesNotThrow(() -> userService.register(generateRegisterUserRequest(key)));
    assertNotNull(user);

    user = userService.findByUsernameOrMail(user.getUsername());
    assertNotNull(user);

    roleService.addRoleToUser(role, user.getUsername());

    Assertions.assertTrue(roleService.removeRoleFromUser(role, user.getUsername()));

    User updatedUser = userService.findByUsernameOrMail(user.getUsername());
    assertFalse(updatedUser.getAuthorities().contains(new SimpleGrantedAuthority(role)));
  }
}