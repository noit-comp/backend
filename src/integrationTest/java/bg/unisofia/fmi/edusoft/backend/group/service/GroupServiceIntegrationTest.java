package bg.unisofia.fmi.edusoft.backend.group.service;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import bg.unisofia.fmi.edusoft.backend.GenericIntegrationTest;
import bg.unisofia.fmi.edusoft.backend.group.domain.LdapGroup;
import bg.unisofia.fmi.edusoft.backend.group.repository.LdapGroupRepository;
import bg.unisofia.fmi.edusoft.backend.user.domain.User;
import bg.unisofia.fmi.edusoft.backend.user.domain.ldap.LdapUser;
import bg.unisofia.fmi.edusoft.backend.user.repository.ldap.LdapUserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import reactor.test.StepVerifier;

public class GroupServiceIntegrationTest extends GenericIntegrationTest {

  @Autowired
  private GroupService groupService;

  @Autowired
  private LdapGroupRepository ldapGroupRepository;

  @Autowired
  private LdapUserRepository ldapUserRepository;

  @Test
  void addUserToGroup__Should_ReturnTrueAndInsertUserInAndCreateGroup_When_UserNotPartOfGroup() {
    String key = "group_01";
    assertUniqueKey(key);
    String groupName = "nonExistent" + key;
    LdapUser ldapUser = generateLdapUser(key);
    ldapUserRepository.save(ldapUser);

    User user = new User(ldapUser, null);

    Assertions.assertTrue(groupService.addUserToGroup(user, groupName));

    //Test it worked + synchronization between member and memberOf
    LdapGroup ldapGroup = ldapGroupRepository.findByName(groupName);
    ldapUser = ldapUserRepository.findByUsername(ldapUser.getUsername());
    assertTrue(ldapGroup.getMembers().contains(ldapUser.getUsername()));
    assertTrue(ldapUser.getMemberOf().contains(ldapGroup.getName()));
  }

  @Test
  void addUserToGroup__Should_ReturnTrueAndInsertUserInGroup_When_AnotherUserPartOfGroup() {
    String key1 = "group_02";
    String key2 = "group_022";
    assertUniqueKey(key1);
    assertUniqueKey(key2);
    String groupName = "nonExistent" + key1;
    LdapUser ldapUserInGroup = generateLdapUser(key1);
    LdapUser ldapUserToBeAddedToGroup = generateLdapUser(key2);
    User userInGroup = new User(ldapUserInGroup, null);
    User userToBeAddedToGroup = new User(ldapUserToBeAddedToGroup, null);

    groupService.addUserToGroup(userInGroup, groupName);

    Assertions.assertTrue(groupService.addUserToGroup(userToBeAddedToGroup, groupName));

    LdapGroup ldapGroup = ldapGroupRepository.findByName(groupName);
    assertTrue(ldapGroup.getMembers().contains(ldapUserToBeAddedToGroup.getUsername()));
    assertTrue(ldapGroup.getMembers().contains(ldapUserInGroup.getUsername()));
  }

  @Test
  void removeUserFromGroup__Should_ReturnTrueAndRemoveUserFromGroupAndDeleteGroup_When_UserIsTheLastOneInGroup() {
    String key = "group_03";
    assertUniqueKey(key);
    String groupName = "groupName" + key;
    LdapUser ldapUser = generateLdapUser(key);
    User user = new User(ldapUser, null);

    ldapUserRepository.save(ldapUser);
    groupService.addUserToGroup(user, groupName);

    ldapUser = ldapUserRepository.findByUsername(ldapUser.getUsername());
    assertTrue(ldapUser.getMemberOf().contains(groupName));

    Assertions.assertTrue(groupService.removeUserFromGroup(user, groupName));

    //Assert member entry was removed from the group
    LdapGroup ldapGroup = ldapGroupRepository.findByName(groupName);
    assertNull(ldapGroup);

    //Assert that the memberOfField is deleted as well
    ldapUser = ldapUserRepository.findByUsername(ldapUser.getUsername());
    assertFalse(ldapUser.getMemberOf().contains(groupName));
  }

  @Test
  void removeUserFromGroup__Should_ReturnTrueAndRemoveUserFromGroup_When_UserNotTheLastOneInGroup() {
    String key1 = "group_04";
    String key2 = "group_044";
    assertUniqueKey(key1);
    assertUniqueKey(key2);
    String groupName = "groupName" + key1;
    LdapUser ldapUser1 = generateLdapUser(key1);
    LdapUser ldapUser2 = generateLdapUser(key2);
    User user1 = new User(ldapUser1, null);
    User user2 = new User(ldapUser2, null);

    ldapUserRepository.save(ldapUser1);
    groupService.addUserToGroup(user1, groupName);
    groupService.addUserToGroup(user2, groupName);

    Assertions.assertTrue(groupService.removeUserFromGroup(user1, groupName));

    //Assert member was removed from the group
    LdapGroup ldapGroup = ldapGroupRepository.findByName(groupName);
    assertFalse(ldapGroup.getMembers().contains(ldapUser1.getUsername()));
    assertTrue(ldapGroup.getMembers().contains(ldapUser2.getUsername()));

    //Assert memberOf was removed at the user
    ldapUser1 = ldapUserRepository.findByUsername(ldapUser1.getUsername());
    assertFalse(ldapUser1.getMemberOf().contains(groupName));

    //Checks if an error will occur if we attempt to remove a user
    //that is not a part of the group
    Assertions.assertTrue(groupService.removeUserFromGroup(user1, groupName));
  }
}
