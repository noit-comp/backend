package bg.unisofia.fmi.edusoft.backend.role;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import bg.unisofia.fmi.edusoft.backend.exception.MissingElementException;
import bg.unisofia.fmi.edusoft.backend.group.service.GroupService;
import bg.unisofia.fmi.edusoft.backend.user.domain.User;
import bg.unisofia.fmi.edusoft.backend.user.service.UserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class RoleServiceUnitTest {

  private static final String USERNAME = "username";
  private static final String ROLE = "role";
  private static final User USER = new User();

  @Autowired
  RoleService roleService;

  @MockBean
  GroupService groupService;

  @MockBean
  UserService userService;

  @Test
  void addRoleToUser__Should_ReturnFalse_When_UserDoesNotExist() throws MissingElementException {
    when(userService.findByUsernameOrMail(USERNAME))
        .thenThrow(new MissingElementException());

    Assertions.assertFalse(roleService.addRoleToUser(ROLE, USERNAME));

    verify(groupService, never()).addUserToGroup(USER, ROLE);
  }

  @Test
  void addRoleToUser__Should_ReturnTrue_When_UserDoesExist() throws MissingElementException {
    when(userService.findByUsernameOrMail(USERNAME))
        .thenReturn(USER);
    when(groupService.addUserToGroup(USER, ROLE)).thenReturn(true);

    Assertions.assertTrue(roleService.addRoleToUser(ROLE, USERNAME));
  }
}
