package bg.unisofia.fmi.edusoft.backend.mail.service;


import static org.junit.jupiter.api.Assertions.assertEquals;

import bg.unisofia.fmi.edusoft.backend.BaseTest;
import java.util.HashMap;
import java.util.Map;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class MailServiceUnitTest extends BaseTest {

  @Autowired
  MailService mailService;

  @Test
  void replaceParamsInBody__Should_ReplaceOnlyTextInCurlyBraces_When_ContainingSameKey() {
    Map<String, String> params = new HashMap<>();
    params.put("test", "works");

    final String actual = MailServiceImpl.replaceParamsInBody("test {test}", params);

    assertEquals("test works", actual);
  }

  @Test
  void replaceParamsInBody__Should_ReplaceMultipleKeys() {
    Map<String, String> params = new HashMap<>();
    params.put("test", "actually");
    params.put("key", "the");
    params.put("works", "works");

    final String actual = MailServiceImpl.replaceParamsInBody("{key} test {test} {works} {works}", params);

    assertEquals("the test actually works works", actual);
  }
}