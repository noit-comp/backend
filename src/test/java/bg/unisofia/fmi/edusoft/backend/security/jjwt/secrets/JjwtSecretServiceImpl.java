package bg.unisofia.fmi.edusoft.backend.security.jjwt.secrets;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

@Primary
@Service
public class JjwtSecretServiceImpl implements JjwtSecretService {

  private static final String secret = "random-secret";
  private static final String encodedSecret = JjwtSecretService.encode(secret);

  @Override
  public String getSecret() {
    return secret;
  }

  @Override
  public String getEncodedSecret() {
    return encodedSecret;
  }

}
