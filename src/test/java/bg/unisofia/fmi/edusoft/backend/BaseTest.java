package bg.unisofia.fmi.edusoft.backend;

import bg.unisofia.fmi.edusoft.backend.user.domain.User;
import bg.unisofia.fmi.edusoft.backend.user.domain.db.DbUser;
import bg.unisofia.fmi.edusoft.backend.user.domain.ldap.LdapUser;
import java.time.LocalDateTime;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@ActiveProfiles({"test"})
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public abstract class BaseTest {
  protected static DbUser generateDbUser(Long userId, String username) {
    return new DbUser(
        userId,
        username,
        false,
        LocalDateTime.now()
    );
  }

  protected static User generateUser(String username) {
    return new User(new LdapUser(), generateDbUser(1L, username));
  }
}
