package bg.unisofia.fmi.edusoft.backend.group.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import bg.unisofia.fmi.edusoft.backend.BaseTest;
import bg.unisofia.fmi.edusoft.backend.group.domain.LdapGroup;
import bg.unisofia.fmi.edusoft.backend.group.repository.LdapGroupRepository;
import bg.unisofia.fmi.edusoft.backend.user.domain.User;
import bg.unisofia.fmi.edusoft.backend.user.domain.db.DbUser;
import bg.unisofia.fmi.edusoft.backend.user.domain.ldap.LdapUser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class GroupServiceUnitTest extends BaseTest {

  private static final String USERNAME = "username";
  private static final String GROUP_NAME = "group_name";
  private static final String PASSWORD = "password123";
  private static final String FIRST_NAME = "fName";
  private static final String MIDDLE_NAME = "mName";
  private static final String LAST_NAME = "lName";
  private static final String MAIL = "mail@me.com";
  private static final LdapGroup GROUP = new LdapGroup(GROUP_NAME);
  private final LdapGroup GROUP_WITH_USER = new LdapGroup(GROUP_NAME);
  private static final LdapUser LDAP_USER =
      new LdapUser(USERNAME, FIRST_NAME, MIDDLE_NAME, LAST_NAME, MAIL, PASSWORD);
  private static final DbUser DB_USER = generateDbUser(1L, USERNAME);
  private static final User USER = new User(LDAP_USER, DB_USER);
  private static final String LDAP_BASE_PATH = "dc=uni,dc=test";
  private static final String LDAP_USER_DN = LDAP_USER.getId() + "," + LDAP_BASE_PATH;
  private static final String SECOND_USER_DN = "second_user_dn," + LDAP_BASE_PATH ;

  @Autowired
  GroupService groupService;

  @MockBean
  LdapGroupRepository ldapGroupRepository;

  @BeforeEach
  void addUserToGroupWithUser() {
    GROUP_WITH_USER.addUserToGroup(LDAP_USER_DN);
    GROUP_WITH_USER.addUserToGroup(SECOND_USER_DN);
  }

  @Test
  void addUserToGroup__Should_ReturnTrue_When_GroupDoesNotExist() {
    when(ldapGroupRepository.findByName(GROUP_NAME)).thenReturn(null);
    when(ldapGroupRepository.save(GROUP_WITH_USER)).thenReturn(GROUP_WITH_USER);

    Assertions.assertTrue(groupService.addUserToGroup(USER, GROUP_NAME));
  }

  @Test
  void addUserToGroup__Should_ReturnTrue_When_GroupDoesExist() {
    when(ldapGroupRepository.findByName(GROUP_NAME)).thenReturn(GROUP);
    when(ldapGroupRepository.save(GROUP_WITH_USER)).thenReturn(GROUP_WITH_USER);

    Assertions.assertTrue(groupService.addUserToGroup(USER, GROUP_NAME));
  }

  @Test
  void removeUserRole__Should_ReturnTrue_When_GroupDoesNotExist() {
    when(ldapGroupRepository.findByName(GROUP_NAME)).thenReturn(null);

    Assertions.assertTrue(groupService.removeUserFromGroup(USER, GROUP_NAME));

    verify(ldapGroupRepository, never()).save(any());
    verify(ldapGroupRepository, never()).delete(any());
  }

  @Test
  void removeUserRole__Should_ReturnTrue_When_UserPartOfGroupAndNotLastMember() {
    when(ldapGroupRepository.findByName(GROUP_NAME)).thenReturn(GROUP_WITH_USER);
    when(ldapGroupRepository.save(GROUP)).thenReturn(GROUP);

    Assertions.assertTrue(groupService.removeUserFromGroup(USER, GROUP_NAME));

    verify(ldapGroupRepository, never()).delete(any());
  }

  @Test
  void removeUserRole__Should_ReturnTrue_When_UserNotPartOfGroupAndNotLastMember() {
    GROUP_WITH_USER.removeUserFromGroup(LDAP_USER_DN);
    when(ldapGroupRepository.findByName(GROUP_NAME)).thenReturn(GROUP_WITH_USER);
    when(ldapGroupRepository.save(GROUP_WITH_USER)).thenReturn(GROUP_WITH_USER);

    Assertions.assertTrue(groupService.removeUserFromGroup(USER, GROUP_NAME));

    verify(ldapGroupRepository, never()).delete(any());
  }

  @Test
  void removeUserRole__Should_ReturnTrue_When_UserPartOfGroupAndLastMember() {
    GROUP_WITH_USER.removeUserFromGroup(SECOND_USER_DN);
    when(ldapGroupRepository.findByName(GROUP_NAME)).thenReturn(GROUP_WITH_USER);

    Assertions.assertTrue(groupService.removeUserFromGroup(USER, GROUP_NAME));

    verify(ldapGroupRepository, never()).save(any());
    verify(ldapGroupRepository, atLeastOnce()).delete(GROUP);
  }
}
