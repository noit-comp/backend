package bg.unisofia.fmi.edusoft.backend.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import bg.unisofia.fmi.edusoft.backend.BaseTest;
import bg.unisofia.fmi.edusoft.backend.exception.MailAlreadyExistsException;
import bg.unisofia.fmi.edusoft.backend.exception.InvalidLdapCredentialsException;
import bg.unisofia.fmi.edusoft.backend.exception.MailRateLimitExceededException;
import bg.unisofia.fmi.edusoft.backend.exception.MissingElementException;
import bg.unisofia.fmi.edusoft.backend.user.controller.UserController;
import bg.unisofia.fmi.edusoft.backend.user.domain.User;
import bg.unisofia.fmi.edusoft.backend.user.domain.db.DbUser;
import bg.unisofia.fmi.edusoft.backend.user.domain.ldap.LdapUser;
import bg.unisofia.fmi.edusoft.backend.user.domain.requests.AuthenticationLoginRequest;
import bg.unisofia.fmi.edusoft.backend.user.domain.requests.UpdateMailRequest;
import bg.unisofia.fmi.edusoft.backend.user.domain.requests.UpdatePasswordRequest;
import bg.unisofia.fmi.edusoft.backend.user.domain.requests.UserTokenDetails;
import bg.unisofia.fmi.edusoft.backend.user.service.UserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class UserControllerUnitTests extends BaseTest {

  private static final String USERNAME = "username";
  private static final String PASSWORD = "password123";
  private static final String FIRST_NAME = "fName";
  private static final String MIDDLE_NAME = "mName";
  private static final String LAST_NAME = "lName";
  private static final String MAIL = "mail@me.com";
  private static final String NEW_MAIL = "mail@you.com";
  private final Long USER_ID = 42L;
  private final LdapUser LDAP_USER =
      new LdapUser(USERNAME, FIRST_NAME, MIDDLE_NAME, LAST_NAME, MAIL, PASSWORD);
  private final DbUser DB_USER = generateDbUser(USER_ID, USERNAME);
  private final User USER = new User(LDAP_USER, DB_USER);
  private final UserTokenDetails USER_TOKEN_DETAIL = new UserTokenDetails(USER);
  private final static UpdatePasswordRequest UPDATE_PASSWORD_REQUEST = new UpdatePasswordRequest(
      "new_password", PASSWORD);
  private final static UpdateMailRequest UPDATE_MAIL_REQUEST = new UpdateMailRequest(
      NEW_MAIL);

  @Autowired
  UserController userController;

  @MockBean
  private UserService userService;

  @MockBean
  private Authentication authentication;

  //region updatePassword
  @Test
  void updatePassword__Should_ReturnHTTPStatusOk_When_UserDoesExist()
      throws MissingElementException {
    when(userService.updatePassword(USERNAME, UPDATE_PASSWORD_REQUEST))
        .thenReturn(LDAP_USER);
    when(authentication.getDetails()).thenReturn(USER_TOKEN_DETAIL);

    ResponseEntity<Object> response = userController
        .updatePassword(authentication, UPDATE_PASSWORD_REQUEST);

    assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
  }

  @Test
  void updatePassword__Should_ReturnHTTPStatusUnauthorized_When_OldPasswordNotValid()
      throws MissingElementException, InvalidLdapCredentialsException {
    when(authentication.getDetails()).thenReturn(USER_TOKEN_DETAIL);
    when(userService.authenticate(new AuthenticationLoginRequest(USERNAME, PASSWORD)))
        .thenThrow(new InvalidLdapCredentialsException());

    ResponseEntity<Object> response = userController
        .updatePassword(authentication, UPDATE_PASSWORD_REQUEST);
    assertEquals(HttpStatus.UNAUTHORIZED, response.getStatusCode());
  }
  //endregion
  //region updateMail

  @Test
  void updateMail__Should_ReturnHTTPStatusConflict_When_EmailIsTaken()
      throws MailAlreadyExistsException {
    when(authentication.getDetails()).thenReturn(USER_TOKEN_DETAIL);
    doThrow(new MailAlreadyExistsException()).when(userService)
        .requestMailUpdate(USER_TOKEN_DETAIL, UPDATE_MAIL_REQUEST);

    ResponseEntity<Object> response = userController
        .updateMail(authentication, UPDATE_MAIL_REQUEST);
    assertEquals(HttpStatus.CONFLICT, response.getStatusCode());
  }

  @Test
  void updateMail__Should_ReturnHTTPStatusTooManyRequests_When_MailRateLimitExceeded()
      throws MailAlreadyExistsException {
    when(authentication.getDetails()).thenReturn(USER_TOKEN_DETAIL);
    doThrow(new MailRateLimitExceededException()).when(userService)
        .requestMailUpdate(USER_TOKEN_DETAIL, UPDATE_MAIL_REQUEST);

    ResponseEntity<Object> response = userController
        .updateMail(authentication, UPDATE_MAIL_REQUEST);
    assertEquals(HttpStatus.TOO_MANY_REQUESTS, response.getStatusCode());
  }

  @Test
  void updateMail__Should_ReturnHTTPStatusCreated_When_EmailIsValidAndNotTaken()
      throws MailAlreadyExistsException {
    when(authentication.getDetails()).thenReturn(USER_TOKEN_DETAIL);
    doNothing().when(userService).requestMailUpdate(USER_TOKEN_DETAIL, UPDATE_MAIL_REQUEST);

    ResponseEntity<Object> response = userController
        .updateMail(authentication, UPDATE_MAIL_REQUEST);
    assertEquals(HttpStatus.CREATED, response.getStatusCode());
  }
  //endregion
}
