package bg.unisofia.fmi.edusoft.backend.enums;

import java.util.Arrays;

public abstract class EnumAbstractService<
    EntityT extends EnumAbstractType,
    EnumTypeT extends Enum<EnumTypeT>>
    extends GenericEnumAbstractService<EntityT, EnumTypeT> {

  /**
   * Abstract enum tables service mock for unit tests.
   */
  public EnumAbstractService(
      final EnumAbstractRepository<EntityT> enumAbstractRepository,
      final Class<EnumTypeT> enumClass) {

    super(enumClass);

  }

  @SafeVarargs
  public final void setup(final EntityT... entities) {
    load(Arrays.asList(entities));
  }

}
