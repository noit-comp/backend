package bg.unisofia.fmi.edusoft.backend.activity;

import bg.unisofia.fmi.edusoft.backend.BaseTest;
import java.util.Arrays;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class ActivityServiceUnitTest extends BaseTest {

  private static final ActivityEntity[] STATES = new ActivityEntity[]{
      new ActivityEntity(1L, "REGISTER_PROJECTS"),
      new ActivityEntity(2L, "EDIT_PROJECTS"),
      new ActivityEntity(3L, "GRADE_PROJECTS_FOR_ROUND"),
      new ActivityEntity(4L, "ACCEPT_INSTRUCTING_PROJECT"),
      new ActivityEntity(5L, "ACCEPT_PARTICIPATING_IN_PROJECT")
  };

  @Autowired
  private ActivityService activityService;

  @BeforeEach
  public void beforeAll() {
    activityService.setup(STATES);
  }

  @Test
  public void Should_MatchIdsWithValues_When_GetFromService() {
    for (final ActivityEntity state : STATES) {
      Assertions
          .assertEquals(activityService.get(state.getId()), Activity.valueOf(state.getValue()));
    }
  }

  @Test
  public void Should_MatchValuesWithIds_When_GetFromService() {
    for (final Activity activity : Activity.values()) {
      final Long id = Arrays.stream(STATES)
          .filter(x -> x.getValue().equals(activity.name()))
          .findFirst()
          .get()
          .getId();
      Assertions.assertEquals(activityService.get(activity), id);
    }
  }
}
