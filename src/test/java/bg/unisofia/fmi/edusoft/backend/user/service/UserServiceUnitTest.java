package bg.unisofia.fmi.edusoft.backend.user.service;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import bg.unisofia.fmi.edusoft.backend.BaseTest;
import bg.unisofia.fmi.edusoft.backend.domain.db.UpdateMail;
import bg.unisofia.fmi.edusoft.backend.exception.InvalidLdapCredentialsException;
import bg.unisofia.fmi.edusoft.backend.exception.MailAlreadyExistsException;
import bg.unisofia.fmi.edusoft.backend.exception.MissingElementException;
import bg.unisofia.fmi.edusoft.backend.exception.UsernameAlreadyExistsException;
import bg.unisofia.fmi.edusoft.backend.fake.FakeUpdateMailTokenService;
import bg.unisofia.fmi.edusoft.backend.mail.domain.MailTemplateType;
import bg.unisofia.fmi.edusoft.backend.mail.service.MailService;
import bg.unisofia.fmi.edusoft.backend.token.service.TokenGeneratorService;
import bg.unisofia.fmi.edusoft.backend.token.type.TokenType;
import bg.unisofia.fmi.edusoft.backend.user.domain.User;
import bg.unisofia.fmi.edusoft.backend.user.domain.db.DbUser;
import bg.unisofia.fmi.edusoft.backend.user.domain.ldap.LdapUser;
import bg.unisofia.fmi.edusoft.backend.user.domain.requests.AuthenticationLoginRequest;
import bg.unisofia.fmi.edusoft.backend.user.domain.requests.RegisterUserRequest;
import bg.unisofia.fmi.edusoft.backend.user.domain.requests.UpdateMailRequest;
import bg.unisofia.fmi.edusoft.backend.user.domain.requests.UpdatePasswordRequest;
import bg.unisofia.fmi.edusoft.backend.user.domain.requests.UserTokenDetails;
import bg.unisofia.fmi.edusoft.backend.user.login.DbUserLoginRepository;
import bg.unisofia.fmi.edusoft.backend.user.mail.UpdateMailRepository;
import bg.unisofia.fmi.edusoft.backend.user.repository.db.DbUserRepository;
import bg.unisofia.fmi.edusoft.backend.user.repository.ldap.CustomLdapUserRepository;
import bg.unisofia.fmi.edusoft.backend.user.repository.ldap.LdapUserRepository;
import java.time.OffsetDateTime;
import java.util.Map;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.password.PasswordEncoder;

public class UserServiceUnitTest extends BaseTest {

  private static final String USERNAME = "username";
  private static final String FRONTEND_URL = "localhost";
  private static final String PASSWORD = "password123";
  private static final String NEW_PASSWORD = "password321";
  private static final String FIRST_NAME = "fName";
  private static final String MIDDLE_NAME = "mName";
  private static final String LAST_NAME = "lName";
  private static final String MAIL = "mail@me.com";
  private static final String NEW_MAIL = "mail@you.com";
  private static final UUID TOKEN = UUID.randomUUID();
  private final LdapUser LDAP_USER =
      new LdapUser(USERNAME, FIRST_NAME, MIDDLE_NAME, LAST_NAME, MAIL, PASSWORD);
  private static final AuthenticationLoginRequest AUTHENTICATION_LOGIN_REQUEST =
      new AuthenticationLoginRequest(USERNAME, PASSWORD);
  private static final RegisterUserRequest REGISTER_USER_REQUEST =
      new RegisterUserRequest(USERNAME, PASSWORD, MAIL, FIRST_NAME, MIDDLE_NAME, LAST_NAME);
  private final static Long ROW_ID = 42L;
  private final static Long USER_ID = 43L;
  private final static UpdatePasswordRequest UPDATE_PASSWORD_REQUEST = new UpdatePasswordRequest(
      NEW_PASSWORD, PASSWORD);
  private final static UpdateMailRequest UPDATE_MAIL_REQUEST = new UpdateMailRequest(
      NEW_MAIL);
  private final UpdateMail UPDATE_MAIL_ROW = new UpdateMail(ROW_ID, NEW_MAIL, USER_ID,
      false, OffsetDateTime.now());
  private static final DbUser DB_USER = generateDbUser(USER_ID, USERNAME);
  private final UserTokenDetails USER_TOKEN_DETAILS = new UserTokenDetails(
      new User(LDAP_USER, DB_USER));


  @Captor
  ArgumentCaptor<LdapUser> ldapUserCaptor;

  @Autowired
  UserService userService;
  @Autowired
  PasswordEncoder passwordEncoder;

  @MockBean
  LdapUserRepository ldapUserRepository;
  @MockBean
  UpdateMailRepository updateMailRepository;
  @MockBean
  DbUserRepository dbUserRepository;
  @MockBean
  DbUserLoginRepository dbUserLoginRepository;
  @MockBean
  CustomLdapUserRepository customLdapUserRepository;
  @MockBean
  FakeUpdateMailTokenService fakeUpdateMailTokenService;
  @MockBean
  TokenGeneratorService tokenGeneratorService;
  @MockBean
  MailService mailService;


  private void mockAuthenticateUser() {
    when(ldapUserRepository.findByUsernameOrMail(anyString(), anyString())).thenReturn(LDAP_USER);
    when(customLdapUserRepository.authenticate(anyString(), anyString())).thenReturn(true);
  }

  // region authenticate
  @Test
  void authenticate__Should_ReturnUser_When_ExistUserWithSuchCredentialsWithEntryInDb() {
    mockAuthenticateUser();
    when(dbUserRepository.findByUsername(anyString())).thenReturn(DB_USER);

    User user = assertDoesNotThrow(() ->
        userService.authenticate(AUTHENTICATION_LOGIN_REQUEST));

    assertTrue(user.getUsername().equals(USERNAME)
        && user.getId().equals(DB_USER.getId()));
  }

  @Test
  void authenticate__Should_ReturnUser_When_ExistUserWithSuchCredentialsAndWithNoEntryInDb() {
    mockAuthenticateUser();
    when(dbUserRepository.findByUsername(anyString())).thenReturn(null);
    when(dbUserRepository.insertWithUsername(DB_USER.getUsername())).thenReturn(DB_USER.getId());
    when(dbUserRepository.findById(DB_USER.getId())).thenReturn(DB_USER);

    User user = assertDoesNotThrow(() ->
        userService.authenticate(AUTHENTICATION_LOGIN_REQUEST));

    assertTrue(user.getUsername().equals(USERNAME)
        && user.getId().equals(DB_USER.getId()));
  }

  @Test
  void authenticate__Should_InsertLoginInfo_When_ExistUserWithSuchCredentialsAndWithEntryInDb() {
    mockAuthenticateUser();
    when(dbUserRepository.findByUsername(anyString())).thenReturn(DB_USER);
    assertDoesNotThrow(() -> userService.authenticate(AUTHENTICATION_LOGIN_REQUEST));

    verify(dbUserLoginRepository, atLeastOnce()).insert(any(), any());
  }

  @Test
  void authenticate__Should_InsertLoginInfo_When_ExistUserWithSuchCredentialsAndWithNoEntryInDb() {
    when(ldapUserRepository.findByUsernameOrMail(anyString(), anyString())).thenReturn(LDAP_USER);
    when(dbUserRepository.findByUsername(anyString())).thenReturn(null);
    when(customLdapUserRepository.authenticate(anyString(), anyString())).thenReturn(true);
    when(dbUserRepository.insertWithUsername(DB_USER.getUsername())).thenReturn(DB_USER.getId());
    when(dbUserRepository.findById(DB_USER.getId())).thenReturn(DB_USER);

    assertDoesNotThrow(() -> userService.authenticate(AUTHENTICATION_LOGIN_REQUEST));

    verify(dbUserLoginRepository, atLeastOnce()).insert(any(), any());
  }

  @Test
  void authenticate__Should_MissingElementException_When_NoUserWithThisEmailOrUsername() {
    when(ldapUserRepository.findByUsernameOrMail(anyString(), anyString())).thenReturn(null);
    assertThrows(MissingElementException.class,
        () -> userService.authenticate(AUTHENTICATION_LOGIN_REQUEST));
  }

  @Test
  void authenticate__Should_InvalidLdapCredentials_When_PasswordDoesNotMatch() {
    when(ldapUserRepository.findByUsernameOrMail(anyString(), anyString())).thenReturn(LDAP_USER);
    assertThrows(InvalidLdapCredentialsException.class, () ->
        userService
            .authenticate(new AuthenticationLoginRequest(USERNAME, PASSWORD.concat(PASSWORD))));
  }

  // endregion
  // region findUserByUsernameOrMail

  @Test
  void findByUsernameOrMail__Should_ReturnValidUser_When_UserExists() {
    when(ldapUserRepository.findByUsernameOrMail(anyString(), anyString())).thenReturn(LDAP_USER);
    when(dbUserRepository.findByUsername(anyString())).thenReturn(DB_USER);

    assertDoesNotThrow(() -> {
      userService.findByUsernameOrMail(USERNAME);
    });
  }

  @Test
  void findByUsernameOrMail__Should_InsertDataIntoDb_When_MissingSuchData() {
    when(ldapUserRepository.findByUsernameOrMail(anyString(), anyString())).thenReturn(LDAP_USER);
    when(dbUserRepository.findByUsername(anyString())).thenReturn(null);
    when(dbUserRepository.insertWithUsername(DB_USER.getUsername())).thenReturn(DB_USER.getId());

    assertDoesNotThrow(() -> userService.findByUsernameOrMail(USERNAME));
    verify(dbUserRepository, atLeastOnce()).insertWithUsername(USERNAME);
  }

  @Test
  void findByUsernameOrMail__Should_ReturnMissingElementException_When_NoSuchUserInLdap() {
    when(ldapUserRepository.findByUsernameOrMail(anyString(), anyString())).thenReturn(null);

    assertThrows(MissingElementException.class, () -> userService.findByUsernameOrMail(USERNAME));
  }

  // endregion
  // region register
  @Test
  void register_Should_ReturnCombinedUser_When_RegistrationIsSuccessful() {
    when(ldapUserRepository.existsByUsername(anyString())).thenReturn(false);
    when(ldapUserRepository.existsByMail(anyString())).thenReturn(false);
    when(ldapUserRepository.save(any())).thenReturn(LDAP_USER);
    when(dbUserRepository.findByUsername(anyString())).thenReturn(null);
    when(dbUserRepository.insertWithUsername(DB_USER.getUsername())).thenReturn(DB_USER.getId());
    when(dbUserRepository.findById(DB_USER.getId())).thenReturn(DB_USER);
    when(updateMailRepository.findUpdateMailByNewMailAndUserId(MAIL, USER_ID)).thenReturn(null);
    when(updateMailRepository.insert(anyString(), anyLong())).thenReturn(ROW_ID);
    when(tokenGeneratorService.createToken(USER_ID, TokenType.CONFIRM_EMAIL, MAIL, ROW_ID))
        .thenReturn(TOKEN);

    Map<String, String> expectedParams = Map.of(
        "token",FRONTEND_URL + "/token/" + TOKEN.toString()
    );

    when(mailService.sendMail(MailTemplateType.CONFIRM_MAIL, MAIL, expectedParams))
        .thenReturn(true);

    User user = assertDoesNotThrow(() -> userService.register(REGISTER_USER_REQUEST));
    assertNotNull(user);
    assertEquals(USERNAME, user.getUsername());
  }

  @Test
  void register_Should_ReturnUsernameAlreadyExists_When_AlreadyHasThisUsername() {
    when(ldapUserRepository.existsByUsername(anyString())).thenReturn(true);

    assertThrows(UsernameAlreadyExistsException.class,
        () -> userService.register(REGISTER_USER_REQUEST));

    verify(ldapUserRepository, never()).save(any());
    verify(dbUserRepository, never()).insertWithUsername(REGISTER_USER_REQUEST.getUsername());
    verify(dbUserRepository, never()).findByUsername(any());
  }

  @Test
  void register_Should_ReturnEmailAlreadyExists_When_AlreadyRegisteredMail() {
    when(ldapUserRepository.existsByUsername(anyString())).thenReturn(false);
    when(ldapUserRepository.existsByMail(anyString())).thenReturn(true);

    assertThrows(MailAlreadyExistsException.class,
        () -> userService.register(REGISTER_USER_REQUEST));

    verify(ldapUserRepository, never()).save(any());
    verify(dbUserRepository, never()).insertWithUsername(REGISTER_USER_REQUEST.getUsername());
  }

  // endregion
  // requestMailUpdate region
  @Test
  void requestMailUpdate__Should_ThrowEmailAlreadyExistsException_When_EmailAlreadyExists() {
    when(ldapUserRepository.existsByMail(NEW_MAIL)).thenReturn(true);

    assertThrows(MailAlreadyExistsException.class,
        () -> userService.requestMailUpdate(USER_TOKEN_DETAILS, UPDATE_MAIL_REQUEST));

    verify(dbUserRepository, never()).findByUsername(anyString());
  }

  @Test
  void requestMailUpdate__Should_CreateMailEntry_When_EmailIsUniqueAndDoesNotExistInDB()
      throws MailAlreadyExistsException {
    when(ldapUserRepository.existsByMail(NEW_MAIL)).thenReturn(false);
    when(updateMailRepository.findUpdateMailByNewMailAndUserId(NEW_MAIL, USER_ID))
        .thenReturn(UPDATE_MAIL_ROW);
    when(updateMailRepository.findUpdateMailCountByUserIdForTime(any(),
       any())).thenReturn(0);
    when(updateMailRepository.insert(anyString(), anyLong())).thenReturn(ROW_ID);
    when(tokenGeneratorService.createToken(USER_ID, TokenType.CONFIRM_EMAIL, MAIL, ROW_ID))
        .thenReturn(TOKEN);

    Map<String, String> expectedParams = Map.of(
        "token",FRONTEND_URL + "/token/" + TOKEN.toString()
    );

    when(mailService.sendMail(MailTemplateType.CONFIRM_MAIL, MAIL, expectedParams))
        .thenReturn(true);

    userService.requestMailUpdate(USER_TOKEN_DETAILS, UPDATE_MAIL_REQUEST);
  }

  //endregion
  //updatePassword region
  @Test
  void updatePassword__Should_UpdatePassword_When_UserExists() throws MissingElementException {
    LdapUser ldapUserWithNewPassword = new LdapUser(USERNAME, FIRST_NAME, MIDDLE_NAME, LAST_NAME,
        MAIL, NEW_PASSWORD);
    when(ldapUserRepository.findByUsernameOrMail(USERNAME, USERNAME)).thenReturn(LDAP_USER);
    when(ldapUserRepository.save(ldapUserWithNewPassword))
        .thenReturn(ldapUserWithNewPassword);

    LdapUser ldapUser = userService.updatePassword(USERNAME, UPDATE_PASSWORD_REQUEST);
    assertEquals(NEW_PASSWORD, ldapUser.getPassword());
  }

  @Test
  void updatePassword__Should__ThrowMissingElementException_When_UserDoesNotExist() {
    when(ldapUserRepository.findByUsernameOrMail(USERNAME, USERNAME)).thenReturn(null);

    assertThrows(MissingElementException.class,
        () -> userService.updatePassword(USERNAME, UPDATE_PASSWORD_REQUEST));

    verify(ldapUserRepository, never()).save(any());
  }
  //endregion

  //region confirmedMail
  @Test
  void confirmedMail__Should__ReturnMissingElementException_When_UpdateMailRowDoesNotExist() {
    when(updateMailRepository.findById(ROW_ID)).thenReturn(null);

    assertThrows(MissingElementException.class, () -> userService.confirmedMail(ROW_ID));
    verify(ldapUserRepository, never()).existsByMail(anyString());
  }

  @Test
  void confirmedMail__Should__ReturnEmailAlreadyExistsException_When_LdapUserWithEmailExists() {
    when(updateMailRepository.findById(ROW_ID)).thenReturn(UPDATE_MAIL_ROW);
    when(ldapUserRepository.existsByMail(NEW_MAIL)).thenReturn(true);

    assertThrows(MailAlreadyExistsException.class, () -> userService.confirmedMail(ROW_ID));
    verify(dbUserRepository, never()).findById(anyLong());
  }

  @Test
  void confirmedMail__Should_ReturnLdapUser_When_UpdateMailRowExists()
      throws MissingElementException, MailAlreadyExistsException {
    LdapUser ldapUserWithNewMail =
        new LdapUser(USERNAME, FIRST_NAME, MIDDLE_NAME, LAST_NAME, NEW_MAIL, PASSWORD);
    when(updateMailRepository.findById(ROW_ID))
        .thenReturn(UPDATE_MAIL_ROW);
    doNothing().when(dbUserRepository).confirmMailByUserId(USER_ID);
    when(dbUserRepository.findById(USER_ID)).thenReturn(DB_USER);
    doNothing().when(updateMailRepository).setConfirmed(ROW_ID, true);
    when(ldapUserRepository.findByUsernameOrMail(USERNAME, USERNAME)).thenReturn(LDAP_USER);
    when(ldapUserRepository.save(ldapUserCaptor.capture())).thenReturn(ldapUserWithNewMail);

    userService.confirmedMail(ROW_ID);

    assertEquals(NEW_MAIL, ldapUserCaptor.getValue().getMail());
  }
  //endregion
}