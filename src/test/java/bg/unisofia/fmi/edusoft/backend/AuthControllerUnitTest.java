package bg.unisofia.fmi.edusoft.backend;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

import bg.unisofia.fmi.edusoft.backend.exception.MailAlreadyExistsException;
import bg.unisofia.fmi.edusoft.backend.exception.InvalidLdapCredentialsException;
import bg.unisofia.fmi.edusoft.backend.exception.MissingElementException;
import bg.unisofia.fmi.edusoft.backend.exception.UsernameAlreadyExistsException;
import bg.unisofia.fmi.edusoft.backend.security.jjwt.JjwtUtil;
import bg.unisofia.fmi.edusoft.backend.user.controller.AuthController;
import bg.unisofia.fmi.edusoft.backend.user.domain.User;
import bg.unisofia.fmi.edusoft.backend.user.domain.requests.AuthenticationLoginRequest;
import bg.unisofia.fmi.edusoft.backend.user.domain.requests.RegisterUserRequest;
import bg.unisofia.fmi.edusoft.backend.user.domain.requests.UserDetailsResponse;
import bg.unisofia.fmi.edusoft.backend.user.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.util.UriComponentsBuilder;

public class AuthControllerUnitTest extends BaseTest {

  private static final String USERNAME = "username";
  private static final String PASSWORD = "password123";
  private static final String FIRST_NAME = "fName";
  private static final String MIDDLE_NAME = "mName";
  private static final String LAST_NAME = "lName";
  private static final String MAIL = "mail@me.com";

  @MockBean
  UserService userService;

  @Autowired
  AuthController authController;

  @Autowired
  JjwtUtil jjwtUtil;

  UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder.newInstance();

  @Test
  void register__Should_ReturnHttpCreatedWhenUnusedCredentialsArePassed()
      throws MailAlreadyExistsException, UsernameAlreadyExistsException {
    when(userService.register(any())).thenReturn(generateUser(USERNAME));

    ResponseEntity<Object> responseEntity = authController
        .register(new RegisterUserRequest(USERNAME, PASSWORD,
            MAIL, FIRST_NAME, MIDDLE_NAME, LAST_NAME), uriComponentsBuilder);
    assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
  }

  @Test
  void register__Should_ReturnHttpConflictWhenMailAlreadyExists()
      throws MailAlreadyExistsException, UsernameAlreadyExistsException {
    when(userService.register(any())).thenThrow(new MailAlreadyExistsException());

    ResponseEntity<Object> responseEntity = authController
        .register(new RegisterUserRequest(USERNAME, PASSWORD,
            MAIL, FIRST_NAME, MIDDLE_NAME, LAST_NAME), uriComponentsBuilder);
    assertEquals(HttpStatus.CONFLICT, responseEntity.getStatusCode());
    assertEquals("Mail already exists", responseEntity.getBody());
  }

  @Test
  void register__Should_ReturnHttpConflictWhenUsernameAlreadyExists()
      throws MailAlreadyExistsException, UsernameAlreadyExistsException {
    when(userService.register(any())).thenThrow(new UsernameAlreadyExistsException());

    ResponseEntity<Object> responseEntity = authController
        .register(new RegisterUserRequest(USERNAME, PASSWORD,
            MAIL, FIRST_NAME, MIDDLE_NAME, LAST_NAME), uriComponentsBuilder);
    assertEquals(HttpStatus.CONFLICT, responseEntity.getStatusCode());
    assertEquals("Username already exists", responseEntity.getBody());
  }

  @Test
  void login__Should_ReturnHttpOkWhenCorrectCredentialsArePassed()
      throws MissingElementException, InvalidLdapCredentialsException {
    when(userService.authenticate(any()))
        .thenReturn(generateUser(USERNAME));

    ResponseEntity<String> responseEntity = authController
        .login(new AuthenticationLoginRequest(USERNAME, PASSWORD));
    assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    assertEquals(true, jjwtUtil.validateToken(responseEntity.getBody()));
  }

  @Test
  void login__Should_ReturnHttpUnauthorizedWhenWrongCredentialsArePassed()
      throws MissingElementException, InvalidLdapCredentialsException {
    when(userService.authenticate(any())).thenThrow(new InvalidLdapCredentialsException());

    ResponseEntity<String> responseEntity = authController
        .login(new AuthenticationLoginRequest(USERNAME, PASSWORD));
    assertEquals(HttpStatus.UNAUTHORIZED, responseEntity.getStatusCode());
  }

  @Test
  void user__Should_ReturnUserData() throws MissingElementException {
    final User user = generateUser(USERNAME);
    UserDetailsResponse userDetailsResponse = new UserDetailsResponse(user);
    when(userService.findByUsernameOrMail(any())).thenReturn(user);

    UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(USERNAME,
        null, null);

    ResponseEntity<Object> responseEntity = authController.me(auth);
    assertEquals(userDetailsResponse, responseEntity.getBody());
  }
}
