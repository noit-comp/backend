package bg.unisofia.fmi.edusoft.backend.token.type;

import bg.unisofia.fmi.edusoft.backend.BaseTest;
import java.util.Arrays;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class TokenServiceUnitTest extends BaseTest {

  private static final TokenTypeEntity[] TOKENS = new TokenTypeEntity[]{
      new TokenTypeEntity(1L, "GRANT_COMMITTEE", true, 60L, true),
      new TokenTypeEntity(2L, "GRANT_SCHOOL", true, 60L, true),
      new TokenTypeEntity(3L, "GRANT_REGION", true, 60L, true),
      new TokenTypeEntity(4L, "ACCEPT_INSTRUCTING_PROJECT", true, 60L, true),
      new TokenTypeEntity(5L, "ACCEPT_PRACTICING_IN_PROJECT", true, 60L, true),
      new TokenTypeEntity(6L, "CONFIRM_EMAIL", true, 60L, true),
      new TokenTypeEntity(7L, "ACCEPT_PARTICIPATING_IN_PROJECT", true, 60L, true)
  };

  @Autowired
  private TokenTypeService tokenTypeService;

  @BeforeEach
  public void beforeAll() {
    tokenTypeService.setup(TOKENS);
  }

  @Test
  public void Should_MatchIdsWithValues_When_GetFromService() {
    for (final TokenTypeEntity token : TOKENS) {
      Assertions
          .assertEquals(tokenTypeService.get(token.getId()), TokenType.valueOf(token.getValue()));
    }
  }

  @Test
  public void Should_MatchValuesWithIds_When_GetFromService() {
    for (final TokenType tokenType : TokenType.values()) {
      final Long id = Arrays.stream(TOKENS)
          .filter(x -> x.getValue().equals(tokenType.name()))
          .findFirst()
          .get()
          .getId();
      Assertions.assertEquals(tokenTypeService.get(tokenType), id);
    }
  }
}
