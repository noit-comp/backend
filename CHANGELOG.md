# Changelog
All notable changes to this project will be documented in this file.
 
The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).





--- 
## [Unreleased] - yyyy-mm-dd
### Added

### Changed

### Fixed





--- 
## [v0.1.0] - 2020-10-25
### Added
+ ACL on endpoints
+ Roles and authorities logic
+ Group names generation
+ Error template on backend
+ Enum tables

### Changed
+ 389DS with OpenLdap
+ Remove cache for CI pipelines

### Fixed





--- 
## [v0.0.9] - 2020-07-23
### Added
+ Ldap User Roles
+ Setup continious deployment

### Changed
+ Separate VersionDto

### Fixed
+ Cors
+ BootJar version
+ Required field exception handling
+ Automatic open api-docs for swagger





--- 
## [v0.0.8] - 2020-06-26
### Added
+ Technology for project
+ Integration tests to use Testcontainers

### Changed
+ DB Diagram images
+ Move UserService to webflux
 
### Fixed
+ Bugs related to mutable java.util.Date fields
+ Unit test for UserService
+ Integration test for UserService
+ User registration





---
## [v0.0.7] - 2020-05-31
### Added
+ Add checkstyle for codestyle checking
+ Add more repo docs and update README
+ Add user additional properties
 
### Changed
+ Update spring version to 2.3.0.RELEASE (#24)
+ Update gradle to version 6.4.1
 
### Fixed
+ Fix user passwords in test ldap (#25)
+ Fix error handler to throw only 500 and 401 (#20)
+ Fix errors from spotbugsMain
+ Fix errors from checkstyleMain





---
## [v0.0.6] - 2020-05-18
### Added
+ Add more api docs (#16)
+ Created DB entities (#9)

### Changed

### Fixed
+ Escaping psql queries issue (#22)
+ Embedded ldap basedn (#13)





---
## [v0.0.5] - 2020-05-11
### Added
   
### Changed
+ Separate unit and integration tests
 
### Fixed
+ fix middlename ldap bug




 

---
## [v0.0.4] - 2020-05-10
### Added
+ Add swagger-ui and open api for docs
   
### Changed
+ Move whole project to gradle
+ Add env variables for db, ldap and some app variables
+ Fix test application.yml file

### Fixed
+ fix unmatching ldap password encoder algos
