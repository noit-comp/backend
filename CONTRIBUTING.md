# Contributing Guide

- Contributing to NOIT/Backend is fairly easy. This document shows you how to
  get started

## General
- The [Codebase Structure](./CODEBASE_STRUCTURE.md) has detailed information 
  about how the various files in this project are structured
- Please ensure that any changes you make are in accordance with the
  [Coding Guidelines](./CODING_GUIDELINES.md) of this repo
- Before uploading your code test it as described [here](README.md#tests)

## Submitting changes
- Fork the repo
- Check out a new branch based and name it to what you intend to do:
  - Example:
    ````
    $ git checkout -b BRANCH_NAME
    ````
    If you get an error, you may need to fetch fooBar first by using
    ````
    $ git remote update && git fetch
    ````
  - Use one branch per fix / feature
  - Is there is issue related to fix / feature you want to commit you
    should use `Gitlab issue branch naming` or just create branch via Gitlab
- Commit your changes
  - Please provide a git message that explains what you've done
  - Please make sure your commits follow
    [this commit style](https://chris.beams.io/posts/git-commit/)
  - Commit to the forked repository
  - Make a pull request
  - Gitlab CI is watching you!
  - Wait till you PR is approved

If you follow these instructions, your PR will land pretty safely in the
  main repo!
