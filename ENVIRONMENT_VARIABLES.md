# Environment variables
We have created some variables for easy configuration and removing data
duplication. We strongly recommend redefining ***at least DB_PASS in 
PRODUCTION***

| Variable | Required | Default | Description |
| ------  | :--: | ------ | ------ | 
| DB_HOST | &#10004; Required | NONE | Database host |
| DB_PORT | &#10004; Required | NONE | Database port |
| DB_NAME | &#10004; Required | NONE | Database name |
| DB_USER | &#10004; Required | NONE | Database login user name |
| DB_PASS | &#10004; Required | NONE | Database login user password |
| ALLOWED_CORS | :x: Optional | NONE | Configuring backend allowed CORS domains. Multiple domains should be comma separated! (example: http://localhost:3000) |
| APP_DATA_PATH | :x: Optional | /data | Where this app would store data |
| APP_LOG_FILE | :x: Optional | /var/log/backend/app.log | Log file for this app |
| LDAP_USERNAME | &#10004; Required | NONE | LDAP admin username |
| LDAP_PASSWORD | &#10004; Required | NONE | LDAP admin password |
| LDAP_URL | &#10004; Required | NONE | LDAP server url address (I.e. ldap://localhost:389) |
| LDAP_BASE | &#10004; Required | NONE | LDAP search base (I.e. dc=springframework,dc=or) |
| SPRING_PROFILES_INCLUDE | :x: Optional | NONE | We use it in production to include ldap (with `enable-ldap` value) |
| API_DOCS_SERVER_URL | :x: Optional | http://localhost:8080 | It is used to add url in api-docs servers. The url will be used to send requests from the docs. (example: https://api.edusoft.fmi.uni-sofia.bg) |
| SMTP_SERVER | &#10004; Required | NONE | SMTP server url address (I.e. smtp.gmail.com) |
| SMTP_PORT | &#10004; Required | NONE | The port the SMTP server is listening on |
| SMTP_USERNAME | &#10004; Required | NONE | The username of the user you will send mail with (I.e. user in user@example.com) |
| SMTP_PASSWORD | &#10004; Required | NONE | The password for the user. Might be a generated token as well. |
| SMTP_MAIL | &#10004; Required | NONE | The email you send mail with (username + domain) (I.e. user@example.com) | 
